#!/bin/sh 


contachar()
{

# letter-count2.sh: Counting letter occurrences in a text file.
#
# Script by nyal [nyal@voila.fr].
# Used in ABS Guide with permission.
# Recommented and reformatted by ABS Guide author.
# Version 1.1: Modified to work with gawk 3.1.3.
#              (Will still work with earlier versions.)


INIT_TAB_AWK=""
# Parameter to initialize awk script.
count_case=0
FILE_PARSE=$1

E_PARAMERR=85

usage()
{
    echo "Usage: letter-count.sh file letters" 2>&1
    # For example:   ./letter-count2.sh filename.txt a b c
    exit $E_PARAMERR  # Too few arguments passed to script.
}

if [ ! -f "$1" ] ; then
    echo "$1: No such file." 2>&1
    usage                 # Print usage message and exit.
fi 

if [ -z "$2" ] ; then
    echo "$2: No letters specified." 2>&1
    usage
fi 

shift                      # Letters specified.
for letter in `echo $@`    # For each one . . .
  do
  INIT_TAB_AWK="$INIT_TAB_AWK tab_search[${count_case}] = \
  \"$letter\"; final_tab[${count_case}] = 0; " 
  # Pass as parameter to awk script below.
  count_case=`expr $count_case + 1`
done

# DEBUG:
# echo $INIT_TAB_AWK;

cat $FILE_PARSE |
# Pipe the target file to the following awk script.

# ---------------------------------------------------------------------
# Earlier version of script:
# awk -v tab_search=0 -v final_tab=0 -v tab=0 -v \
# nb_letter=0 -v chara=0 -v chara2=0 \

awk \
"BEGIN { $INIT_TAB_AWK } \
{ split(\$0, tab, \"\"); \
for (chara in tab) \
{ for (chara2 in tab_search) \
{ if (tab_search[chara2] == tab[chara]) { final_tab[chara2]++ } } } } \
END { for (chara in final_tab) \
{ print  final_tab[chara] } }"
# ---------------------------------------------------------------------
#  Nothing all that complicated, just . . .
#+ for-loops, if-tests, and a couple of specialized functions.

return 

# Compare this script to letter-count.sh.

}






LOCALQOBS="../../../OBS/QOBS/"


qobsfile=$LOCALQOBS"vobs_ale.csv"

contapv=`contachar  $qobsfile ";"`
contav=`contachar  $qobsfile ","`

echo $contapv
echo $contav

if test $contapv -gt 0 
then 
delim=";"
fi 
if test $contav -gt 0 
then 
delim=","
fi 


while IFS= read -r var 
do
  #echo $var 
  token=`echo $var | cut -d$delim -f1`
 
   if [ "$token" ==  "Posto" ]
  then 
      echo "Pegando linha com datas"  
      DATAS=$var 
	  echo $DATAS > .temps
      numreg=`contachar  .temps $delim`
	  let numreg=(numreg+1)
	  let lastreg=(numreg-60) 	   
	  
	   
      t=0
	  for n in `seq $lastreg $numreg`
	  do
	       
	       #data[$n]=`cat .temps | cut -d";" -f$n`
		   #echo `cat .temps | cut -d$delim -f$n | sed `
		   ano=`cat .temps | cut -d$delim -f$n | cut -d"/" -f3`
		   mes=`cat .temps | cut -d$delim -f$n | cut -d"/" -f2`
		   dia=`cat .temps | cut -d$delim -f$n | cut -d"/" -f1`
		   
		   #datas[$t]=`cat .temps | cut -d$delim -f$n | cut -d"/"`
		   #datas[$t]=`cat .temps | cut -d$delim -f$n`
		   #echo $ano$mes$dia
		   datas[$t]=$ano"-"$mes"-"$dia
		   let t=(t+1) 
      done
		   
  else
      
      POSTOS=( AVERMELHA MARIMBONDO CAMARGOS EDACUNHA FUNIL_MG FURNAS PCOLOMBIA 
	           ITAIPU CORUMBA1 CORUMBAIV 
	           EMBORCACAO ITUMBIARA NOVAPONTE SDOFACAO 
	           CANOASI CAPIVARA CHAVANTES JURUMIRIM MAUA ROSANA )
	  ID=( 18 17 1 15 211 6 12 266 209 205 24 31 25 251 52 61 49 47 57 63 ) 
	  TEXT=( "61998080|COO|DI|VNM|" "61941080|COO|DI|VNS|" "61065080|COO|DI|VIA|" "61818080|COO|DI|VIA|" 
	         "61146080|COO|DI|VNM|" "61661000|COO|DI|VNS|" "61796080|COO|DI|VNS|" "64918980|COO|DI|VNS|"
			 "60460000|COO|DI|VIA|" "60444000|COO|DI|VNA|" "60160080|COO|DI|VNM|" "60610080|COO|DI|VIA|"
			 "60330080|COO|DI|VNA|" "60035000|COO|DI|VIA|" "64345080|COO|DI|VIA|" "64516080|COO|DI|VNM|"
			 "64270080|COO|DI|VIA|" "64215080|COO|DI|VNA|" "64490080|COO|DI|VNA|" "64571080|COO|DI|VIA|" ) 
			 
      # echo $token
      # echo $numreg 
	  # echo ${datas[10]} 
      for k in `seq 0 19`
      do
           if [ "${ID[$k]}" == "$token" ] 
           then 
  			       echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		               linha=`cat .temps | cut -d$delim -f$n`
					   echo ${TEXT[$k]}${datas[$t]}" 00:00:00|"$linha >>${POSTOS[$k]}".txt"
                       let t=(t+1) 					   
		  
	               done	
           fi				   
	    done 		  
       			   
  fi 
  done < $qobsfile 

echo "==================>" ${camargos[10]}

