#!/bin/sh 




#
# calculo dias juliano 
#
diasjul()
{

ano=`date +"%Y"`
dias0=`convert_julian 1 1 $ano | cut -d"." -f1`
dias1=`convert_julian $1 $2 $3 |  cut -d"." -f1`
let diaj=(dias1-dias0+1)
echo $diaj
}


convert_julian()
{
#!/bin/sh
# convert Gregorian calendar date to Julian Day Number 
# convert Julian Day Number to Gregorian calendar date 
#
# algorithm source:
# http://quasar.as.utexas.edu/BillInfo/JulianDatesG.html
# 
# examples:
# $ ./script.sh 15 5 2013
# 2456428
# $ ./script.sh 2456428
# 15/5/2013
#
# contact:
# milosz@sleeplessbeastie.eu
#


# gtojdn
# convert Gregorian calendar date to Julian Day Number
#
# parameters:
# day
# month
# year
# 
# example:
# gtojdn 15 5 2013
#
gtojdn() {
  if [ $2 -le 2 ]; then
    y=$(($3 - 1))
    m=$(($2 + 12))
  else
    y=$3
    m=$2
  fi
  d=$1

  x=$(echo "2 - $y / 100 + $y / 400" | bc)
  x=$(echo "($x + 365.25 * ($y + 4716))/1" | bc) 
  x=$(echo "($x + 30.6001 * ($m + 1))/1" | bc)
  
  echo $(echo "($x + $d - 1524.5)" | bc)
}


# jdntog
# convert Julian Day Number to Gregorian calendar
#
# parameters:
# jdn
#
# example:
# jdntog 2456428
#
# notes:
# algorithm is simplified
# loses accuracy for years less than in 1582
#
jdntog() {
  z=$(echo "($1+0.5)" | bc)
  w=$(echo "(($z - 1867216.25)/36524.25)/1" | bc)
  x=$(echo "$w / 4" | bc)
  a=$(echo "$z + 1 + $w - $x" | bc)
  b=$(echo "$a + 1524" | bc)
  c=$(echo "(($b - 122.1) / 365.25)/1" | bc)
  d=$(echo "(365.25 * $c)/1" | bc)
  e=$(echo "(($b - $d) / 30.6001)/1" | bc)
  f=$(echo "(30.6001 * $e)/1" | bc)

  md=$(echo "($b - $d - $f)/1" | bc)
  if [ $e -le 13 ]; then
    m=$(echo "$e - 1" | bc)
  else
    m=$(echo "$e - 13" | bc)
  fi

  if [ $m -le 2 ]; then
    y=$(echo "$c - 4715" | bc)
  else
    y=$(echo "$c - 4716" | bc)
  fi
 
  echo "$md/$m/$y"
  if [ "$y" -lt 1582 ]; then
    echo "not accurate as year < 1582"
  fi
}


#
# process the command-line arguments
#
if [ "$#" -eq 1 ]; then
  jdntog $1
elif [ "$#" -eq 3 ]; then
  gtojdn $1 $2 $3
else
  d=`date +%d`
  m=`date +%m`
  y=`date +%Y`
  gtojdn $d $m $y
fi

}


contachar()
{

# letter-count2.sh: Counting letter occurrences in a text file.
#
# Script by nyal [nyal@voila.fr].
# Used in ABS Guide with permission.
# Recommented and reformatted by ABS Guide author.
# Version 1.1: Modified to work with gawk 3.1.3.
#              (Will still work with earlier versions.)


INIT_TAB_AWK=""
# Parameter to initialize awk script.
count_case=0
FILE_PARSE=$1

E_PARAMERR=85

usage()
{
    echo "Usage: letter-count.sh file letters" 2>&1
    # For example:   ./letter-count2.sh filename.txt a b c
    exit $E_PARAMERR  # Too few arguments passed to script.
}

if [ ! -f "$1" ] ; then
    echo "$1: No such file." 2>&1
    usage                 # Print usage message and exit.
fi 

if [ -z "$2" ] ; then
    echo "$2: No letters specified." 2>&1
    usage
fi 

shift                      # Letters specified.
for letter in `echo $@`    # For each one . . .
  do
  INIT_TAB_AWK="$INIT_TAB_AWK tab_search[${count_case}] = \
  \"$letter\"; final_tab[${count_case}] = 0; " 
  # Pass as parameter to awk script below.
  count_case=`expr $count_case + 1`
done

# DEBUG:
# echo $INIT_TAB_AWK;

cat $FILE_PARSE |
# Pipe the target file to the following awk script.

# ---------------------------------------------------------------------
# Earlier version of script:
# awk -v tab_search=0 -v final_tab=0 -v tab=0 -v \
# nb_letter=0 -v chara=0 -v chara2=0 \

awk \
"BEGIN { $INIT_TAB_AWK } \
{ split(\$0, tab, \"\"); \
for (chara in tab) \
{ for (chara2 in tab_search) \
{ if (tab_search[chara2] == tab[chara]) { final_tab[chara2]++ } } } } \
END { for (chara in final_tab) \
{ print  final_tab[chara] } }"
# ---------------------------------------------------------------------
#  Nothing all that complicated, just . . .
#+ for-loops, if-tests, and a couple of specialized functions.

return 

# Compare this script to letter-count.sh.

}





verificadir2()
{

#
#
#  Precisa de uma rodada do bat-smap.
#
# verifica o diretorio da ultima rodada do XONS e do SMAPONS
#  ao fim variavel LASTRUN= local da ultima rodada XONS 
#   e  LASTRUNSMAP= ultima rodada do smapons 
GRANDE=(AVERMELHA Camargos CAPESCURO EDACUNHA FUNIL_MG FURNAS MARIMBONDO PARAGUACU PASSAGEM PBUENOS PCOLOMBIA)
GRANDE2=(AVERMELHA CAMARGOS CAPESCURO EDACUNHA FUNIL_MG FURNAS MARIMBONDO PARAGUACU PASSAGEM PBUENOS PCOLOMBIA)

#
# Procura qual o diretorio mais atual que o XONS já rodou
# Procura qual diretorio onde foi feita a ultima rodada do app do ons (batsmap)
#
#
# vou verificar um arquivo de cada bacia , assumindo que se tem ele tem todos os demais
#
BACIA=( Grande Itaipu  Paranaiba Paranapanema)
SUBBACIA=( AVERMELHA ITAIPU CORUMBA1 CANOASI )
SUBBACIA2=( AVERMELHA Itaipu CORUMBA1 CanoasI )

#
# verifica se há alguam coisa na varivel ambiental DIRXONS
# se não coloca default 
#
if test -z $DIRXONS
then
DIRXONS="../../../OUTPUT/"
fi
#
# Apaga quando em wiindows a praga do desktop.ini 
#
rm $DIRXONS"Desktop.ini"  > /dev/null 2>&1 
#
#  lista os diretorios existentes. só os ultimos 7 e procura por uma rodada no batsmap 
#  se naão tiver , sai fora. 
#
echo "Utilizando o diretorio XONS localizado em:"$DIRXONS
echo `ls -1tr $DIRXONS/ | sort -r |  head -7` > listadirxons  
#
# procura pelas rodadas do BATSMAP 
# LASTRUN =  diretorio da ultima pasta com os arquivos para rodar o BATSMAP
# LASTRUNSMAP =  diretorio da ultima rodada  do batsmap ( onde tem diretorios ARQ_SAIDA) 
# 
for dire in `cat listadirxons`
do 
	check=0
	for i in `seq 0 3`
	do
		if test -e $DIRXONS$dire"/SMAPONS/"${BACIA[$i]}"/ARQ_ENTRADA/"${SUBBACIA[$i]}".txt" 
		then
			let check=($check+1)
		fi 
	done
		if test "$check" -eq 4 ;then 
			export LASTRUN=$DIRXONS$dire
			echo "Achado ultimo dortorio OUTPUT do XONS:"$DIRXONS$dire		
			break 
		fi
	done 
	for dire in `cat listadirxons`
    do 
	check2=0 
	for i in `seq 0 3`
	do
      if test -e $DIRXONS$dire"/SMAPONS/"${BACIA[$i]}"/ARQ_SAIDA/"${SUBBACIA2[$i]}"_AJUSTE.txt" 
		then
			let check2=($check2+1)
		fi 
    	if test "$check2" -eq 4 ;then 
		export LASTRUNSMAP=$DIRXONS$dire
		echo "Achado o ultimo diretorio com os arquivos do bat-smap:"$DIRXONS$dire
		break 
		fi
	done
done 


#
# se não encontra nada termina o processo 
#

if test -z  $LASTRUN ;then
echo "Não há diretorios com o SMAPONS. Terminando processo."
exit
fi 

if test -z  $LASTRUNSMAP ;then 
echo "Não há diretorios com uma rodada do  SMAPONS. Terminando processo."
exit
fi 




#
# verifcicar se o arquivo de vazoes esta atualzado 
#

if test -z $QOBSDIR 
then  
QOBSDIR="../../../OBS/QOBS/"
fi 
if test -z $QOBSFILE 
then  
QOBSFILE="vob_d.csv"
fi 

#
#  usando dos2unix para corrigir  erros de fim de linha
#
qobsfile=$QOBSDIR$QOBSFILE
dos2unix -R $qobsfile 
#
# calcula quantos registros (datas ) de vazoes existem 
#
contapv=`contachar  $qobsfile ";"`
contav=`contachar  $qobsfile ","`
#
# contapv conta se o arquivo é separado por ;
# contap conta se o arquivo é separado por ,
#
if test $contapv -gt 0 
then 
delim=";"
fi 
if test $contav -gt 0 
then 
delim=","
fi 

#
# pega o numero re registros 
#
head -1 $qobsfile > .temps 
numreg=`contachar  .temps $delim`
let numreg=(numreg+1)
#
# pega a ultima data do registro (datafim)
# dataok é a data anterior a data da rodada
# datadehoje é autoexplicativo (vai ser a data da rodada numa versao futura) 

if test -z $1 ;then 
datafim=`head -1 $qobsfile | cut -d$delim -f$numreg`
dataok=`date +"%d/%m/%Y" -d"1 days ago"`
datahoje=`date +"%d/%m/%Y"`
else
datafim=`head -1 $qobsfile | cut -d$delim -f$numreg`
let vg=($1+1)
dataok=`date +"%d/%m/%Y" -d"$vg days ago"` 
datahoje=`date +"%d/%m/%Y" -d"$1 days ago"`
fi
echo "Para data de hoje:"$datahoje" o BatSMAP precisa das vazoes de ontem:"$dataok
echo "Data da ultima vazão encontrada em:"$qobsfile " é "$datafim 





#
# cria copia dos arquivos fixos da ultima rodada feito do BATSMAP para a rodada atual  
#
GRANDE=(AVERMELHA CAMARGOS CAPESCURO EDACUNHA FUNIL_MG FURNAS MARIMBONDO PARAGUACU PASSAGEM PBUENOS PCOLOMBIA )
PANEMA=(CANOASI CAPIVARA CHAVANTES JURUMIRIM MAUA ROSANA) 
PARANAIBA=(SDOFACAO NOVAPONTE ITUMBIARA EMBORCACAO CORUMBAIV CORUMBA1) 
ITAIPU=(PTAQUARA IVINHEMA ITAIPU FLOR+ESTRA BALSA) 
ITENS=("" ETA40 EVAPOTRANSPIRACAO PARAMETROS POSTOS_PLU PRECMEDIA_ETA40.GEFS PRECZERADA_ETA40) 
BACIAS=( Grande Paranapanema Itaipu  Paranaiba )

if test  -z $1 ;then  
DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/"`
OLDSMAP=$LASTRUNSMAP"/SMAPONS/"
else
DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/" -d"$1 days ago"`
OLDSMAP=$LASTRUNSMAP"/SMAPONS/"
fi 
echo " Copiandoarquivos não mutaveis do batsmap de  "$OLDSMAP" para  "$DIRSMAP

#
# copiando o caso.txt  (que é meio diferente por causa do FUNIL MG.txt e FUNIL_MG.txt
# 
for k in `seq 0 3`
do
	cp $OLDSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/bat.conf'  $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA' 
	if [ $k = 0 ]
	then 
		##cp $OLDSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/caso.txt'  $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA' 
		cp  ./TEMPLATES/caso_grande.txt $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/caso.txt' 
	else
		cp $OLDSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/caso.txt'  $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA' 
	fi 
	cp $OLDSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/MODELOS_PRECIPITACAO.txt'  $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA' 
done 

#
# ONS coloca FUNIL MG.txt isso no linux provoca problemas. por isso copio tudo para FUNIL_MG.txt
#
 if  test -e  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_ETA40.txt" 
 then 
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_ETA40.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_ETA40.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_EVAPOTRANSPIRACAO.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_EVAPOTRANSPIRACAO.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_PARAMETROS.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_PARAMETROS.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_POSTOS_PLU.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_POSTOS_PLU.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_PRECMEDIA_ETA40.GEFS.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_PRECMEDIA_ETA40.GEFS.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_PRECZERADA_ETA40.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_PRECZERADA_ETA40.txt"
 fi 
 if  test -e  "$OLDSMAP/Grande/ARQ_SAIDA/FUNIL MG_AJUSTE.txt"
 then
     cp  "$OLDSMAP/Grande/ARQ_SAIDA/FUNIL MG_AJUSTE.txt" "$OLDSMAP/Grande/ARQ_SAIDA/FUNIL_MG_AJUSTE.txt"
 fi
 
 
#
# copios os arquivos fios da ultima rodado do smapons para a nova 
#
echo "Copiando deck para Grande"
for n in `seq  0 10`
do
file1=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_ETA40.txt'
file2=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_EVAPOTRANSPIRACAO.txt'
file3=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_PARAMETROS.txt'
file4=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_POSTOS_PLU.txt'
file5=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_PRECMEDIA_ETA40.GEFS.txt'
file6=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_PRECZERADA_ETA40.txt'
file11=$DIRSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_INC_BAT_ETA40.txt'
file22=$DIRSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_INC_BAT_EVAPOTRANSPIRACAO.txt'
file33=$DIRSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_INC_BAT_PARAMETROS.txt'
file44=$DIRSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_INC_BAT_POSTOS_PLU.txt'
file55=$DIRSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_INC_BAT_PRECMEDIA_ETA40.GEFS.txt'
file66=$DIRSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_INC_BAT_PRECZERADA_ETA40.txt'
cp $file1     $DIRSMAP"/Grande/ARQ_ENTRADA" 
cp $file2     $DIRSMAP"/Grande/ARQ_ENTRADA" 
cp $file3     $DIRSMAP"/Grande/ARQ_ENTRADA" 
cp $file4     $DIRSMAP"/Grande/ARQ_ENTRADA" 
cp $file5     $DIRSMAP"/Grande/ARQ_ENTRADA" 
cp $file6     $DIRSMAP"/Grande/ARQ_ENTRADA" 
cp $file1     $file11 
cp $file2     $file22 
cp $file3     $file33 
cp $file4     $file44 
cp $file5     $file55 
cp $file6     $file66 

done





echo "Copiando deck para Paranapanema"
for n in `seq  0 5`
do
file1=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_ETA40.txt'
file2=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_EVAPOTRANSPIRACAO.txt'
file3=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_PARAMETROS.txt'
file4=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_POSTOS_PLU.txt'
file5=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_PRECMEDIA_ETA40.GEFS.txt'
file6=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_PRECZERADA_ETA40.txt'
cp $file1 $file2 $file3 $file4 $file5 $file6    $DIRSMAP"/Paranapanema/ARQ_ENTRADA" 
done
echo "Copiando deck para Itaipu"
for n in `seq  0 4`
do
file1=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_ETA40.txt'
file2=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_EVAPOTRANSPIRACAO.txt'
file3=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_PARAMETROS.txt'
file4=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_POSTOS_PLU.txt'
file5=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_PRECMEDIA_ETA40.GEFS.txt'
file6=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_PRECZERADA_ETA40.txt'
file11=$DIRSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_INC_BAT_ETA40.txt'
file22=$DIRSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_INC_BAT_EVAPOTRANSPIRACAO.txt'
file33=$DIRSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_INC_BAT_PARAMETROS.txt'
file44=$DIRSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_INC_BAT_POSTOS_PLU.txt'
file55=$DIRSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_INC_BAT_PRECMEDIA_ETA40.GEFS.txt'
file66=$DIRSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_INC_BAT_PRECZERADA_ETA40.txt'


cp $file1 $file2 $file3 $file4 $file5 $file6    $DIRSMAP"/Itaipu/ARQ_ENTRADA" 
cp $file1     $file11 
cp $file2     $file22 
cp $file3     $file33 
cp $file4     $file44 
cp $file5     $file55 
cp $file6     $file66 

done






echo "Copiando deck para Paranaiba"
for n in `seq  0 5`
do
file1=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_ETA40.txt'
file2=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_EVAPOTRANSPIRACAO.txt'
file3=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_PARAMETROS.txt'
file4=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_POSTOS_PLU.txt'
file5=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_PRECMEDIA_ETA40.GEFS.txt'
file6=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_PRECZERADA_ETA40.txt'
cp $file1 $file2 $file3 $file4 $file5 $file6    $DIRSMAP"/Paranaiba/ARQ_ENTRADA" 
done


#
# SE DECKCOPY=1 copia o deck do batsmap para o diretorio DIRSMAP
#
if test $DECKCOPY -eq 1 ;then 
	for k in `seq 0 3`
	do
		cp ./TEMPLATES/batsmap-desktop.exe  $DIRSMAP"/"${BACIAS[$k]}"/"
		cp -r ./TEMPLATES/bin  $DIRSMAP"/"${BACIAS[$k]}"/"
		
	    mkdir -p  $DIRSMAP"/"${BACIAS[$k]}"/ARQ_SAIDA"
		mkdir -p  $DIRSMAP"/"${BACIAS[$k]}"/logs"
	done	
fi 			
		

#
#
# Cria arquivos iniciais _INICIALIZACAO
#
GRANDE=(AVERMELHA Camargos CAPESCURO EDACUNHA FUNIL_MG FURNAS MARIMBONDO PARAGUACU PASSAGEM PBUENOS PCOLOMBIA)
GRANDE2=(AVERMELHA CAMARGOS CAPESCURO EDACUNHA FUNIL_MG FURNAS MARIMBONDO PARAGUACU PASSAGEM PBUENOS PCOLOMBIA)

# if test -z $DIRXONS ;then 
# DIRXONS="../../../OUTPUT/"
# fi 

# DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/"`



# if test  -z $1 ;then  
# DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/"`
# OLDSMAP=$LASTRUNSMAP"/SMAPONS/"
# else
# DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/" -d"$1 days ago"`
# OLDSMAP=$LASTRUNSMAP"/SMAPONS/"
# fi 

echo "*"
echo " Copia dos arquivos de inicialização. "
echo " Parametros são lidos de:"$OLDSMAP
echo " E transferidos para    :"$DIRSMAP 

for n in `seq  0 10`
do
echo ${GRANDE2[$n]}"---------------------------------"

if test -z $1 ;then 
echo `date +"%d/%m/%Y"`" ' data da rodada" >${GRANDE2[$n]}"_INICIALIZACAO.txt"
else
echo `date +"%d/%m/%Y" -d"$1 days ago"`" ' data da rodada" >${GRANDE2[$n]}"_INICIALIZACAO.txt"
fi 
echo "32         ' dia de inicializacao do aquecimento do modelo"  >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
echo "12         ' numero de dias de previsao" >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
tu=`tac $OLDSMAP"/Grande/ARQ_SAIDA/"${GRANDE[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 2`
eb=`tac $OLDSMAP"/Grande/ARQ_SAIDA/"${GRANDE[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 3`
sup=`tac $OLDSMAP"/Grande/ARQ_SAIDA/"${GRANDE[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 4`
echo $eb"          ' ebin"    >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
echo $sup"         ' supin"  >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
echo $tu"          ' tuin"    >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
done 

cp FURNAS_INICIALIZACAO.txt FURNAS_INC_BAT_INICIALIZACAO.txt 
cp PCOLOMBIA_INICIALIZACAO.txt PCOLOMBIA_INC_BAT_INICIALIZACAO.txt 
cp MARIMBONDO_INICIALIZACAO.txt MARIMBONDO_INC_BAT_INICIALIZACAO.txt 
mv *INICIALIZACAO.txt  $DIRSMAP"/Grande/ARQ_ENTRADA"  

ITAIPU=(PTaquara Ivinhema Itaipu Flor+Estra Balsa)
ITAIPU2=(PTAQUARA IVINHEMA ITAIPU FLOR+ESTRA BALSA) 

for n in `seq  0 4`
do
if test -z $1 ;then 
echo `date +"%d/%m/%Y"`" ' data da rodada" >${ITAIPU2[$n]}"_INICIALIZACAO.txt"
else
echo `date +"%d/%m/%Y" -d"$1 days ago"`" ' data da rodada" >${ITAIPU2[$n]}"_INICIALIZACAO.txt"
fi 
echo "32         ' dia de inicializacao do aquecimento do modelo"  >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
echo "12         ' numero de dias de previsao" >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
tu=`tac $OLDSMAP"/Itaipu/ARQ_SAIDA/"${ITAIPU[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 2`
eb=`tac $OLDSMAP"/Itaipu/ARQ_SAIDA/"${ITAIPU[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 3`
sup=`tac $OLDSMAP"/Itaipu/ARQ_SAIDA/"${ITAIPU[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 4`
echo $eb"          ' ebin"    >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
echo $sup"         ' supin"  >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
echo $tu"          ' tuin"    >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
done 
cp ITAIPU_INICIALIZACAO.txt ITAIPU_INC_BAT_INICIALIZACAO.txt 
mv *INICIALIZACAO.txt  $DIRSMAP"/Itaipu/ARQ_ENTRADA"

PARANAIBA=(SDOFACAO NOVAPONTE ITUMBIARA EMBORCACAO CORUMBAIV CORUMBA1) 
for n in `seq  0 5`
do
if test -z $1 ;then 
echo `date +"%d/%m/%Y"`" ' data da rodada" >${PARANAIBA[$n]}"_INICIALIZACAO.txt"
else
echo `date +"%d/%m/%Y" -d"$1 days ago"`" ' data da rodada" >${PARANAIBA[$n]}"_INICIALIZACAO.txt"
fi 
echo "32         ' dia de inicializacao do aquecimento do modelo"  >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
echo "12         ' numero de dias de previsao" >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
tu=`tac $OLDSMAP"/Paranaiba/ARQ_SAIDA/"${PARANAIBA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 2`
eb=`tac $OLDSMAP"/Paranaiba/ARQ_SAIDA/"${PARANAIBA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 3`
sup=`tac $OLDSMAP"/Paranaiba/ARQ_SAIDA/"${PARANAIBA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 4`
echo $eb"          ' ebin"    >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
echo $sup"         ' supin"  >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
echo $tu"          ' tuin"    >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
done 
mv *INICIALIZACAO.txt  $DIRSMAP"/Paranaiba/ARQ_ENTRADA"

PANEMA=(Rosana Maua Jurumirim Chavantes Capivara CanoasI)
PANEMA2=(CANOASI CAPIVARA CHAVANTES JURUMIRIM MAUA ROSANA) 
for n in `seq  0 5`
do
echo `date +"%d/%m/%Y"`" ' data da rodada" >${PANEMA2[$n]}"_INICIALIZACAO.txt"
if test -z $1 ;then 
echo `date +"%d/%m/%Y"`" ' data da rodada" >${PANEMA2[$n]}"_INICIALIZACAO.txt"
else
echo `date +"%d/%m/%Y" -d"$1 days ago"`" ' data da rodada" >${PANEMA2[$n]}"_INICIALIZACAO.txt"
fi 
echo "32         ' dia de inicializacao do aquecimento do modelo"  >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
echo "12         ' numero de dias de previsao" >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
tu=`tac $OLDSMAP"/Paranapanema/ARQ_SAIDA/"${PANEMA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 2`
eb=`tac $OLDSMAP"/Paranapanema/ARQ_SAIDA/"${PANEMA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 3`
sup=`tac $OLDSMAP"/Paranapanema/ARQ_SAIDA/"${PANEMA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 4`
echo $eb"          ' ebin"    >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
echo $sup"         ' supin"  >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
echo $tu"          ' tuin"    >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
done 
mv *INICIALIZACAO.txt  $DIRSMAP"/Paranapanema/ARQ_ENTRADA"
























# se dataok e datafim estao corretos
#
if test  "$dataok" = "$datafim"
then 
echo " SMAPONS SERA RODADO COM ARQUIVO DE VAZOES ATUALIZADO."
export QOBSCHECK=1
return
fi 


#
# como arquivo de vazoes não foi atualizado 
#
# Atualiza o arquivo de vazoes VOBS com a  vazão  prevista"
#
POSTOS=( AVERMELHA MARIMBONDO CAMARGOS EDACUNHA FUNIL_MG FURNAS PCOLOMBIA 
	           ITAIPU 
			   CORUMBA1 CORUMBAIV EMBORCACAO ITUMBIARA NOVAPONTE SDOFACAO 
	           CANOASI CAPIVARA CHAVANTES JURUMIRIM MAUA ROSANA 
			   FURNAS_INC MARIMBONDO_INC PCOLOMBIA_INC ITAIPU_INC  )
BACIAS=( Grande Grande Grande Grande Grande Grande Grande Itaipu Paranaiba Paranaiba Paranaiba 
	  Paranaiba Paranaiba Paranaiba Paranapanema Paranapanema Paranapanema Paranapanema Paranapanema Paranapanema
	  Grande Grande Grande Itaipu) 	  
ID=( 18 17 1 15 211 6 12 266 209 205 24 31 25 251 52 61 49 47 57 63 996 917 912 966 ) 

#
# antes tenhoque verificar quais datas eu tenho 
#  que pegar do previsto   $datafim 
#
# Data       Qcal
# 05/07/2018 000133.12 
# 06/07/2018 000132.43 
# 07/07/2018 000131.75 
# 08/07/2018 000131.07 
# 09/07/2018 000130.41 
# 10/07/2018 000129.75 
# 11/07/2018 000129.10 
# 12/07/2018 000128.46 
# 13/07/2018 000127.84 
# 14/07/2018 000127.20 
# 15/07/2018 000126.57 
# 16/07/2018 000125.98 
#
#
# pego as datas da ultima vazao 
#
ano0=`echo $datafim | cut -d"/" -f3`
mes0=`echo $datafim | cut -d"/" -f2`
dia0=`echo $datafim | cut -d"/" -f1`
diaj0=`diasjul $dia0 $mes0 $ano0`

#echo $ano0$mes0$dia0$diaj0
#
# pego a data de hoje
# e calculo em dias juianos
#
if test -z $1 ;then 
ano1=`date +"%Y" `
mes1=`date +"%m" `
dia1=`date +"%d" `
diaj1=`diasjul $dia1 $mes1 $ano1`
else
ano1=`date +"%Y" -d"$1 day ago"`
mes1=`date +"%m" -d"$1 day ago"`
dia1=`date +"%d" -d"$1 day ago"`
diaj1=`diasjul $dia1 $mes1 $ano1`
fi 
#echo $ano1$mes1$dia1$diaj1

#
# calcula dias que faltam ou que sobram para a data correta
#
let  dt=($diaj1-$diaj0)-1 
#echo $dt
#
# codigo = -9999 quer dizer que smapons não vai ser executado. para por aqui. 
#
if test $dt -gt 7  
then 
echo "Arquivo de vazões muito desatualizado.  "
echo "Favor atualizar e rodar novamente."
exit
fi 

#
# arquivo de vazao está no futuro em relaçao a data de hoje
#
if test $dt -lt 0 
then
echo $dt 
echo " ARquivo de vazões com datas futuras."
echo " Ajuste o arquivo de vazões para a data correta"
exit
fi






let k=0
while IFS= read -r var 
do
	token=`echo $var | cut -d"/" -f3 | cut -d" " -f1` 
	datax=`echo $var | cut -d" " -f1`
	vazao=`echo $var | cut -d" " -f2`

	if test "$token" = "Data" ;then 
	echo "process" 
	else
		dia=`echo $var | cut -d"/" -f1` 
		mes=`echo $var | cut -d"/" -f2` 
		ano=`echo $var | cut -d"/" -f3 | cut -d" " -f1` 
	fi    

	diaj=`diasjul $dia $mes $ano`

	if test $diaj  -gt $diaj0 -a  $diaj -lt $diaj1 ;then 

		gdates[$k]=$datax
		let k=(k+1)
#		echo "intervalo esta entre as data que eu quero"$diaj0" "$diaj" "$diaj1" "$datax" "$vazao
	fi 
done < $LASTRUNSMAP"/SMAPONS/"${BACIAS[0]}"/ARQ_SAIDA/"${POSTOS[0]}"_ETA40_LIMITES_PREVISAO.txt"  
for n in `seq 0 $k `
do
	novasdatas=$novasdatas$delim${gdatas[$n]} 
	novasvazoes=$novasvazoes$delim${gvazoes[$n]} 
done





for j in `seq 0 23`
do
    gvazoes=""
	xvazoes=""
	let k=0
	while IFS= read -r var 
	do
		#if  test  $var -eq "Data       Qcal" 
		token=`echo $var | cut -d"/" -f3 | cut -d" " -f1` 
		datax=`echo $var | cut -d" " -f1`
		vazao=`echo $var | cut -d" " -f2`

		if test "$token" = "Data" ;then 
			echo " process"
		else
			dia=`echo $var | cut -d"/" -f1` 
			mes=`echo $var | cut -d"/" -f2` 
			ano=`echo $var | cut -d"/" -f3 | cut -d" " -f1` 
		fi    
		diaj=`diasjul $dia $mes $ano`

		if test $diaj  -gt $diaj0 -a  $diaj -lt $diaj1 ;then 

			##gdates[$k]=$datax
			gvazoes[$k]=$vazao 
			let k=(k+1)
			#echo "intervalo esta entre as data que eu quero"$diaj0" "$diaj" "$diaj1" "$datax" "$vazao
		fi 
	done < $LASTRUNSMAP"/SMAPONS/"${BACIAS[$j]}"/ARQ_SAIDA/"${POSTOS[$j]}"_ETA40_LIMITES_PREVISAO.txt"  
	for n in `seq 0 $k `
	do
		xvazoes=$xvazoes$delim${gvazoes[$n]} 
	done
    novasvazoes[$j]=$xvazoes  
	echo $j" "${novasvazoes[$j]} 
done 









    if test -z $1 ;then
    vobsalter="vobs_xons_"`date +"%Y%m%d$ROD"`".csv"
	else
	vobsalter="vobs_xons_"`date +"%Y%m%d$ROD" -d"$1 day ago"`".csv"
	fi
	
	export QOBSFILE=$vobsalter 
	echo " Usando para testes arquivo adaptado repetindo ultimas vazões:"$vobsalter
echo "  adaptando  VOBS "
rm $QOBSDIR$QOBSFILE

while IFS= read -r var 
do
	token=`echo $var | cut -d$delim -f1`
	#echo $var
	if [ "$token" ==  "Posto" ]
	then
	    echo "posto"
		echo $var$novasdatas >> $QOBSDIR$QOBSFILE
    fi
	for k in `seq 0 23`
	do
	    #echo ${ID[$k]} 
		if test "$token" =  "${ID[$k]}" ;then 
		    #echo  ${ID[$k]} 
     		echo $var""${novasvazoes[$k]} >> $QOBSDIR$QOBSFILE 
			
		fi
	done	
done < $qobsfile 


exit 



#
# atualiza o arquivo de VOBS repetindo as datas até a data que precisa 
#  usado m teetes
#
#
if test $dt -le 7 -a $dt -gt 0 -a $VOBSFILECHECK -eq 1  ;then
    vobsalter="vobs_xons_"`date +"%Y%m%d$ROD"`".csv"
	export QOBSFILE=$vobsalter 
	echo " Usando para testes arquivo adaptado repetindo ultimas vazões:"$vobsalter
	#
	# matriz de datas (até 7 dias ) 
	# 
	if test -z $1 ;then 
	gdatas=( `date +"%d/%m/%Y "`                   `date +"%d/%m/%Y" -d"1 day ago"`  `date +"%d/%m/%Y" -d"2 day ago"`  
       `date +"%d/%m/%Y" -d"3 day ago"`     `date +"%d/%m/%Y" -d"4 day ago"`   `date +"%d/%m/%Y" -d"5 day ago"` 
       `date +"%d/%m/%Y" -d"6 day ago"`     `date +"%d/%m/%Y" -d"7 day ago"`  )  
	else
	
	
     gdatas=( `date +"%d/%m/%Y"`                   `date +"%d/%m/%Y" -d"1 day ago"`  `date +"%d/%m/%Y" -d"2 day ago"`  
       `date +"%d/%m/%Y" -d"3 day ago"`     `date +"%d/%m/%Y" -d"4 day ago"`   `date +"%d/%m/%Y" -d"5 day ago"` 
       `date +"%d/%m/%Y" -d"6 day ago"`     `date +"%d/%m/%Y" -d"7 day ago"`  )  
	
    fi	
	 
	for n in `seq $dt -1 1`
	do
		##echo $n"---->"${gdatas[$n]} 
		novasdatas=$novasdatas$delim${gdatas[$n]} 
	done
	###echo $novasdatas
	rm $QOBSDIR$QOBSFILE 	2>&1   
	while IFS= read -r var 
	do
		newvar=""
		#echo $var 
		token=`echo $var | cut -d$delim -f1`
		#echo $token 
		if [ "$token" ==  "Posto" ]
			then 
				echo $var$novasdatas >> $QOBSDIR$QOBSFILE
			else 
				for k in `seq $dt -1 1`
				do
					vazao=`echo $var | cut -d$delim -f$numreg` 
	  
					newvar=$newvar$delim$vazao
	
				done
				echo $var$newvar >> $QOBSDIR$QOBSFILE 
		fi
	done < $qobsfile 
fi 
}


#
# cria arquivos iniciais 
# para o smapons 
#
criaarqinic()
{
GRANDE=(AVERMELHA Camargos CAPESCURO EDACUNHA FUNIL_MG FURNAS MARIMBONDO PARAGUACU PASSAGEM PBUENOS PCOLOMBIA)
GRANDE2=(AVERMELHA CAMARGOS CAPESCURO EDACUNHA FUNIL_MG FURNAS MARIMBONDO PARAGUACU PASSAGEM PBUENOS PCOLOMBIA)

if test -z $DIRXONS ;then 
DIRXONS="../../../OUTPUT/"
fi 

DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/"`



if test  -z $1 ;then  
DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/"`
OLDSMAP=$LASTRUNSMAP"/SMAPONS/"
else
DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/" -d"$1 days ago"`
OLDSMAP=$LASTRUNSMAP"/SMAPONS/"
fi 



for n in `seq  0 10`
do
echo ${GRANDE2[$n]}"---------------------------------"

if test -z $1 ;then 
echo `date +"%d/%m/%Y"`" ' data da rodada" >${GRANDE2[$n]}"_INICIALIZACAO.txt"
else
echo `date +"%d/%m/%Y" -d"$1 days ago"`"' data da rodada" >${GRANDE2[$n]}"_INICIALIZACAO.txt"
fi 
echo "32         ' dia de inicializacao do aquecimento do modelo"  >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
echo "12         ' numero de dias de previsao" >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
tu=`tac $OLDSMAP"/Grande/ARQ_SAIDA/"${GRANDE[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 2`
eb=`tac $OLDSMAP"/Grande/ARQ_SAIDA/"${GRANDE[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 3`
sup=`tac $OLDSMAP"/Grande/ARQ_SAIDA/"${GRANDE[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 4`
echo $eb"          ' ebin"    >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
echo $sup"         ' supin"  >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
echo $tu"          ' tuin"    >>${GRANDE2[$n]}"_INICIALIZACAO.txt"
done 

cp FURNAS_INICIALIZACAO.txt FURNAS_INC_BAT_INICIALIZACAO.txt 
cp PCOLOMBIA_INICIALIZACAO.txt FURNAS_INC_BAT_INICIALIZACAO.txt 
cp MARIMBONDO_INICIALIZACAO.txt FURNAS_INC_BAT_INICIALIZACAO.txt 
mv *INICIALIZACAO.txt  $DIRSMAP"/Grande/ARQ_ENTRADA"  

ITAIPU=(PTaquara Ivinhema Itaipu Flor+Estra Balsa)
ITAIPU2=(PTAQUARA IVINHEMA ITAIPU FLOR+ESTRA BALSA) 

for n in `seq  0 4`
do
if test -z $1 ;then 
echo `date +"%d/%m/%Y"`" ' data da rodada" >${ITAIPU2[$n]}"_INICIALIZACAO.txt"
else
echo `date +"%d/%m/%Y" -d"$1 days ago"`"' data da rodada" >${ITAIPU2[$n]}"_INICIALIZACAO.txt"
fi 
echo "32         ' dia de inicializacao do aquecimento do modelo"  >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
echo "12         ' numero de dias de previsao" >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
tu=`tac $OLDSMAP"/Itaipu/ARQ_SAIDA/"${ITAIPU[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 2`
eb=`tac $OLDSMAP"/Itaipu/ARQ_SAIDA/"${ITAIPU[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 3`
sup=`tac $OLDSMAP"/Itaipu/ARQ_SAIDA/"${ITAIPU[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 4`
echo $eb"          ' ebin"    >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
echo $sup"         ' supin"  >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
echo $tu"          ' tuin"    >>${ITAIPU2[$n]}"_INICIALIZACAO.txt"
done 
cp ITAIPU_INICIALIZACAO.txt ITAIPU_INC_BAT_INICIALIZACAO.txt 
mv *INICIALIZACAO.txt  $DIRSMAP"/Itaipu/ARQ_ENTRADA"

PARANAIBA=(SDOFACAO NOVAPONTE ITUMBIARA EMBORCACAO CORUMBAIV CORUMBA1) 
for n in `seq  0 5`
do
if test -z $1 ;then 
echo `date +"%d/%m/%Y"`" ' data da rodada" >${PARANAIBA[$n]}"_INICIALIZACAO.txt"
else
echo `date +"%d/%m/%Y" -d"$1 days ago"`"' data da rodada" >${PARANAIBA[$n]}"_INICIALIZACAO.txt"
fi 
echo "32         ' dia de inicializacao do aquecimento do modelo"  >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
echo "12         ' numero de dias de previsao" >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
tu=`tac $OLDSMAP"/Paranaiba/ARQ_SAIDA/"${PARANAIBA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 2`
eb=`tac $OLDSMAP"/Paranaiba/ARQ_SAIDA/"${PARANAIBA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 3`
sup=`tac $OLDSMAP"/Paranaiba/ARQ_SAIDA/"${PARANAIBA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 4`
echo $eb"          ' ebin"    >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
echo $sup"         ' supin"  >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
echo $tu"          ' tuin"    >>${PARANAIBA[$n]}"_INICIALIZACAO.txt"
done 
mv *INICIALIZACAO.txt  $DIRSMAP"/Paranaiba/ARQ_ENTRADA"

PANEMA=(Rosana Maua Jurumirim Chavantes Capivara CanoasI)
PANEMA2=(CANOASI CAPIVARA CHAVANTES JURUMIRIM MAUA ROSANA) 
for n in `seq  0 5`
do
echo `date +"%d/%m/%Y"`" ' data da rodada" >${PANEMA2[$n]}"_INICIALIZACAO.txt"
if test -z $1 ;then 
echo `date +"%d/%m/%Y"`" ' data da rodada" >${PANEMA2[$n]}"_INICIALIZACAO.txt"
else
echo `date +"%d/%m/%Y" -d"$1 days ago"`"' data da rodada" >${PANEMA2[$n]}"_INICIALIZACAO.txt"
fi 
echo "32         ' dia de inicializacao do aquecimento do modelo"  >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
echo "12         ' numero de dias de previsao" >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
tu=`tac $OLDSMAP"/Paranapanema/ARQ_SAIDA/"${PANEMA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 2`
eb=`tac $OLDSMAP"/Paranapanema/ARQ_SAIDA/"${PANEMA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 3`
sup=`tac $OLDSMAP"/Paranapanema/ARQ_SAIDA/"${PANEMA[$n]}"_AJUSTE.txt" | head -31 | tail -1 | cut -d" " -f 4`
echo $eb"          ' ebin"    >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
echo $sup"         ' supin"  >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
echo $tu"          ' tuin"    >>${PANEMA2[$n]}"_INICIALIZACAO.txt"
done 
mv *INICIALIZACAO.txt  $DIRSMAP"/Paranapanema/ARQ_ENTRADA"

}



#
#
#  faz a copia da ultima rodada do smapons
#  so copia os que são fixos 
#
copiatemplateons()
{
GRANDE=(AVERMELHA CAMARGOS CAPESCURO EDACUNHA FUNIL_MG FURNAS MARIMBONDO PARAGUACU PASSAGEM PBUENOS PCOLOMBIA)
PANEMA=(CANOASI CAPIVARA CHAVANTES JURUMIRIM MAUA ROSANA) 
PARANAIBA=(SDOFACAO NOVAPONTE ITUMBIARA EMBORCACAO CORUMBAIV CORUMBA1) 
ITAIPU=(PTAQUARA IVINHEMA ITAIPU FLOR+ESTRA BALSA) 

ITENS=("" ETA40 EVAPOTRANSPIRACAO PARAMETROS POSTOS_PLU PRECMEDIA_ETA40.GEFS PRECZERADA_ETA40) 

if test -z $DIRXONS ;then 
DIRXONS="../../../OUTPUT/"
fi 

if test  -z $1 ;then  
DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/"`
OLDSMAP=$LASTRUN"/SMAPONS/"
else
DIRSMAP=$DIRXONS`date +"%Y%m%d00/SMAPONS/" -d"$1 days ago"`
OLDSMAP=$LASTRUNSMAP"/SMAPONS/"
fi 
echo " Copiandoarquivos não mutaveis do batsmap de  "$OLDSMAP" para  "$DIRSMAP
BACIAS=( Grande Paranapanema Itaipu  Paranaiba )



for k in `seq 0 3`
do
cp $OLDSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/bat.conf'  $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA' 

if [ $k = 0 ]
then 
##cp $OLDSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/caso.txt'  $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA' 
cp  ./TEMPLATES/caso_grande.txt $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/caso.txt' 
else
cp $OLDSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/caso.txt'  $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA' 
fi 


cp $OLDSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA/MODELOS_PRECIPITACAO.txt'  $DIRSMAP'/'${BACIAS[$k]}'/ARQ_ENTRADA' 

done 

 
 # ls  "../../../OUTPUT/2018051700/SMAPONS//Grande/ARQ_ENTRADA/FUNIL MG.txt"
 # ls  "../../../OUTPUT/2018051700/SMAPONS//Grande/ARQ_ENTRADA/AVERMELHA.txt"

 if  test -e  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_ETA40.txt" 
 then 
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_ETA40.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_ETA40.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_EVAPOTRANSPIRACAO.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_EVAPOTRANSPIRACAO.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_PARAMETROS.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_PARAMETROS.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_POSTOS_PLU.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_POSTOS_PLU.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_PRECMEDIA_ETA40.GEFS.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_PRECMEDIA_ETA40.GEFS.txt"
 cp  "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL MG_PRECZERADA_ETA40.txt" "$OLDSMAP/Grande/ARQ_ENTRADA/FUNIL_MG_PRECZERADA_ETA40.txt"
 fi 
 
 if  test -e  "$OLDSMAP/Grande/ARQ_SAIDA/FUNIL MG_AJUSTE.txt"
then
cp  "$OLDSMAP/Grande/ARQ_SAIDA/FUNIL MG_AJUSTE.txt" "$OLDSMAP/Grande/ARQ_SAIDA/FUNIL_MG_AJUSTE.txt"
 fi
 

for n in `seq  0 10`
do
# if [ "$n" = "4" ] 
# then 

# file1=$OLDSMAP'"/Grande/ARQ_ENTRADA/FUNIL MG_ETA40.txt"'
# file2=$OLDSMAP'"/Grande/ARQ_ENTRADA/FUNIL MG_EVAPOTRANSPIRACAO.txt"'
# file3=$OLDSMAP'"/Grande/ARQ_ENTRADA/FUNIL MG_PARAMETROS.txt"'
# file4=$OLDSMAP'"/Grande/ARQ_ENTRADA/FUNIL MG_POSTOS_PLU.txt"'
# file5=$OLDSMAP'"/Grande/ARQ_ENTRADA/FUNIL MG_PRECMEDIA_ETA40.GEFS.txt"'
# file6=$OLDSMAP'"/Grande/ARQ_ENTRADA/FUNIL MG_PRECZERADA_ETA40.txt"'
# cp $file1 $file2 $file3 $file4 $file5 $file6    $DIRSMAP"/Grande/ARQ_ENTRADA" 
# else
file1=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_ETA40.txt'
file2=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_EVAPOTRANSPIRACAO.txt'
file3=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_PARAMETROS.txt'
file4=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_POSTOS_PLU.txt'
file5=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_PRECMEDIA_ETA40.GEFS.txt'
file6=$OLDSMAP'/Grande/ARQ_ENTRADA/'${GRANDE[$n]}'_PRECZERADA_ETA40.txt'
cp $file1 $file2 $file3 $file4 $file5 $file6    $DIRSMAP"/Grande/ARQ_ENTRADA" 



done

for n in `seq  0 5`
do
file1=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_ETA40.txt'
file2=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_EVAPOTRANSPIRACAO.txt'
file3=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_PARAMETROS.txt'
file4=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_POSTOS_PLU.txt'
file5=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_PRECMEDIA_ETA40.GEFS.txt'
file6=$OLDSMAP'/Paranapanema/ARQ_ENTRADA/'${PANEMA[$n]}'_PRECZERADA_ETA40.txt'
cp $file1 $file2 $file3 $file4 $file5 $file6    $DIRSMAP"/Paranapanema/ARQ_ENTRADA" 
done

for n in `seq  0 4`
do
file1=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_ETA40.txt'
file2=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_EVAPOTRANSPIRACAO.txt'
file3=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_PARAMETROS.txt'
file4=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_POSTOS_PLU.txt'
file5=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_PRECMEDIA_ETA40.GEFS.txt'
file6=$OLDSMAP'/Itaipu/ARQ_ENTRADA/'${ITAIPU[$n]}'_PRECZERADA_ETA40.txt'
cp $file1 $file2 $file3 $file4 $file5 $file6    $DIRSMAP"/Itaipu/ARQ_ENTRADA" 
done

for n in `seq  0 5`
do
file1=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_ETA40.txt'
file2=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_EVAPOTRANSPIRACAO.txt'
file3=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_PARAMETROS.txt'
file4=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_POSTOS_PLU.txt'
file5=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_PRECMEDIA_ETA40.GEFS.txt'
file6=$OLDSMAP'/Paranaiba/ARQ_ENTRADA/'${PARANAIBA[$n]}'_PRECZERADA_ETA40.txt'
cp $file1 $file2 $file3 $file4 $file5 $file6    $DIRSMAP"/Paranaiba/ARQ_ENTRADA" 
done






return 




}










levazoes() 
{

POSTOS=( AVERMELHA MARIMBONDO CAMARGOS EDACUNHA FUNIL_MG FURNAS PCOLOMBIA 
          ITAIPU 
	   CORUMBA1 CORUMBAIV EMBORCACAO ITUMBIARA NOVAPONTE SDOFACAO 
         CANOASI CAPIVARA CHAVANTES JURUMIRIM MAUA ROSANA 
   FURNAS_INC MARIMBONDO_INC PCOLOMBIA_INC ITAIPU_INC  )
BACIAS=( Grande Grande Grande Grande Grande Grande Grande Itaipu Paranaiba Paranaiba Paranaiba 
  Paranaiba Paranaiba Paranaiba Paranapanema Paranapanema Paranapanema Paranapanema Paranapanema Paranapanema
  Grande Grande Grande Itaipu) 	  
  ID=( 18 17 1 15 211 6 12 266 209 205 24 31 25 251 52 61 49 47 57 63 996 917 912 966 ) 
	  
  TEXT=( "61998080|COO|DI|VNM|" "61941080|COO|DI|VNS|" "61065080|COO|DI|VIA|" "61818080|COO|DI|VIA|" 
         "61146080|COO|DI|VNM|" "61661000|COO|DI|VNS|" "61796080|COO|DI|VNS|" "64918980|COO|DI|VNS|"
		 "60460000|COO|DI|VIA|" "60444000|COO|DI|VNA|" "60160080|COO|DI|VNM|" "60610080|COO|DI|VIA|"
		 "60330080|COO|DI|VNA|" "60035000|COO|DI|VIA|" "64345080|COO|DI|VIA|" "64516080|COO|DI|VNM|"
		 "64270080|COO|DI|VIA|" "64215080|COO|DI|VNA|" "64490080|COO|DI|VNA|" "64571080|COO|DI|VIA|"
		 "61661000|COO|DI|VIC|" "61941080|COO|DI|VIC|" "61796080|COO|DI|VIC|" "64918980|COO|DI|VIC|" ) 
			 
	  


#
#  le o arquivo de vazoes 
#
#
if test -z $DIRXONS ;then 
DIRXONS="../../../OUTPUT/"
fi 
if  test -z $1 ;then 
DIRSMAP=$DIRXONS`date +"%Y%m%d$ROD/SMAPONS/"`
else
DIRSMAP=$DIRXONS`date +"%Y%m%d$ROD/SMAPONS/" -d"$1 day ago"`
fi 
qobsfile=$QOBSDIR$QOBSFILE
echo "Leitura do arquivo de vazões: "$qobsfile 
echo "Diretorio SMAP              : "$DIRSMAP 

#
# delimitadores do arquivo de vazoes
#
contapv=`contachar  $qobsfile ";"`
contav=`contachar  $qobsfile ","`
#
#  teste dos delimitadores
#
if test $contapv -gt 0 
then 
delim=";"
fi 
if test $contav -gt 0 
then 
delim=","
fi 
#
#  leitura das vazoes. procura por l1 c1 = POSTO 
#
let newreg=-999
let lastreg=-999
let lastreg2=-999
echo "Lendo arquivo de vazões:"$qobsfile 
while IFS= read -r var 
do
  #echo $var
  token=`echo $var | cut -d$delim -f1`
  if [ "$token" ==  "Posto" ]
  then 
      #echo "Pegando linha com datas"  
      DATAS=$var 
	  echo $DATAS > .temps
      numreg=`contachar  .temps $delim`
	  let numreg=(numreg+1)
	  let lastreg=(numreg-44) 	
      let lastreg2=(numreg-49) 	  
	  #
	  # teste se roda hoje ou para trás
	  #
	  if test -z $1 ;then 
		 datafim=`head -1 $qobsfile | cut -d$delim -f$numreg`
	     dataok=`date +"%d/%m/%Y" -d"1 days ago"`
         datahoje=`date +"%d/%m/%Y"`
      else
		datafim=`head -1 $qobsfile | cut -d$delim -f$numreg`
		let vg=($1+1)
		dataok=`date +"%d/%m/%Y" -d"$vg days ago"` 
		datahoje=`date +"%d/%m/%Y" -d"$1 days ago"`
	 fi
     #  dataok=`date +"%d/%m/%Y" -d"1 days ago"` 
     #  datafim=`head -1 $qobsfile | cut -d$delim -f$numreg`
     echo "Data final arquivo de vazoes            :"$datafim 
	 echo "Data para processo do  arquivo de vazoes:"$datafim 
     t=0
     for n in `seq $lastreg $numreg`
	 do
		   ano=`cat .temps | cut -d$delim -f$n | cut -d"/" -f3`
		   mes=`cat .temps | cut -d$delim -f$n | cut -d"/" -f2`
		   dia=`cat .temps | cut -d$delim -f$n | cut -d"/" -f1`
		   datas[$t]=$ano"-"$mes"-"$dia
		   let t=(t+1) 
     done
	 t=0
	 for n in `seq $lastreg2 $numreg`
	 do
		ano=`cat .temps | cut -d$delim -f$n | cut -d"/" -f3`
		mes=`cat .temps | cut -d$delim -f$n | cut -d"/" -f2`
		dia=`cat .temps | cut -d$delim -f$n | cut -d"/" -f1`
	    datas2[$t]=$ano"-"$mes"-"$dia
		let t=(t+1) 
    done

  else
  if test $numreg -eq -9999 ;then 
     echo " Erro no arquivo de Vobs_d.csv Possivel falta do nome Posto na celula A1"	 
     exit
  fi 	 
      # echo $token
      # echo $numreg 
	  # echo ${datas[10]} 
      for k in `seq 0 23`
      do
           if [ "${ID[$k]}" == "$token" ] 
           then 
		           echo "=============>"$token" "${ID[$k]}" "${POSTOS[$k]}
		           if [ "$token" == "266" ] ||  [ "$token" == "966" ]
				   
				   then 
				       echo $var > .temps
				   t=0
				   let newreg=$lastreg-5
				   echo $newreg";"$lastreg";"$lastreg2
				   
                   for n in `seq $newreg $numreg`
	               do
				      # echo $n 
		               linha=`cat .temps | cut -d$delim -f$n`
					   echo ${TEXT[$k]}${datas2[$t]}" 00:00:00|"$linha >>${POSTOS[$k]}".txt"
                       let t=(t+1) 					   
		  
	               done
				   else
  			       echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		               linha=`cat .temps | cut -d$delim -f$n`
					   echo ${TEXT[$k]}${datas[$t]}" 00:00:00|"$linha >>${POSTOS[$k]}".txt"
                       let t=(t+1) 					   
		  
	               done
                  fi 				   
           fi				   
	    done 		  
       			   
  fi 
  done < $qobsfile 

echo "finalziação" 
for n in `seq  0 23`
do
mv ${POSTOS[$n]}".txt" $DIRSMAP'/'${BACIAS[$n]}'/ARQ_ENTRADA/'  
done 



}



#
#
#  chama script do DAVID 
#

postosflu()
{
cd ../POSTOSFLU
./geraflu_TODOS.sh $1

}




#
# chama novo script do DAVID
#

novoscript()
{
#  DAVID VOCE ESTA EM XONS/SCRIPTS/MODULOS/SMAP_ONS/ 



return 
}


#-------------------------------------------------------------------------------------
# LAMMOC/UFF  - LABORATÓRIO DE MONITORAMENTO E MODELAGEM DO SISTEMA CLIM
# AMBMET      - CONSULTORIA EM METEOROLOGIA , OCEANOGRAFIA E HIDROLOGIA 
# WXWEATHER   - CONSULTORIA EM METEOROLOGIA E COMPUTAÇÃO DE ALTO DESEMPENHO  
#     
#-------------------------------------------------------------------------------------
#
#  
#  
#
#
# -----------------------------------------------------------------------------------  
# 
#  Desenvolvido por: Reginaldo Ventura de Sa (regis@lamma.ufrj.br) 
#  Release 1 em :  27/03/2018     
#
#-----------------------------------------------------------------------------------
# Sintaxe:
#
#-----------------------------------------------------------------------------------------------
# CHANGE LOG
# 
#-----------------------------------------------------------------------------------------------
#
# INICIO


#
# verifica qual diretorio foi ultimo diretorio que foi rodado 
# e qual a que tem a ultima rodada do SMAPONS
#

verificadir2 $1 $2 $3 
###copiatemplateons $1 

##criaarqinic $1 
levazoes  $1 #### parei aqui .a cho que funcionou . ver ITAIPU_INC no vobs 


cd ../POSTOSFLU
./geraflu_TODOS.sh $1
cd ../INCREMENTAL
./incremental_TODOS.sh $1 

echo "Pode rodar  o deck batsmap" 
# novoscript   ### 


