#!/bin/sh 


contachar()
{

# letter-count2.sh: Counting letter occurrences in a text file.
#
# Script by nyal [nyal@voila.fr].
# Used in ABS Guide with permission.
# Recommented and reformatted by ABS Guide author.
# Version 1.1: Modified to work with gawk 3.1.3.
#              (Will still work with earlier versions.)


INIT_TAB_AWK=""
# Parameter to initialize awk script.
count_case=0
FILE_PARSE=$1

E_PARAMERR=85

usage()
{
    echo "Usage: letter-count.sh file letters" 2>&1
    # For example:   ./letter-count2.sh filename.txt a b c
    exit $E_PARAMERR  # Too few arguments passed to script.
}

if [ ! -f "$1" ] ; then
    echo "$1: No such file." 2>&1
    usage                 # Print usage message and exit.
fi 

if [ -z "$2" ] ; then
    echo "$2: No letters specified." 2>&1
    usage
fi 

shift                      # Letters specified.
for letter in `echo $@`    # For each one . . .
  do
  INIT_TAB_AWK="$INIT_TAB_AWK tab_search[${count_case}] = \
  \"$letter\"; final_tab[${count_case}] = 0; " 
  # Pass as parameter to awk script below.
  count_case=`expr $count_case + 1`
done

# DEBUG:
# echo $INIT_TAB_AWK;

cat $FILE_PARSE |
# Pipe the target file to the following awk script.

# ---------------------------------------------------------------------
# Earlier version of script:
# awk -v tab_search=0 -v final_tab=0 -v tab=0 -v \
# nb_letter=0 -v chara=0 -v chara2=0 \

awk \
"BEGIN { $INIT_TAB_AWK } \
{ split(\$0, tab, \"\"); \
for (chara in tab) \
{ for (chara2 in tab_search) \
{ if (tab_search[chara2] == tab[chara]) { final_tab[chara2]++ } } } } \
END { for (chara in final_tab) \
{ print  final_tab[chara] } }"
# ---------------------------------------------------------------------
#  Nothing all that complicated, just . . .
#+ for-loops, if-tests, and a couple of specialized functions.

return 

# Compare this script to letter-count.sh.

}






LOCALQOBS="../../../OBS/QOBS/"


qobsfile=$LOCALQOBS"vobs_ale.csv"

contapv=`contachar  $qobsfile ";"`
contav=`contachar  $qobsfile ","`

echo $contapv
echo $contav

if test $contapv -gt 0 
then 
delim=";"
fi 
if test $contav -gt 0 
then 
delim=","
fi 


while IFS= read -r var 
do
  #echo $var 
  token=`echo $var | cut -d$delim -f1`
 
   if [ "$token" ==  "Posto" ]
  then 
      echo "Pegando linha com datas"  
      DATAS=$var 
	  echo $DATAS > .temps
      numreg=`contachar  .temps $delim`
	  let lastreg=(numreg-51) 	   
	  
	   
      t=0
	  for n in `seq $lastreg $numreg`
	  do
	       
	       #data[$n]=`cat .temps | cut -d";" -f$n`
		   #echo `cat .temps | cut -d";" -f$n`
		   datas[$t]=`cat .temps | cut -d$delim -f$n`
		   let t=(t+1) 
      done
		   
  else
       
      # echo $token
      # echo $numreg 
	  # echo ${datas[10]} 
	  case $token in
	              18) 
				  echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              avermelha[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61998080|COO|DI|VNM|"${datas[$t]}" 00:00:00|"${avermelha[$t]} >>AVERMELHA.txt 
		  
					  let t=(t+1) 
					  
			  
                   done 			
                   ;;
				   17)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              marimbondo[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61941080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${marimbondo[$t]} >>MARIMBONDO.txt 
					  let t=(t+1) 
                   done 			
                   ;;				   
				   1)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              camargos[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61065080|COO|DI|VIA|"${datas[$t]}" 00:00:00|"${camargos[$t]} >>CAMARGOS.txt 
					  let t=(t+1) 
                   done 			
                   ;;				  
				   15)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              edacunha[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61818080|COO|DI|VIA|"${datas[$t]}" 00:00:00|"${edacunha[$t]} >>EDACUNHA.txt 
					  let t=(t+1) 
                   done 			
                   ;;		
				   211)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              funil[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61146080|COO|DI|VNM|"${datas[$t]}" 00:00:00|"${funil[$t]} >>FUNIL_MG.txt 
					  let t=(t+1) 
                   done 			
                   ;;						   
				   6)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              furnas[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61661000|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${furnas[$t]} >>FURNAS.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   #-----------------------------------------------------------------------------------------------
				   266)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              itaipu[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${itaipu[$t]} >>ITAIPU.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   209)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              corumbai[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${corumbai[$t]} >>CORUMBA1.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   205)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              corumba4[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${corumba4[$t]} >>CORUMBAIV.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   24)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	
				   12)
				   echo $var > .temps
				   t=0
                   for n in `seq $lastreg $numreg`
	               do
		              pcolombia[$t]=`cat .temps | cut -d$delim -f$n`
					  echo "61796080|COO|DI|VNS|"${datas[$t]}" 00:00:00|"${pcolombia[$t]} >>PCOLOMBIA.txt 
					  let t=(t+1) 
                   done 			
                   ;;	




				   esac 			   
fi 
done < $qobsfile 

echo "==================>" ${camargos[10]}

