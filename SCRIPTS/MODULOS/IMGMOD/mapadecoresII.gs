

*
* magenta 
* violet 
* orchid
* mediumorchid
* purple
* darkorchid
'set rgb 100 255 000 255 050' 
'set rgb 101 238 130 238 050'
'set rgb 102 218 112 214 050'
'set rgb 103 186 085 211 050'
'set rgb 104 139 000 139 050'
'set rgb 105 153 050 204 050'
'set rgb 106 147 112 219 050'
'set rgb 107 123 104 238 050' 
* 
* darkviolet
* blueviolet 
*darkstateblue
* mediumblue
* meublue1
* meublue2
* blue
*
*'set rgb 110 148 000 211 050'
*'set rgb 111 138 043 226 050'
*'set rgb 112 072 061 139 050'
*'set rgb 113 000 000 150 050'
*'set rgb 114 000 000 100 050'
*'set rgb 115 000 000 205 050'
*'set rgb 116 000 000 255 050'
* darkgreen 
* springgreen
* limegreen
* yellowgreen
* olivegrab 
*'set rgb 120 000 100 000 050'
*'set rgb 121 000 250 154 050'
*'set rgb 122 050 205 050 050'
*'set rgb 123 107 142 035 050'
*'set rgb 124 173 255 047 050' 
*
* escala de azul 
*
'set rgb 110      0      0      100 50'
'set rgb 111      0      0      150 50'
'set rgb 112      0      0      200 50'
'set rgb 113      0      0      255 50'
'set rgb 114      0      100      255 50'
'set rgb 115      0      150      255 50'
'set rgb 116      0      200      255 50'
'set rgb 117      0      255      255 50'
'set rgb 118      150      255      255 50'

* escala de verde 

'set rgb 120      0      50      0     12'
'set rgb 121      0      100      0    12'
'set rgb 122      0      150      0    12'
'set rgb 123      0      200      0    12'
'set rgb 124      0      255      0    12'
'set rgb 125      0      255      100  12'





*
* darkred
* firebrick
* red
* orangered
* tomato
* coral
* salmon 
* darkorange
* orange
* yellow
'set rgb 130 128 000 000 050'
'set rgb 131 178 034 034 050'
'set rgb 132 255 000 000 050'
'set rgb 133 255 069 000 050'
'set rgb 134 255 099 071 050'
'set rgb 135 255 127 080 050'
'set rgb 136 250 128 114 050'
'set rgb 137 255 140 000 050'
'set rgb 138 255 165 000 050'
'set rgb 140 255 215 000 050' 
*
*  aqua/cyan
*  lightseagreen
*  turquesa
* aquamarine --hidro 
'set rgb 150 000 255 255 050'
'set rgb 151 032 178 170 050'
'set rgb 152 064 224 208 050'
'set rgb 153 127 255 212 050'

*
* escala de cinzas
*  mais claro para o negro 
*
'set rgb 200 220 220 220 050'
'set rgb 201 211 211 211 050'
'set rgb 202 192 192 192 050'
'set rgb 203 169 169 169 050'
'set rgb 204 128 128 128 050'
'set rgb 205 105 105 105 050'
'set rgb 206 075 075 075 050'
'set rgb 207 050 050 050 050'
'set rgb 208 025 025 025 050'
'set rgb 209 012 012 012 050'
'set rgb 210 000 000 000 050'




