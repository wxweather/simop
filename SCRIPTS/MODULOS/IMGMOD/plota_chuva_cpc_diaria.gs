
'open chuvacpc.ctl'



t=1
d=1
while(t<=90) 
*
* vou calcular ti e tf
*
'set t 't
'q time'
var=subwrd(result,3)
hora=substr(var,1,2)
dia=substr(var,4,2)
mes=substr(var,6,3)
ano=substr(var,9,4)
say ano' 'mes' 'dia
*

***
id=0
close("CONTROLE.DAT")
n=1
while(n<=12)
id=read("CONTROLE.DAT")
status=sublin(id,1)   
 if (status>0) 
say 'deu ruim ---'status
say id'--->'status'--->'codigo
'quit'
endif
var=sublin(id,2)
ID=subwrd(var,1)
plota=subwrd(var,2)
formato=subwrd(var,3)
shape=subwrd(var,4)
shape1=subwrd(var,5)
hidro=subwrd(var,6)
lon1=subwrd(var,7)
lon2=subwrd(var,8)
lat1=subwrd(var,9)
lat2=subwrd(var,10)
codigo=subwrd(var,11)
titulo=subwrd(var,12)" "subwrd(var,13)" "subwrd(var,14)" "subwrd(var,15)" "subwrd(var,16)" "subwrd(var,17)" "subwrd(var,18)
****
levels="    001 003 005 007 009 010 015 020 025 030 035 040 045 050 060 070 080 090 100 125 150 175 200 225 250 275 300 350 400 500 600 700 1000" 
cores= "000 200 201 202 203 204 205 130 131 132 133 134 135 136 137 138 120 121 122 123 124 110 111 112 113 114 115 107 106 105 104 103 102 101 100"    
figura="CHUVA_CPC_DIARIA_"codigo"_"
titulox="CHUVA ACUMULADA DIARIA "titulo 

if ( ID = 1 )

if ( formato="L") 
pi2=plotagfsa("sum","rain/10",lon1,lon2,lat1,lat2,levels,cores,shape,hidro,0,t,t,figura,"data",t,"WXWEATHER",titulox,1.5,8.3) 
endif
if (formato ="P") 
pi2=plotagfsa("sum","rain/10",lon1,lon2,lat1,lat2,levels,cores,shape,hidro,0,t,t,figura,"data",t,"WXWEATHER",titulox,1.5,8.3) 
endif
endif 
 
n=n+1
endwhile
t=t+1
endwhile


*
*





*'quit'
return





*----------------------------------------------------------------------------------------------
* funcao
* variavel= variavel a ser plotada
* x1,x2,y1,y2  = longitudes e atitudes  
* levels,cores,   = escala de niveis e escala de cores
* shape,   = arquivo shape file
* tempo1,tempo2    = plota o tempo 
* figura,    =nome da figura
* titulo1,titulo2,posx)    = titulo linha 1 , titulo linha 2 e posicao em x 
*

function  plotagfsa(funcao,variavel,x1,x2,y1,y2,levels,cores,shape,hidro,mascara,tempo1,tempo2,figura,tipotempo,item_fig,titulo1,titulo2,posx,posy) 

'reset'
'c'
*'set parea 2.12 8.85 0.73 7.76 '
*
* define coordenadas
*
*'set mpdset hires'
'set lon 'x1' 'x2
'set lat 'y1' 'y2
'set gxout shaded'


*
* pega o tempo a ser 
* processado 
*  

'set gxout shade2'
'set t 'tempo1
'q time'
var=subwrd(result,3)
horax=substr(var,1,2)
hora=substr(var,1,2)':00'
dia=substr(var,4,2)
mes=substr(var,6,3)
ano=substr(var,9,4)
'set t 'tempo2
'q time'
*say var' 'tempo2
var2=subwrd(result,3)
indicador=subwrd(var2,6,1)
if (indicador=":") 
hora2=substr(var2,1,5)
hora2=substr(var2,1,2)':59'
dia2=substr(var2,7,2)
mes2=substr(var2,9,3)
ano2=substr(var2,12,4)
else
hora2=substr(var2,1,2)
dia2=substr(var2,4,2)
mes2=substr(var2,6,3)
ano2=substr(var2,9,4)
endif


*
* grava as datas em aarquivo tempos (append)
*
pi=write("tempos",hora'Z'dia'/'mes'/'ano,append) 
*
* chama escala definida 
*
'define_colors.gs'
'mapadecoresII.gs'

'set clevs 'levels''
'set ccols 'cores'' 
*
* pega infos
* 
'q dims'
var=sublin(result,6)
ens=subwrd(var,6) 

'set mpt * off'
'set mpt 0 1 1 6'
'set mpt 1 0 1 1'
*
* plota variavel
*
'd 'funcao'('variavel',t='tempo1',t='tempo2')'
*'set gxout contour'
*'set ccolor 15'
*'d 'funcao'('variavel',t='tempo1',t='tempo2')'
'set gxout shade2'


*
* plota titulo com data e hora e ensemble se for o caso
*
'draw string 'posx' 'posy' 'titulo1''
'draw string 'posx' 'posy-0.2' 'titulo2''
'draw string 'posx' 'posy-0.4' DATA    : 'dia'/'mes'/'ano' as 'hora'Z a 'dia2'/'mes2'/'ano2' as 'hora2'Z '
*
* escala de cores
*
'cbarn.gs'


if ( mascara = 0 ) 
'set rgb 50   255   255    255'
'basemap.gs O 50 0 M'
'set mpdset hires'
'set map 15 1 6'
'draw map'
'set map auto'
endif 

*
* shape file
*
'set line  1 1 6'
'draw shp 'shape
'set line 5 1 6'
'draw shp 'hidro

if (tipotempo = "hora" ) 
'printim 'figura'_'horax'.png white'
return
endif

say tipotempo
if (tipotempo = "dia" ) 
'printim 'figura'_'dia'.png white'
return
endif



if (tipotempo = "user" ) 
'printim 'figura'_'item_fig'.png white'
return
endif


if (tipotempo = "data" ) 
say  'printim 'figura'_'dia'_'mes'_'ano'_'horax'.png white'
'printim 'figura'_'dia'_'mes'_'ano'_'horax'.png white'
return
endif

if (tipotempo = "mes" ) 
'printim 'figura'_'mes'_'ano'_.png white'
return
endif
return 











function plotausinas(arquivo) 


id=0
while( 0=0) 
id=read(arquivo)
*
* se status=0 tudo ok. se n�o ...
*
status=sublin(id,1)   
if (status>0) 
say 'seu ruim'
close(arquivo)
return
endif
var=sublin(id,2)
lat=subwrd(var,1)
lon=subwrd(var,2)
marca=subwrd(var,3)
titulo=subwrd(var,4)''subwrd(var,5)''subwrd(var,6)''subwrd(var,7)''subwrd(var,6)

'set line 1 1 1'
'q w2xy 'lon' ' lat''
x=subwrd(result,3)
y=subwrd(result,6)
'draw mark 'marca' 'x' 'y' 0.3'
*'set strsiz 1'
'draw string 'x' 'y' 'titulo''
say titulo
endwhile
return





*------------------------------------------------------------------------------------------------
* variavel= variavel a ser plotada
* x1,x2,y1,y2  = longitudes e atitudes  
* levels,cores,   = escala de niveis e escala de cores
* shape,   = arquivo shape file
* vento,    se =0 plota stremlines e =1 vector (se nao 1 e 0 n�o plota vento)
* pressao,  se pressao=0 plota niveis de pressao
* tempo,    = plota o tempo 
* figura,    =nome da figura
* titulo1,titulo2,posx)    = titulo linha 1 , titulo linha 2 e posicao em x 
*

function  plotagfs(variavel,x1,x2,y1,y2,levels,cores,shape,vento,pressao,tempo,figura,titulo1,titulo2,posx) 

'reset'
*
* define coordenadas
*
*'set mpdset nmap'
'set lon 'x1' 'x2
'set lat 'y1' 'y2
'set gxout shaded'
*
* define tempo da rodad (t=1)
*
'set t 1'
'q time'
rodada=subwrd(result,3)
*
* pega o tempo a ser 
* processado 
*  
'c'
'set gxout shade2'
'set t 'tempo
'q time'
var=subwrd(result,3)
hora=substr(var,1,2)
dia=substr(var,4,2)
mes=substr(var,6,3)
ano=substr(var,9,4)
*
* grava as datas em aarquivo tempos (append)
*
pi=write("tempos",hora'Z'dia'/'mes'/'ano,append) 
*
* chama escala definida 
*
'define_colors.gs'
'set rgb 99 251 94 107'
'set clevs 'levels''
'set ccols 'cores'' 
*
* pega infos
* 
'q dims'
var=sublin(result,6)
ens=subwrd(var,6) 
*
* plota variavel
*
'd 'variavel''
*
* plota titulo com data e hora e ensemble se for o caso
*
'draw string 'posx' 8.3 'titulo1''
'draw string 'posx' 8.1 'titulo2''
'draw string 'posx' 7.9 RODADA : 'rodada
'draw string 'posx' 7.7 DATA    : 'dia'/'mes'/'ano' as 'hora'Z - ENS='ens
*
* escala de cores
*
'cbarn.gs'
*
* shape file
*
'draw shp 'shape
*'set line 6 1 1'
*'draw shp 'hidro

*
* se vento-0 plota stremlines
*
if (vento = 0) 
'set gxout stream'
'set clevs 2 4 6 8 10 12 15 20 25' 
'd ugrdprs;vgrdprs;mag(ugrdprs,vgrdprs)'
'cbarc.gs'
endif 
*
* se vento=1 plota vetor do vento e magnitude
*
if (vento = 1) 
'set gxout vector'
'set clevs 2 4 6 8 10 12 15 20 25' 
'd ugrdprs;vgrdprs;mag(ugrdprs,vgrdprs)'
'cbarc.gs'
endif 
*
* plota pressao se pressao=0 
*
if (pressao = 0) 
'set gxout contour'
'set clevs 1000 1002 1005 1007 1010 1011 1012 1013 1014 1015 1017 1020 1025'
'd prmslmsl/100'
endif 
'printim 'figura'_'tempo'.png white'
return 



