*'sdfopen http://nomads.ncep.noaa.gov:9090/dods/gfs_0p25_1hr/gfs20170215/gfs_0p25_1hr_00z'
'sdfopen http://nomads.ncep.noaa.gov:80/dods/gfs_0p50/gfs20170324/gfs_0p50_00z'


'q files'
var=sublin(result,1)
modelo=subwrd(var,5)
versao=subwrd(var,7)

if (modelo ="1.0") ; modnumber=0; endif
if (modelo ="0.5") ; modnumber=1; endif
if (modelo ="0.25"); modnumber=2; endif
if (modelo ="0.25" & versao="hourly") ; modnumber=3; endif
 





'q gxinfo'
var=sublin(result,2)
valor=subwrd(var,4) 

if(valor = "11")  
_modo="L"
say "FORMATO PAISAGEM"
else
SAY "FORMATO RETRATO"
_modo="P"
endif

n=1
while ( n<=12)
id=read("CONTROLE.DAT")
status=sublin(id,1)   
 if (status>0) 
say 'deu ruim ---'status
say id
'quit'
endif
var=sublin(id,2)
ID=subwrd(var,1)
plota=subwrd(var,2)
formato=subwrd(var,3)
shape=subwrd(var,4)
shape1=subwrd(var,5)
hidro=subwrd(var,6)
lon1=subwrd(var,7)
lon2=subwrd(var,8)
lat1=subwrd(var,9)
lat2=subwrd(var,10)
codigo=subwrd(var,11)
titulo=subwrd(var,12)" "subwrd(var,13)" "subwrd(var,14)" "subwrd(var,15)" "subwrd(var,16)" "subwrd(var,17)" "subwrd(var,18)

say TITULO
NUMTEMPOS=15

if (_modo = formato) 
variavel="TEMPERATURA DO AR (C)"
empresa="SANTO ANTONIO ENERGIA"
levels='   000 005 010 015 020 022 024 026 028 030 032 034 036 040'
cores='200 201 202 203 255 254 253 252 251 106 105 104 103 102 101 100'
pi=gerafigura("tmp2m-273.15",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_temp2m",ID, modnumber)    

variavel="UMIDADE RELATIVA DO AR (%)"
empresa="SANTO ANTONIO ENERGIA"
levels='   020 030 040 050 060 070 080 090 100' 
cores='100 101 102 250 251 252 204 203 202 201 200     '
pi=gerafigura("rh2m",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_urel",ID, modnumber)    


variavel="UMIDADE RELATIVA DO AR (%) - 850 hPa"
empresa="SANTO ANTONIO ENERGIA"
levels='   020 030 040 050 060 070 080 090 100' 
cores='100 101 102 250 251 252 204 203 202 201 200     '
pi=gerafigura("rhprs(lev=850)",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_urel850",ID, modnumber)    

variavel="UMIDADE RELATIVA DO AR (%) - 500 hPa"
empresa="SANTO ANTONIO ENERGIA"
levels='   020 030 040 050 060 070 080 090 100' 
cores='100 101 102 250 251 252 204 203 202 201 200     '
pi=gerafigura("rhprs(lev=500)",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_urel500",ID, modnumber)    

variavel="UMIDADE RELATIVA DO AR (%) - 200 hPa"
empresa="SANTO ANTONIO ENERGIA"
levels='   020 030 040 050 060 070 080 090 100' 
cores='100 101 102 250 251 252 204 203 202 201 200     '
pi=gerafigura("rhprs(lev=500)",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_urel200",ID, modnumber)    


variavel="AGUA PRECIPITAVEL (mm)"
empresa="SANTO ANTONIO ENERGIA"
levels="   002 010 015 020 030 040 050 060 070 080 090 100 150 175 200 300 "
cores="000 108 107 106 105 255 254 253 252 251 205 204 203 202 201 103 102 101"
pi=gerafigura("pwatclm",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_pwat",ID, modnumber)    


variavel="COBERTURA DE NUVEM (10%-100%) / CÉU CLARO (0%)"
empresa="SANTO ANTONIO ENERGIA"
levels="   000 010 020 030 040 050 060 070 080 090 100 "
cores=" 00 000 116 116 116 115 115 114 113 112 111 110 110 "  
pi=gerafigura("tcdcclm",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_nuvem",ID, modnumber)    


variavel="ALTURA GEOPOTENCIAL EM 500 hPa ( dm )"
empresa="SANTO ANTONIO ENERGIA"
levels=""
cores=""  
pi=gerafigura("hgtprs(lev=500)/10",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_hgt500",ID, modnumber)    

variavel="INDICE CAPE"
empresa="SANTO ANTONIO ENERGIA"
levels="    1000  1500  2000    2500   3000   3500  4000 "
cores="01   21    22      23      24     25     26    27   29" 
pi=gerafigura("capesfc",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_cape",ID, modnumber)    

  
variavel="INDICE LEVANTAMENTO"
empresa="SANTO ANTONIO ENERGIA"
levels="    -10  -9 -8 -7 -6 -5 -4 -3 -2 -1  0  1  2  3   4 "
cores="59  29  29 29 29 29 29 27 27 25 23 23 23 22 22  22  00" 
pi=gerafigura("no4lftxsfc",NUMTEMPOS,2,lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,titulo,codigo,"sae_diaria_lifted",ID, modnumber)    




 




endif 
n=n+1
endwhile


rhprs

'quit'


function gerafigura( varx, num_tempos, incr, lon1,lon2,lat1,lat2,levels,cores,shape,shape2,hidro,empresa,variavel,area,codarea,titfig,ID,modnumber) 

say ID'---------------------'
t=1
d=1
while (t <=num_tempos)
'reset'
'set grads off'
'set gxout shade2'
'set lon 'lon1' 'lon2
'set lat 'lat1' 'lat2
'c'
'set t 't
'q time'

var=subwrd(result,3)
hora=substr(var,1,2)
dia=substr(var,4,2)
mes=substr(var,6,3)
ano=substr(var,9,4)


if (_modo="L") 
'set string 4 bl 12 0'
'draw string 0.5 8.3 'empresa
'draw string 0.5 8.1 'variavel' - 'area
'draw string 0.5 7.9 DATA: 'dia'/'mes'/'ano
endif 

if (_modo="P")
'set string 4 bl 12 0'
'draw string 0.5 10.7 'empresa
'draw string 0.5 10.5 'variavel' - 'area
'draw string 0.5 10.3 DATA: 'dia'/'mes'/'ano 
endif 

'define_colors.gs'
'mapadecores.gs'
'set clevs 'levels
'set ccols 'cores



'd ave('varx',t='t',t='t+1')'
'cbarn.gs'


'set line 1 1 12'
'draw shp 'shape
'set line 15 1 1'
'draw shp 'shape2
'set line 207 1 1'
'draw shp 'hidro



if (_modo="P")
*'printim 'titfig'_'codarea'_'dia'_'mes'_'ano'.png white x1236 y1600'
'printim 'titfig'_'ID'_'d'_'modnumber'.png white'

else
*'printim 'titfig'_'codarea'_'dia'_'mes'_'ano'.png white x1600 y1236'
'printim 'titfig'_'ID'_'d'_'modnumber'.png white'

endif 



t=t+incr
d=d+1
endwhile 

return





