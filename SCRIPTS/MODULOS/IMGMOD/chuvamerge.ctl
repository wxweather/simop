DSET ^../../../../DADOS/MERGE/prec_%y4%m2%d2.bin
options  little_endian template
title global daily analysis 
undef -999.0 
xdef 245 linear    -82.8000 0.2000
ydef 313 linear -50.2000  0.2000 
zdef 1    linear 1 1 
tdef 90 linear 00Z01JAN2017 1dy 
vars 2
chuva     1  00 chuva acumulada 24 horas
gnum     1  00 the number of stn
ENDVARS
