'sdfopen http://nomads.ncep.noaa.gov:9090/dods/gfs_0p25_1hr/gfs20170324/gfs_0p25_1hr_00z'


t=1
d=1
while(t<=49) 
'define_colors.gs'
*
*  SINOTICA
*
empresa="WXWEATHER - CONSULTORIA EM METEOROLOGIA"
variavel="PRECIPITAÇÃO ACUMULADA EM 24 HORAS (mm/dia)" 
shape="../../SHAPES/americadosulebrasil.shp"
hidro="../../SHAPES/rios_niveis_1_e_2_brasil.shp"
variavel="PRECIPITACAO ACUMULADA EM 24 HORAS (mm/dia) (TX)" 
levels="    2  05 10 15 20 25 30 40 50 60 70 80 90 100 150 200"
cores="00  22  23 34 35 36 37 38 39 43 44 45 46 47  48  49   "
mapopt=0
vento=0
skip=5
pressao=0
contur=0
cmin=2.0
*plotagfs("pratesfc*3600",contur,cmin,-150,0,-75,20,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_GFS_CHUVA_O1_",empresa,variavel,0.5,8.3) 
*plotagfs("cwatclm",contur,cmin,-150,0,-75,20,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_GFS_PWAT_O1_",empresa,variavel,0.5,8.3) 
mapopt=0
vento=1
skip=5
pressao=0
contur=0
cmin=2.0
*plotagfs("pratesfc*3600",contur,cmin,-150,0,-75,20,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_GFS_CHUVA_O2_",empresa,variavel,0.5,8.3) 
*plotagfs("cwatclm",contur,cmin,-150,0,-75,20,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_GFS_PWAT_O2_",empresa,variavel,0.5,8.3) 
mapopt=0
vento=-1
skip=5
pressao=0
contur=0
cmin=2.0
*plotagfs("pratesfc*3600",contur,cmin,-150,0,-75,20,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_GFS_CHUVA_O3_",empresa,variavel,0.5,8.3) 
*plotagfs("cwatclm",contur,cmin,-150,0,-75,20,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_GFS_PWAT_O3_",empresa,variavel,0.5,8.3) 
mapopt=0
vento=-1
skip=5
pressao=-1
contur=0
cmin=2.0
*plotagfs("pratesfc*3600",contur,cmin,-150,0,-75,20,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_GFS_CHUVA_O4_",empresa,variavel,0.5,8.3) 
*plotagfs("cwatclm",contur,cmin,-150,0,-75,20,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_GFS_PWAT_O4_",empresa,variavel,0.5,8.3) 
t=t+6
endwhile


t=1
d=1
while(t<=49) 
*
* RIO DE JANEIRO
*
empresa="WXWEATHER - CONSULTORIA EM METEOROLOGIA"
variavel="PRECIPITAÇÃO ACUMULADA EM 24 HORAS (mm/dia)" 
shape="../SHAPES/rio_de_janeiro.shp"
hidro="../SHAPES/rios_niveis_1_e_2_brasil.shp"
variavel="PRECIPITACAO ACUMULADA EM 24 HORAS (mm/dia) (TX)" 
levels="    2  05 10 15 20 25 30 40 50 60 70 80 90 100 150 200"
cores="00  22  23 34 35 36 37 38 39 43 44 45 46 47  48  49   "
*
* configuracao mapa shape sem mapa grads sem pressao sem vento  sem contour 
*
mapopt=1
vento=1
skip=5
pressao=0
contur=0
cmin=1.0
*plotagfs("pratesfc*3600",contur,cmin,-45,-40.7,-23.5,-20.75,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_RIO_GFS_CHUVA_O2_",empresa,variavel,0.5,8.3) 
*plotagfs("cwatclm",contur,cmin,-45,-40.7,-23.5,-20.75,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_RIO_GFS_PWAT_O1_",empresa,variavel,0.5,8.3) 

mapopt=1
vento=2
skip=1
pressao=1
contur=1
cmin=1.0
plotagfs("pratesfc*3600",contur,cmin,-45,-40.7,-23.5,-20.75,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_RIO_GFS_CHUVA_O3_",empresa,variavel,0.5,8.3) 
*plotagfs("cwatclm",contur,cmin,-45,-40.7,-23.5,-20.75,levels,cores,shape,hidro,mapopt,vento,skip,pressao,t,"WXWEATHER_RIO_GFS_PWAT_O1_",empresa,variavel,0.5,8.3) 


t=t+1
endwhile
return
'quit'




*------------------------------------------------------------------------------------------------
* variavel= variavel a ser plotada
* x1,x2,y1,y2  = longitudes e atitudes  
* levels,cores,   = escala de niveis e escala de cores
* shape,   = arquivo shape file
* vento,    se =0 plota stremlines e =1 vector (se nao 1 e 0 não plota vento)
* skip =   funcao skip do grads para vento 1 mostra tudo
* pressao,  se pressao=0 plota niveis de pressao
* tempo,    = plota o tempo 
* figura,    =nome da figura
* titulo1,titulo2,posx)    = titulo linha 1 , titulo linha 2 e posicao em x 
*

function  plotagfs(variavel,contur,cmin,x1,x2,y1,y2,levels,cores,shape,hidro,mapopt,vento,skp,pressao,tempo,figura,titulo1,titulo2,posx,posy) 

'reset'
*
* define coordenadas
*
if (mapopt = 1) 
'set mpdset hires'
endif 
'set lon 'x1' 'x2
'set lat 'y1' 'y2
'set gxout shaded'
*
* define tempo da rodad (t=1)
*
'set t 1'
'q time'

var=subwrd(result,3)
horar=substr(var,1,2)
diar=substr(var,4,2)
mesr=substr(var,6,3)
anor=substr(var,9,4)

*
* pega o tempo a ser 
* processado 
*  
'c'
'set gxout shade2'
'set t 'tempo
'q time'
var=subwrd(result,3)
hora=substr(var,1,2)
dia=substr(var,4,2)
mes=substr(var,6,3)
ano=substr(var,9,4)
*
* grava as datas em aarquivo tempos (append)
*
pi=write("tempos",hora'Z'dia'/'mes'/'ano,append) 
*
* chama escala definida 
*
'define_colors.gs'
'set rgb 99 251 94 107'
'set clevs 'levels''
'set ccols 'cores'' 
*
* pega infos
* 
'q dims'
var=sublin(result,6)
ens=subwrd(var,6) 
*
* plota variavel
*
'd 'variavel''
*
* 
*
if (contur = 1)
'set gxout contour'
'set cmin 'cmin
'd 'variavel''
endif 


*
* plota titulo com data e hora e ensemble se for o caso
*
*
'draw string 'posx' 'posy' 'titulo1''
'draw string 'posx' 'posy-0.2' 'titulo2''
'draw string 'posx' 'posy-0.4' DATA DA RODADA:'diar'/'mesr'/'anor' as 'horar'Z  DATA PREVISAO:'dia'/'mes'/'ano' as 'hora'Z' 
*
* escala de cores
*
'cbarn.gs'
*
* shape file
*
'draw shp 'shape
if (hidro="")
say
else
'set line 5 1 1'
'draw shp 'hidro
endif

*
*
*
* plota pressao se pressao=0 
*
*
if (pressao = 1) 
'set gxout contour'
'set clevs 1000 1002 1005 1007 1010 1011 1012 1013 1014 1015 1017 1020 1025'
'd prmslmsl/100'
endif 
*
* se vento-0 plota stremlines
*
if (vento = 1) 
'set gxout stream'
'set clevs 2 4 6 8 10 12 15 20 25' 
'd ugrdprs;vgrdprs;mag(ugrdprs,vgrdprs)'
'cbarc.gs'
endif 
*
* se vento=1 plota vetor do vento e magnitude
*
if (vento = 2) 
'set gxout vector'
'set clevs 2 4 6 8 10 12 15 20 25' 
'd skip(ugrdprs,'skp');skip(vgrdprs,'skp');mag(ugrdprs,vgrdprs)'
***'d ugrdprs;vgrdprs;'
'cbarc.gs'
endif 

*
* shape file
*
'set line 1 1 1'
'draw shp 'shape
if (hidro="")
say
else
'set line 5 1 1'
'draw shp 'hidro
endif

'printim 'figura'_'tempo'.png white'
return 



