
*'open eta.ctl'
*_arqbacia="bacias_eta40.dat"
*_prefixo="PRECMEDIA_ETA40.GEFS"
*_baciap="GRANDE"
*pi=prevconj("conjunto_ETA40_GRANDE.dat", "conjunto_GEFSRMV_GRANDE.dat", 10) 



'quit'







function prevconj( arq_prev_m1, arq_prev_m2, numtempos) 
say "INICIO"

status=0
while (status =0)
p1=read(arq_prev_m1) 
status=sublin(p1,1)
if (status = 2) 
    say "Arquivo:"arq_prev_m1" fim atingido"
    return
endif


if (status = 1) 
    say "Arquivo:"arq_prev_m1" nao  foi encontrado"
    return
endif
args1=sublin(p1,2) 

*
* processa arquivo II
*
status2=0
p2=close(arq_prev_m2)
while(status2 =0) 
p2=read(arq_prev_m2) 
status2=sublin(p2,1)
if (status2 = 1) 
    say "Arquivo:"arq_prev_m2" nao  foi encontrado"
    return
endif
args2=sublin(p2,2)
bacia1=subwrd(args1,1)
bacia2=subwrd(args2,1)
say "ETA:processando "args1
say "GEFS:processando "args2
*
*
* se encontra a bacia na busca 
* pega as informaçoes 
*
*
say "par'"
say "==========================="bacia1' 'bacia2

if (bacia1 = bacia2 )
say "achou a bacia "bacia1" com a bacia "bacia2
j=1
while(j<=numtempos) 
     chuva1.j=subwrd(args1,j+7)
     chuva2.j=subwrd(args2,j+7)
     say "LINHA062:"chuva1.j" "chuva2.j" j"j 
     _chuvat.j=math_format("%06.2f",chuva1.j+chuva2.j)  
	 say "LINHA062:"chuva1.j" "chuva2.j" "_chuvat.j" j="j 
     j=j+1
endwhile
*pi=write(".temp2.dat",bacia1' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvat.1' '_chuvat.2' '_chuvat.3' '_chuvat.4' '_chuvat.5' '_chuvat.6' '_chuvat.7' '_chuvat.8' '_chuvat.9' '_chuvat.10' '_chuvat.11' '_chuvat.12' '_chuvat.13' '_chuvat.14' '_chuvat.15' '_chuvat.16)
pi=parafmtons(bacia1,numtempos)
status2=-1
say "dddddddddddddddddddddddddddddddddd"
endif
endwhile  *** status2
endwhile   *** status1
return 




function parafmtons(bacia,numtempos) 

loop=0
say "bacia="bacia
while( loop=0  ) 
pi=getfile(0)
if (_args="</START/>" ) 
id=getfile(0)
endif 
*
if (_args ="</BACIA/>") 
*
id=getfile(1)
*say "oi"
*
* pega nome da bacia e quantidade de pontos (lat,lon)
*
bacia1=subwrd(_args,2)
numlin=subwrd(_args,4)
debug=subwrd(_args,6) 
*
*
*
*say "LINHA102:bacia1="bacia1" bacia="bacia
if (bacia1 = bacia) 
say "XXXX Processando:"bacia" com "numlin" coordenadas "debug
j=1
while(j<=numlin) 
id=getfile(0)
lonx=subwrd(_args,1)
latx=subwrd(_args,2)
*say latx' 'lonx
i=1
while(i<=numtempos)
pi=geranomeons(i,_prefixo) 

flon=math_format("%6.2f",lonx)
flat=math_format("%6.2f",latx)


pi=write(_baciap'_'_nomeons,flon' 'flat' '_chuvat.i) 
pi=geranomeons(i,"PRECZERADA_ETA40") 
pi=write(_baciap'_'_nomeons,flon' 'flat' '000.00) 
i=i+1
endwhile 
*pi=write(".temp2.dat",lonx' 'latx' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvat.1' '_chuvat.2' '_chuvat.3' '_chuvat.4' '_chuvat.5' '_chuvat.6' '_chuvat.7' '_chuvat.8' '_chuvat.9' '_chuvat.10' '_chuvat.11' '_chuvat.12' '_chuvat.13' '_chuvat.14' '_chuvat.15' '_chuvat.16)
j=j+1
endwhile
endif 
endif
if (_args ="</END/>") 
loop=1
return
endif
endwhile
return 



 
 
function getfile(cmd)
_id=read(_arqbacia)
_status=sublin(_id,1)   
_args=sublin(_id,2)
if (_status = 2 )
say "[ERRO 01] Fimdo arquivo "_arqbacia" atingido"
_id=close(_arqbacia) 
 return 
endif 

return 





function geranomeons(tempo,prefixo)

pi=pegadata(0)
*say _ano' '_mes' '_dia
ano=math_format("%02g",_ano-2000) 
mes=math_format("%02g",_mesx)
dia=math_format("%02g",_dia) 

t=tempo 
pi=pegadata(t)
ano2=math_format("%02g",_ano-2000) 
mes2=math_format("%02g",_mesx)
dia2=math_format("%02g",_dia) 

_nomeons=prefixo'_p'dia''mes''ano'a'dia2''mes2''ano2'.dat' 

return 


function pegadata(tempo)

if (tempo = -1) 
'set t last'
else
'set t 'tempo
endif 

'q time'
datap=subwrd(result,3)
indica=substr(datap,3,1)
if (indica =":" ) 
_ano=substr(datap,12,4)
_mes=substr(datap,9,3) 
_dia=substr(datap,7,2)
_hora=substr(datap,1,2)
_min=substr(datap,4,2)
else
_ano=substr(datap,9,4)
_mes=substr(datap,6,3) 
_dia=substr(datap,4,2)
_hora=substr(datap,1,2)
_min=00
endif


if (_mes = "JAN" ) ; _mesx=1; endif 
if (_mes = "FEB" ) ; _mesx=2; endif 
if (_mes = "MAR" ) ; _mesx=3; endif 
if (_mes = "APR" ) ; _mesx=4; endif 
if (_mes = "MAY" ) ; _mesx=5; endif 
if (_mes = "JUN" ) ; _mesx=6; endif 
if (_mes = "JUL" ) ; _mesx=7; endif 
if (_mes = "AUG" ) ; _mesx=8; endif 
if (_mes = "SEP" ) ; _mesx=9; endif 
if (_mes = "OCT" ) ; _mesx=10; endif 
if (_mes = "NOV" ) ; _mesx=11; endif 
if (_mes = "DEC" ) ; _mesx=12; endif 

return 


