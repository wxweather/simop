*-------------------------------------------------------------------------------------
* LAMMOC/UFF  - LABORATÓRIO DE MONITORAMENTO E MODELAGEM DO SISTEMA CLIMÁTICO 
* AMBMET      - CONSULTORIA EM METEOROLOGIA , OCEANOGRAFIA E HIDROLOGIA 
* WXWEATHER   - CONSULTORIA EM METEOROLOGIA E COMPUTAÇÃO DE ALTO DESEMPENHO  
*     
*-------------------------------------------------------------------------------------
* CALCBACIA VERSÃO 3  RELEASE 3
* 
* Calcula a média de precipitação em um contorno de bacia hidrografica. a Chuva pode 
* ser prevista (ETA40, GFS, GEFS , CFS, CAM 3.1 ou de chuva observada do tipo grade 
* CPC, MERGE ,CMORPH etc...
* 
* Outros tipos de dados em grade podem ser adaptados.   
*------------------------------------------------------------------------------------  
* 
*  Desenvolvido por: Reginaldo Ventura de Sa (regis@lamma.ufrj.br) 
*  Release 1 em :  21/11/2017     
*
*-----------------------------------------------------------------------------------
*  CHANGE LOG: 
*
* 
*------------------------------------------------------------------------------------

say "fim calcbacia V3.2"
'quit'



*-----------------------------------------------------------------------------
* CALCBACIA VERSAO 3.0   ->   VEIO DO SCRIPT SIMOP ATÉ 2.0  
* 
* Calcula a soma e média de uma quantidade dentro de um contorno de uma bacia 
* ou região qualquer. 
*
* gera chuva em ponto de grade ou media da bacia.
*
* INPUT:  arquivos criado pelo processo ADDBACIA e um arquivo de dados de modelos e chuva de grade 
* 
*--------------------------------------------------------------
*  calcbacia() ---> seleciona bacia do arquivo bacia.dat e chama função pegapontos().
*  pegapontos() --> calcula o valor da variavel para os pontos par de coordenadas. cada ponto é processado em  pegavalor6h()
* pegavalor6h()  --> /





function calcbacia( vargrads, t0,  numtempos , modolon,  rmv , freq , modocalc )  
*-----------------------------------
*
* calcbacia ( variavel a ser calculada, tempo inicial , numero de tempos a processar , prefixo do arquivo de saida) 
*
*-------------------------------------------------------------

if (_modelo="")
say "MODELO NÃO DEFINIDO. NÃO SE PODE FAZER NADA"
return
endif 

*---------------------------------------------------------------------------------------
*
* INICIALIZAÇÃO DAS VARIAVEIS 
* variaveis globais de controle de fluxo
*
_id=0
_status=0
_args=""
_ano=2018
_mes=1
_dia=1
_hora=0
_min=0 
*
* inicialização dos vetores
*  
* parte 1 - alocando memoria
*           usando vetor nullo  
* 
j=1
while(j<=16)
_valor.j=""
_chuva.j=""
_chuvamed.j="" 
_chuvaf.j=""
_chuvamedf.j=""
_chuvaconj.j=""
_beta.j=""
_chuvapg.j=""
_ppr10dias.j=""
_ppr1dia.j=""
_pprlim10dias=""
_pprlim1dia=""
_chuvaacu.j=""
_chuvamedf.j="" 
j=j+1
endwhile 
*
*
* 
*
while ( 0=0 )      
*
* abre o arquivo bacias e pega um registro
*
id=getfile(0)
*
*
* se tiver </START/> inicia o processo 
*
if (_args="</START/>" ) 
id=getfile(0)
endif 


*
if (_args ="</BACIA/>") 
*
* pega novo registro 
*

id=getfile(1)

*
* pega nome da bacia e quantidade de pontos (lat,lon)
*
bacia=subwrd(_args,2)
numlin=subwrd(_args,4)
debug=subwrd(_args,6) 
*
*
*



say "Processando:"bacia" com "numlin" coordenadas "dbg 
*
* zerando os vetores PARA ORICESSAR UMA NOVA BACIA
* SOMENTE SAO INICIALIZADAS VETORES QUE SERÃO USADOS. DEMAIS FICAM NULOS
*

j=1
while(j<=numtempos)
_valor.j=0.0
_chuva.j=0.0
_chuvamed.j=0.0 
_chuvaf.j=0.0
_chuvamedf.j=0.0
_chuvapg.j=0.0
_ppr10dias.j=0.0
_ppr1dia.j=0.0
_pprlim10dias=0.0
_pprlim1dia=0.0 
_chuvaacu.j=0.0
_chuvamedf.j=0.0
j=j+1
endwhile 
*
* Processa os dados da bacia N 
*
* GRAVA REGISTRO TEMPORARIO PARA CALCULO RETROATIVO 
* 
pi=write(".temp.prn",numlin' 'bacia) 
pi=write(_arq_debug,numlin' 'bacia) 

*
*  CHAMA A FUNCAO QUE PROCESSA OS PONTOS DE GRADE DA BACIA
*  RETORNA NENHUMA INFORMAÇÃO. SO ENVIA. 


pi=labeldebug(" PONTOS DE  GRADE ORIGINAIS ",1) 

if ( modocalc = "PG" )
pi=pegapontos(bacia,numlin,vargrads,numtempos,modolon,rmv,freq)
endif

if ( modocalc = "CMB" )
pi=geramedia(bacia,numlin,vargrads,numtempos,modolon,freq)
endif

if ( modocalc = "CFSENS" )
pi=geraenscfs(bacia,numlin,vargrads,numtempos,modolon,freq)
endif



if (rmv = 1) 

pi=remocaovies(bacia,numlin,numtempos,modolon,freq) 

endif 



*
* SE ACHA O TOKEN BACIA EH POR QUE O PROCESSAMENTO NA BACIA ATUAL
* TERMINOU. ENTÃO PORCESSO BUSCA NOVA BACIA. 
*

endif   ******    if (_args ="</BACIA/>")    
*
*  SE ACHA O TOKEN END É POR QUE CHEGOU AO FINAL DA OPERAÇÃO 
*

if (_args ="</END/>")      
say "Fim da operacao"
close (_arqbacia)
return
endif

*
*  "FIM" DO LOOP INFINITO  
*
endwhile  ****** while ( 0=0 )       
close (_arq_bacia)
return 


function  geraenscfs(bacia,nunlin,vargrads,numtempos,modolon,freq) 
*=====================================================================================================
*  
*  GERAMEDIA
*
*=====================================================================================================
*
* processa cada coordenada do arquivo de bacias
*
*


k=1
while(k<=_numens_cfs) 
j=1
while(j<=16)
_chuvaacu.j.k=""
_chuvamed.j.k="" 
j=j+1
endwhile
k=k+1
endwhile 



k=1
while(k<=_numens_cfs) 
j=1
while(j<=numtempos)
_chuvaacu.j.k=0.0
_chuvamed.j.k=0.0 
j=j+1
endwhile
k=k+1
endwhile 




say vargrads' '_numens_cfs
n=1
while ( n <= nunlin)
*
* pega novo registro 
*
id=getfile(2)
*
* trata lat e lon 
* verifica se usa lon sempre positivo ou  não (opcao:modolon) 
*
if (modolon = 1 )
lonxx=subwrd(_args,1)
say lonxx
lonx=lonxx+360
latx=subwrd(_args,2)
else
lonxx=subwrd(_args,1)
lonx=lonxx
latx=subwrd(_args,2)
endif 
*
*
*  
say "debugao [1] "lonx ' 'latx' 'n' 'modolon 
*
*  calcula chuva  para o ponto de grade no decorrer do tempo
*  valores são gravados em _valor.1, _valor.2 ...._valor.n 
*  
* 


k=1
while(k<=_numens_cfs) 
var=subwrd(vargrads,k) 


if (freq=6)
pi=pegavalor6h(lonx,latx,var,numtempos)
endif 
if (freq=24)
pi=pegavalor24h(lonx,latx,var,numtempos)
endif 

*
* 

* CHUVA ACUMULADA DIARIA (_CHUVAACU.J) 
* acumulo em _chuvaacu o valor da chuva para cada tempo somando os valores de 
* cada ponto de grade 


 


* 
j=1
while (j<=numtempos)
*say _valor.j' '_chuvaacu.j.k' j='j' k= 'k
_chuvaacu.j.k=_chuvaacu.j.k+_valor.j
j=j+1
endwhile     
k=k+1
endwhile 




n=n+1
endwhile  ******while ( n < nunlin)


*
*
* calcula media de cada dia no tempo
*
*"%06.2f"

k=1
while(k<=_numens_cfs) 
j=1
while (j<=numtempos)
_chuvamed.j.k=math_format("%06.2f",(_chuvaacu.j.k/nunlin))  
say _chuvaacu.j' '_chuvamed.j
j=j+1
endwhile     
k=k+1
endwhile 


k=1
while(k<=_numens_cfs) 


cuia=substr(vargrads,1,6)  

say cuia "    ############################  "
pi=pegadata(k)
ano1=_ano
mes1=_mesx
dia1=_dia

***say k" "mes1" "dia1 


pi=write(_prefixo"_"bacia".dat",ano1' 'mes1' 'dia1' '_chuvamed.1.k' '_chuvamed.2.k' '_chuvamed.3.k' '_chuvamed.4.k' '_chuvamed.5.k' '_chuvamed.6.k)
*pi=write(_prefixo"_"bacia".dat",ano1' 'mes1' 'dia1' '_chuvamed.1.k' '_chuvamed.2.k' '_chuvamed.3.k' '_chuvamed.4.k' '_chuvamed.5.k' '_chuvamed.6.k' '_chuvamed.7.k' '_chuvamed.8.k' '_chuvamed.9.k' '_chuvamed.10.k' '_chuvamed.11.k' '_chuvamed.12.k' '_chuvamed.13.k' '_chuvamed.14.k' '_chuvamed.15.k' '_chuvamed.16)

k=k+1
endwhile



return 










*--------------------
function  geramedia(bacia,nunlin,vargrads,numtempos,modolon,freq) 
*=====================================================================================================

*  
*  GERAMEDIA
*
*=====================================================================================================
*
* processa cada coordenada do arquivo de bacias
*
*
n=1
while ( n <= nunlin)
*
* pega novo registro 
*
id=getfile(2)
*
* trata lat e lon 
* verifica se usa lon sempre positivo ou  não (opcao:modolon) 
*
if (modolon = 1 )
lonxx=subwrd(_args,1)
lonx=lonxx+360
latx=subwrd(_args,2)
else
lonxx=subwrd(_args,1)
lonx=lonxx
latx=subwrd(_args,2)
endif 
*
*
*  
say "debugao [1] "lonx ' 'latx' 'n' 'modolon 
*
*  calcula chuva  para o ponto de grade no decorrer do tempo
*  valores são gravados em _valor.1, _valor.2 ...._valor.n 
*  
* 
if (freq=6)
pi=pegavalor6h(lonx,latx,vargrads,numtempos)
endif 
if (freq=24)
pi=pegavalor24h(lonx,latx,vargrads,numtempos)
endif 

*
* 

* CHUVA ACUMULADA DIARIA (_CHUVAACU.J) 
* acumulo em _chuvaacu o valor da chuva para cada tempo somando os valores de 
* cada ponto de grade 


 


* 
j=1
while (j<=numtempos)
*say _valor.j' '_chuvaacu.j
if (_valor.j >=0 )
_chuvaacu.j=_chuvaacu.j+_valor.j
endif 
j=j+1
endwhile     
n=n+1
endwhile  ******while ( n < nunlin)



*
*
* calcula media de cada dia no tempo
*
*
j=1
while (j<=numtempos)

_chuvamed.j=math_format("%06.2f",(_chuvaacu.j/nunlin))  

if (_chuvamed.j < 0) 
 _chuvamed.j=0.00
 endif 
 

say _chuvaacu.j' '_chuvamed.j
j=j+1
endwhile     
*
* atualiza relatorio debug.prn 
*
pi=pegadata(1)
ano1=_ano
mes1=_mesx
dia1=_dia
pi=pegadata(-1)
ano2=_ano
mes2=_mesx
dia2=_dia
*pi=labeldebug(" CHUVA DIARIA ACUMULADA E CHUVA MEDIA DIARIA NA BACIA - SEM REMOCAO DE VIES  ",1) 
*pi=write(_arq_debug,'chuvaacu          'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvaacu.1' '_chuvaacu.2' '_chuvaacu.3' '_chuvaacu.4' '_chuvaacu.5' '_chuvaacu.6' '_chuvaacu.7' '_chuvaacu.8' '_chuvaacu.9' '_chuvaacu.10' '_chuvaacu.11' '_chuvaacu.12' '_chuvaacu.13' '_chuvaacu.14' '_chuvaacu.15' '_chuvaacu.16)
*pi=write(_arq_debug,'chuvamed          'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvamed.1' '_chuvamed.2' '_chuvamed.3' '_chuvamed.4' '_chuvamed.5' '_chuvamed.6' '_chuvamed.7' '_chuvamed.8' '_chuvamed.9' '_chuvamed.10' '_chuvamed.11' '_chuvamed.12' '_chuvamed.13' '_chuvamed.14' '_chuvamed.15' '_chuvamed.16)

j=1
jj=1
while(j<=numtempos)

pi=pegadata(jj)
ano1=_ano
mes1=_mesx
dia1=_dia

pi=write(_prefixo"_"bacia".dat",ano1' 'mes1' 'dia1' '_chuvamed.j)

if (_postos = 1 ) 

pi=close("config_postos_ons.dat") 
status=0
while(status=0)
pi=read("config_postos_ons.dat") 
status=sublin(pi,1) 
args=sublin(pi,2)
subbacia=subwrd(args,1)
baciao=subwrd(args,2)
numpostos=subwrd(args,3) 
if (subbacia = bacia) 
k=1
while(k<=numpostos)
posto=subwrd(args,k+3)
filenc=baciao"_"posto"_C.txt"

ju=write(filenc,posto' 'dia1'/'mes1'/'ano1' 1000 '_chuvamed.j) 
k=k+1
endwhile
endif
endwhile 
endif *** _postos=1
j=j+1
if ( freq=6) 
jj=jj+4
endif
if ( freq=24)
jj=jj+1
endif
endwhile

return 


function criapostosons(bacia)

pi=read("config_postos_ons.dat") 
args=sublin(pi,2)
subbacia=subwrd(args,1)
baciao=subwrd(args,2)
numpostos=subwrd(args,3) 
if (subbacia = bacia) 


endif






 


return 








----------------------

function pegapontos(bacia,nunlin,vargrads,numtempos,modolon,rmv,freq) 
*=====================================================================================================
*  
*   PEGAPONTOS
*
*=====================================================================================================
*
* processa cada coordenada do arquivo de bacias
*
*
n=1
while ( n <= nunlin)
*
* pega novo registro 
*
id=getfile(2)
*
* trata lat e lon 
* verifica se usa lon sempre positivo ou  não (opcao:modolon) 
*
if (modolon = 1 )
lonxx=subwrd(_args,1)
lonx=lonxx+360
latx=subwrd(_args,2)
else
lonxx=subwrd(_args,1)
lonx=lonxx
latx=subwrd(_args,2)
endif 
*
*
*  
say "debugao [1] "lonx ' 'latx' 'n' 'modolon 
*
*  calcula chuva  para o ponto de grade no decorrer do tempo
*  valores são gravados em _valor.1, _valor.2 ...._valor.n 
*  
* 
if (freq=6)
pi=pegavalor6h(lonx,latx,vargrads,numtempos)
endif 
if (freq=24)
pi=pegavalor24h(lonx,latx,vargrads,numtempos)
endif 

*
* 

* CHUVA ACUMULADA DIARIA (_CHUVAACU.J) 
* acumulo em _chuvaacu o valor da chuva para cada tempo somando os valores de 
* cada ponto de grade 


 


* 
j=1
while (j<=numtempos)
*say _valor.j' '_chuvaacu.j
_chuvaacu.j=math_format("%06.2f",_chuvaacu.j+_valor.j)
j=j+1
endwhile     
n=n+1
endwhile  ******while ( n < nunlin)
*
*
* calcula media de cada dia no tempo
*
*
j=1
while (j<=numtempos)
_chuvamed.j=math_format("%06.2f",_chuvaacu.j/nunlin) 
j=j+1
endwhile     
*
* atualiza relatorio debug.prn 
*
pi=pegadata(1)
ano1=_ano
mes1=_mesx
dia1=_dia
pi=pegadata(-1)
ano2=_ano
mes2=_mesx
dia2=_dia
pi=labeldebug(" CHUVA DIARIA ACUMULADA E CHUVA MEDIA DIARIA NA BACIA - SEM REMOCAO DE VIES  ",1) 
pi=write(_arq_debug,'chuvaacu          'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvaacu.1' '_chuvaacu.2' '_chuvaacu.3' '_chuvaacu.4' '_chuvaacu.5' '_chuvaacu.6' '_chuvaacu.7' '_chuvaacu.8' '_chuvaacu.9' '_chuvaacu.10' '_chuvaacu.11' '_chuvaacu.12' '_chuvaacu.13' '_chuvaacu.14' '_chuvaacu.15' '_chuvaacu.16)
pi=write(_arq_debug,'chuvamed          'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvamed.1' '_chuvamed.2' '_chuvamed.3' '_chuvamed.4' '_chuvamed.5' '_chuvamed.6' '_chuvamed.7' '_chuvamed.8' '_chuvamed.9' '_chuvamed.10' '_chuvamed.11' '_chuvamed.12' '_chuvamed.13' '_chuvamed.14' '_chuvamed.15' '_chuvamed.16)





if (_formato="CMB" | _formato ="ALL" ) 
j=1
while(j<=numtempos)
pi=pegadata(j)
pi=write(_modelo"_"bacia".bacia",_ano' '_mesx' '_dia' '_chuvamed.j) 
j=j+1
endwhile  
endif 





return 





*=========================================================================================
function remocaovies(bacia,nunlin,numtempos,modolon,freq) 
*
*

*
* 
* remocao vies
*
* Primeiro acha o acumualdo de cada dia da media dos pontos de grade 
*
*  Pagina 44 da nota tecnica ONS 156 R4 
*  Modelos chuva_vazao-processamento pag. 13 
* 
ptotmod10dias=0
j=1
while (j<=numtempos)
ptotmod10dias=ptotmod10dias+_chuvamed.j
j=j+1
endwhile
say "Debugao[9] "ptotmod10dias' 'j' '_chuvamed.1
*
* Pega os parametros no arquivo config.dat 
* arquivo ocntem os aprametros a b  e limites 
*
pi=pegadata(1)
pi=pegaparam(bacia)   
say _limite1dia
say _limite10dias
say _a
say _b
say _baciap



*
* aplicar remocao de vies 
*
ptotprmod10dias=_a*(ptotmod10dias*ptotmod10dias)+(_b*ptotmod10dias) 
*
* verifica se o limite 10 dias foi atingido
*
if ( ptotprmod10dias > _limite10dias ) 
  ptotprmod10lim = _limite10dias 
else
  ptotprmod10lim  =  ptotprmod10dias
endif 
*
* aplica limite  do dia 
*
* calcula alfa
*
alfa=ptotprmod10lim/(ptotmod10dias+0.000001) 
*say alfa
*
* calcula pprlim10dias 
*
j=1
while (j<=numtempos)
_ppr10dias.j=alfa*_chuvamed.j
*
* verifica limite diario 
*
if (_ppr10dias.j > _limite1dia ) 
_ppr1dia.j = _limite1dia
else
_ppr1dia.j = _ppr10dias.j 
endif 
j=j+1
endwhile

* aplica limite de 1 dia     
*
j=1
while(j<=numtempos)
say "[5] debugao"_ppr1dia.j' '_chuvamed.j' 'j
_beta.j=_ppr1dia.j/(_chuvamed.j+0.000001) 
j=j+1
endwhile 
*
* atualiza relatorio 
*
pi=pegadata(1)
ano1=_ano
mes1=_mesx
dia1=_dia
pi=pegadata(-1)
ano2=_ano
mes2=_mesx
dia2=_dia

pi=labeldebug(" PROCESSO DE REMOCAO DE VIES  ",1) 

pi=write(_arq_debug,'pprlim10dias      'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_ppr10dias.1' '_ppr10dias.2' '_ppr10dias.3' '_ppr10dias.4' '_ppr10dias.5' '_ppr10dias.6' '_ppr10dias.7' '_ppr10dias.8' '_ppr10dias.9' '_ppr10dias.10' '_ppr10dias.11' '_ppr10dias.12' '_ppr10dias.13' '_ppr10dias.14' '_ppr10dias.15' '_ppr10dias.16)
pi=write(_arq_debug,'pprlim1dia        'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_ppr1dia.1' '_ppr1dia.2' '_ppr1dia.3' '_ppr1dia.4' '_ppr1dia.5' '_ppr1dia.6' '_ppr1dia.7' '_ppr1dia.8' '_ppr1dia.9' '_ppr1dia.10' '_ppr1dia.11' '_ppr1dia.12' '_ppr1dia.13' '_ppr1dia.14' '_ppr1dia.15' '_ppr1dia.16)
pi=write(_arq_debug,'beta              'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_beta.1' '_beta.2' '_beta.3' '_beta.4' '_beta.5' '_beta.6' '_beta.7' '_beta.8' '_beta.9' '_beta.10' '_beta.11' '_beta.12' '_beta.13' '_beta.14' '_beta.15' '_beta.16)


*
* parte ruminante 
* 
* no arquivo .temp.prn foi gravado os pontos de grades e seus valores no tempo .agora seroa relidos e reprocessados com a remoção do vies
* 


*
* fecha o arquivo .temp.prn 
*
pi=close(".temp.prn") 
pi=getfiletmp(0)
result=sublin(_id2,2)
nf=subwrd(result,1) 
n=1
*
* reprocessa 
* 
pi=write(_baciap,_modelo' '_bacia' 'bacia' 'nunlin' 'ano' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2)   

pi=labeldebug(" CHUVA POR PONTO DE GRADE COM REMOCAO DE VIES ",1) 



while(n<=nf)
pi=getfiletmp(0)
result=sublin(_id2,2)
*say result
latx=subwrd(result,1)
lonxx=subwrd(result,2)
if (lonxx > 180 )
lonx=lonxx-360
else
lonx=lonxx
endif
*
*
*
j=1
while(j<=numtempos)
valor=subwrd(result,8+j)
say bacia' 'valor' '_beta.j' 'n' 'j
_chuvapg.j=math_format("%06.2f",valor*_beta.j) 

if (_chuvapg.j <=0 )
_chuvapg.j = 0.00
endif 


j=j+1
endwhile 
pi=write(_arq_debug,latx' 'lonx' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvapg.1' '_chuvapg.2' '_chuvapg.3' '_chuvapg.4' '_chuvapg.5' '_chuvapg.6' '_chuvapg.7' '_chuvapg.8' '_chuvapg.9' '_chuvapg.10' '_chuvapg.11' '_chuvapg.12' '_chuvapg.13' '_chuvapg.14' '_chuvapg.15' '_chuvapg.16)
pi=write(_baciap,latx' 'lonx' '_chuvapg.1' '_chuvapg.2' '_chuvapg.3' '_chuvapg.4' '_chuvapg.5' '_chuvapg.6' '_chuvapg.7' '_chuvapg.8' '_chuvapg.9' '_chuvapg.10' '_chuvapg.11' '_chuvapg.12' '_chuvapg.13' '_chuvapg.14' '_chuvapg.15' '_chuvapg.16)
*
* gravar arquivos formato ONS 
*
if (_formato="ONS" | _formato="ALL") 
j=1
jj=1
while(j<=numtempos)

if (freq = 6)
jj=jj+4
pi=geranomeons(jj,_prefixo) 
else
pi=geranomeons(j,_prefixo) 
endif 




flon=math_format("%6.2f",lonx)
flat=math_format("%6.2f",latx)
kh=write("omega.txt","j="j" jj="j" "_nomeons)


pi=write(_baciap'_'_nomeons,flon' 'flat' '_chuvapg.j) 
j=j+1
endwhile 
endif 
*
* GERAR CHUVA ACUMULADA POR BACIA
*
*
*
j=1
while(j<=numtempos)
_chuvaf.j=_chuvaf.j+_chuvapg.j 
j=j+1
endwhile 
n=n+1
endwhile 
*
* GERAR CHUVA MEDIA ACUMULADA POR BACIA
*
*
*
j=1
while(j<=numtempos)
*say j' '_chuvaf.j' 'nunlin' 'nf
_chuvamedf.j=math_format("%06.2f",(_chuvaf.j)/nf) 
j=j+1
endwhile 

pi=close(".temp.prn") 
*
* 
*
pi=labeldebug(" CHUVA MÉDIA DIARIA NA BACIA COM REMOCAO DE VIES  ",1) 
pi=write(_arq_debug,'_chuvamedf              'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvamedf.1' '_chuvamedf.2' '_chuvamedf.3' '_chuvamedf.4' '_chuvamedf.5' '_chuvamedf.6' '_chuvamedf.7' '_chuvamedf.8' '_chuvamedf.9' '_chuvamedf.10' '_chuvamedf.11' '_chuvamedf.12' '_chuvamedf.13' '_chuvamedf.14' '_chuvamedf.15' '_chuvamedf.16)
* 
*
* 

*
*  previsao por conjunto 
* PGEFS sb,d . pesoGEFS B,d
*
pi=pegadata(1)
ano1=_ano
mes1=_mesx
dia1=_dia
pi=pegadata(-1)
ano2=_ano
mes2=_mesx
dia2=_dia
if (_conjunto=1) 
if (_modelo = "GEFSRMV" ) 

pi=close("RELACAO_GEFS_CONJUNTO.dat") 
pi=read("RELACAO_GEFS_CONJUNTO.dat") 
status=sublin(pi,1) 
if ( status < 0)
say "erro arquivo RELACAO_GEFS_CONJUNTO.dat" 
return 
endif 


args=sublin(pi,2) 
numreg=subwrd(args,1)
j=1
*say "debugao[17] "numreg 
*pi=write("conjunto_"_modelo'_'_baciap'.dat',numreg) 
while (j<=numreg) 
pi=read("RELACAO_GEFS_CONJUNTO.dat") 
args=sublin(pi,2) 
agrupa=subwrd(args,1)
baciad=subwrd(args,2)
  
if (bacia =  agrupa )
i=1  
while(i<=numtempos)
peso.i=subwrd(args,4+i) 
*say peso.i' 'i ' '_chuvamedf.i
_chuvaconj.i=math_format("%06.2f",_chuvamedf.i*peso.i ) 
i=i+1
endwhile

pi=write("conjunto_"_modelo'_'_baciap'.dat',baciad' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvaconj.1' '_chuvaconj.2' '_chuvaconj.3' '_chuvaconj.4' '_chuvaconj.5' '_chuvaconj.6' '_chuvaconj.7' '_chuvaconj.8' '_chuvaconj.9' '_chuvaconj.10' '_chuvaconj.11' '_chuvaconj.12' '_chuvaconj.13' '_chuvaconj.14' '_chuvaconj.15' '_chuvaconj.16)
*pi=labeldebug(" CHUVA MÉDIA DIARIA NA BACIA COM REMOCAO DE VIES X  PESO   ",1) 
*pi=write(_arq_debug,baciad' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvaconj.1' '_chuvaconj.2' '_chuvaconj.3' '_chuvaconj.4' '_chuvaconj.5' '_chuvaconj.6' '_chuvaconj.7' '_chuvaconj.8' '_chuvaconj.9' '_chuvaconj.10' '_chuvaconj.11' '_chuvaconj.12' '_chuvaconj.13' '_chuvaconj.14' '_chuvaconj.15' '_chuvaconj.16)
endif 
j=j+1
endwhile 
endif


*
*  previsao conjunyo ETA 
*
if (_modelo="ETA40" ) 
pi=close("RELACAO_ETA_CONJUNTO.dat") 
pi=read("RELACAO_ETA_CONJUNTO.dat") 
status=sublin(pi,1) 
if ( status < 0)
say "erro arquivo RELACAO_ETA_CONJUNTO.dat" 
return 
endif 

args=sublin(pi,2) 
numreg=subwrd(args,1)
j=1
say "debugao[18] "numreg 

while (j<=numreg) 
pi=read("RELACAO_ETA_CONJUNTO.dat") 
args=sublin(pi,2) 
baciad=subwrd(args,1)
  
if (bacia =  baciad )
i=1  
while(i<=numtempos)
peso.i=subwrd(args,2+i) 
say peso.i' 'i ' '_chuvamedf.i
_chuvaconj.i=math_format("%06.2f",_chuvamedf.i*peso.i ) 
i=i+1
endwhile
pi=write("conjunto_"_modelo'_'_baciap'.dat',baciad' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvaconj.1' '_chuvaconj.2' '_chuvaconj.3' '_chuvaconj.4' '_chuvaconj.5' '_chuvaconj.6' '_chuvaconj.7' '_chuvaconj.8' '_chuvaconj.9' '_chuvaconj.10' '_chuvaconj.11' '_chuvaconj.12' '_chuvaconj.13' '_chuvaconj.14' '_chuvaconj.15' '_chuvaconj.16)

*pi=labeldebug(" CHUVA MÉDIA DIARIA NA BACIA COM REMOCAO DE VIES X  PESO   ",1) 
*pi=write(_arq_debug,baciad' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_chuvaconj.1' '_chuvaconj.2' '_chuvaconj.3' '_chuvaconj.4' '_chuvaconj.5' '_chuvaconj.6' '_chuvaconj.7' '_chuvaconj.8' '_chuvaconj.9' '_chuvaconj.10' '_chuvaconj.11' '_chuvaconj.12' '_chuvaconj.13' '_chuvaconj.14' '_chuvaconj.15' '_chuvaconj.16)
endif 
j=j+1
endwhile 
endif
endif 


if (_formato="CMB" | _formato="ALL") 
j=1
while(j<=numtempos)
pi=pegadata(j)
pi=write(_modelo"_"bacia".bacia.rmv",_ano' '_mesx' '_dia' '_chuvamedf.j) 
j=j+1

endwhile  

 endif 
return 








*----------------------------------------------------------------------------
* 
* FUNCAO PEGAVALOR6H (PARA DADOS DE 6 EM 6 HORAS)  
function pegavalor6h(lonx,latx,vargrads,numtempos)
*
* inicializa os vetores
*
t=1
while (t<=numtempos)
_valor.t=0
t=t+1
endwhile 
*
* processa no tempo (a cada 6 horas gera valor diario)
*
*  chuva = (t,t+3)  
* 
*
t=1
tt=1
while (t<=numtempos)
*
* pega data
*
pi=pegadata(tt)
*
* processa EM LAT LON NO DECORRER DO TEMPO E GUARDA EM VALOR.J ONDE J EH TEMPO
* O TEMPO < =  NUMTEMPOS 
* 
'set lon 'lonx
'set lat 'latx
*
*---------------------------------------------------------------------
* 
*  CADA MODELO E TIPO DE MODELO TEM UM BLOCO DEFINIDO 
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

if (_modelo="GFS" ) 

* GEFS - um membro 
*
'd  sum('vargrads',t='tt',t='tt+3')'
*
*say result
var=sublin(result,2)
valor=subwrd(var,4) 
verify=subwrd(var,1) 
_valor.t=valor
if (valor < 0) 
_valor.t=0.00
endif 

*
endif 
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


if (_modelo="GEFSRMV" | _modelo ="GEFS") 
*
*  GEFS - media dos membros segundo ONS. 
*
'd  ave(sum('vargrads',t='tt',t='tt+3'),e=1,e=21)'
*
var=sublin(result,23)
valor=subwrd(var,4) 
verify=subwrd(var,1) 
if (valor < 0) 
valor=0.00
endif 

_valor.t=math_format("%06.2f",valor)
endif 

*
*
*
pi=write("tempos6h",t" "tt) 
tt=tt+4
t=t+1
endwhile 

*------------------------------------------------------------------------
* 
* atualiza relatorio   
* .temp.prn  ==> relaorio que vai ser usado para calculo no futuro  
* debug.prn -> mostra as etapas dos calculos por ponto de grade e tempo 
* 
* pega data do tempo 1 e last 
*
pi=pegadata(1)
ano1=_ano
mes1=_mesx
dia1=_dia
pi=pegadata(tt-1)
ano2=_ano
mes2=_mesx
dia2=_dia
*
*
pi=write(_arq_debug,latx' 'lonx' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_valor.1' '_valor.2' '_valor.3' '_valor.4' '_valor.5' '_valor.6' '_valor.7' '_valor.8' '_valor.9' '_valor.10' '_valor.11' '_valor.12' '_valor.13' '_valor.14' '_valor.15' '_valor.16)
pi=write(".temp.prn",latx' 'lonx' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_valor.1' '_valor.2' '_valor.3' '_valor.4' '_valor.5' '_valor.6' '_valor.7' '_valor.8' '_valor.9' '_valor.10' '_valor.11' '_valor.12' '_valor.13' '_valor.14' '_valor.15' '_valor.16)
return






*----------------------------------------------------------------------------
* 
* FUNCAO PEGAVALOR24H (PARA DADOS DE 6 EM 6 HORAS)  
function pegavalor24h(lonx,latx,vargrads,numtempos)
*
* inicializa os vetores
*
t=1
while (t<=numtempos)
_valor.t=0
t=t+1
endwhile 
*
* processa no tempo (a cada 24 horas gera valor diario)
*
*  chuva = (t,t+1)  
* 
*
t=1
while (t<=numtempos)
*
* pega data
*
pi=pegadata(t)
*
* processa EM LAT LON NO DECORRER DO TEMPO E GUARDA EM VALOR.J ONDE J EH TEMPO
* O TEMPO < =  NUMTEMPOS 
* 
'set lon 'lonx
'set lat 'latx
*
*---------------------------------------------------------------------
* 
*  CADA MODELO E TIPO DE MODELO TEM UM BLOCO DEFINIDO 
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 


'd  'vargrads''
*
valor=subwrd(result,4) 
verify=subwrd(var,1) 
_valor.t=math_format("%06.2f",valor)



*
*
*
t=t+1
endwhile 

*------------------------------------------------------------------------
* 
* atualiza relatorio   
* .temp.prn  ==> relaorio que vai ser usado para calculo no futuro  
* debug.prn -> mostra as etapas dos calculos por ponto de grade e tempo 
* 
* pega data do tempo 1 e last 
*
pi=pegadata(1)
ano1=_ano
mes1=_mes
dia1=_dia
pi=pegadata(t-1)
ano2=_ano
mes2=_mes
dia2=_dia
*
*
pi=write(_arq_debug,latx' 'lonx' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_valor.1' '_valor.2' '_valor.3' '_valor.4' '_valor.5' '_valor.6' '_valor.7' '_valor.8' '_valor.9' '_valor.10' '_valor.11' '_valor.12' '_valor.13' '_valor.14' '_valor.15' '_valor.16)
pi=write(".temp.prn",latx' 'lonx' 'ano1' 'mes1' 'dia1' 'ano2' 'mes2' 'dia2' '_valor.1' '_valor.2' '_valor.3' '_valor.4' '_valor.5' '_valor.6' '_valor.7' '_valor.8' '_valor.9' '_valor.10' '_valor.11' '_valor.12' '_valor.13' '_valor.14' '_valor.15' '_valor.16)
return







****-----------------------------------------------------------------------------

*--------------------------------------------------------------------------------
*
* funcao que abre o arquivo de bacias contendo os pontos de grade para os calculos
* 
* quem controla é a variavel global _arqbacia 
*
*
function getfile()
_id=read(_arqbacia)
_status=sublin(_id,1)   
_args=sublin(_id,2)
if (_status = 2 )
say "[ERRO 01] Problemas com "_arqbacia 
return 
endif 


return 

function getfiletmp()

_id2=read(".temp.prn")
status=sublin(_id2,1)   
_args2=sublin(_id2,2)
if (status < 0 )
'quit'
endif 
return 





function geranomeons(tempo,prefixo)


if  (_modelo="ETA40") 
pi=pegadata(0)
ano=math_format("%02g",_ano-2000) 
mes=math_format("%02g",_mesx)
dia=math_format("%02g",_dia) 
t=tempo
pi=pegadata(t)
ano2=math_format("%02g",_ano-2000) 
mes2=math_format("%02g",_mesx)
dia2=math_format("%02g",_dia) 
_nomeons=prefixo'_p'dia''mes''ano'a'dia2''mes2''ano2'.dat' 
else
pi=pegadata(1)
ano=math_format("%02g",_ano-2000) 
mes=math_format("%02g",_mesx)
dia=math_format("%02g",_dia) 
t=tempo+1
pi=pegadata(t)
ano2=math_format("%02g",_ano-2000) 
mes2=math_format("%02g",_mesx)
dia2=math_format("%02g",_dia) 
_nomeons=prefixo'_p'dia''mes''ano'a'dia2''mes2''ano2'.dat' 
endif 


return 






function pegadata(tempo)

if (tempo = -1) 
'set t last'
else
'set t 'tempo
endif 

'q time'
datap=subwrd(result,3)
indica=substr(datap,3,1)
if (indica =":" ) 
_ano=substr(datap,12,4)
_mes=substr(datap,9,3) 
_dia=substr(datap,7,2)
_hora=substr(datap,1,2)
_min=substr(datap,4,2)
else
_ano=substr(datap,9,4)
_mes=substr(datap,6,3) 
_dia=substr(datap,4,2)
_hora=substr(datap,1,2)
_min=00
endif


if (_mes = "JAN" ) ; _mesx=01; endif 
if (_mes = "FEB" ) ; _mesx=02; endif 
if (_mes = "MAR" ) ; _mesx=03; endif 
if (_mes = "APR" ) ; _mesx=04; endif 
if (_mes = "MAY" ) ; _mesx=05; endif 
if (_mes = "JUN" ) ; _mesx=06; endif 
if (_mes = "JUL" ) ; _mesx=07; endif 
if (_mes = "AUG" ) ; _mesx=08; endif 
if (_mes = "SEP" ) ; _mesx=09; endif 
if (_mes = "OCT" ) ; _mesx=10; endif 
if (_mes = "NOV" ) ; _mesx=11; endif 
if (_mes = "DEC" ) ; _mesx=12; endif 

return 





function labeldebug(texto,modo) 

if (modo = 0 )
pi=write(_arq_debug,texto)
return
endif

pi=write(_arq_debug,"*")
pi=write(_arq_debug,"*")
pi=write(_arq_debug,"*")
pi=write(_arq_debug,texto)
pi=write(_arq_debug,"*")
pi=write(_arq_debug,"*")
pi=write(_arq_debug,"*")
return 




function pegaparam2(bacia)

n=1

id=close(_arqconfig) 
id=read(_arqconfig) 
status=sublin(id,1)
if (status < 0) 
say "kkkkkkkkkkkkkkkkkkk"
return
endif
args=sublin(id,2) 
numcad=subwrd(args,1)


say "debugao [11] "numcad
while (n<=numcad)
id=read(_arqconfig) 
status=sublin(id,1)
if (status < 0) 
say "kkkkkkkkkkkkkkkkkkk"
return
endif

args=sublin(id,2) 
namebac=subwrd(args,1)
_baciap=subwrd(args,2)
*say "+++++>"namebac' '_baciap' '_mesx

 
if (bacia = namebac )
say "++++++> ACHOU A BACIA UHUUUU"namebac" "_baciap
pi=write("pista",bacia' 'nomebac' '_baciap' 'n) 
pi=pegadata(1)


if (_mesx >=9 & _mesx <=11) 
_limite1dia=subwrd(args,15)
_limite10dias=subwrd(args,19)
_a=subwrd(args,3)
_b=subwrd(args,4)
return
endif



if (_mesx >=12 | _mesx =1) 
_limite1dia=subwrd(args,15)
_limite10dias=subwrd(args,16)
_a=subwrd(args,6)
_b=subwrd(args,7)
return
endif

if (_mesx >=2 &  _mesx <=4) 
_limite1dia=subwrd(args,15)
_limite10dias=subwrd(args,17)
_a=subwrd(args,9)
_b=subwrd(args,10)
return
endif

if (_mesx >=5 &  _mesx <=8) 
_limite1dia=subwrd(args,15)
_limite10dias=subwrd(args,18)
_a=subwrd(args,12)
_b=subwrd(args,13)
return
endif


endif
n=n+1
endwhile
return 




function pegaparam(bacia)

n=1

id=close(_arqconfig) 
id=read(_arqconfig) 
status=sublin(id,1)
if (status < 0) 
say "kkkkkkkkkkkkkkkkkkk"
return
endif
args=sublin(id,2) 
numcad=subwrd(args,1)


say "debugao [11] "numcad
while (n<=numcad)
id=read(_arqconfig) 
status=sublin(id,1)
if (status = 2) 
say "kkkkkkkkkkkkkkkkkkk"
return
endif

args=sublin(id,2) 



pi=pegadata(1)
namebac=subwrd(args,1)
_baciap=subwrd(args,2)
*say "+++++>"namebac' '_baciap' '_mesx


indice=_mesx*5-2

if (bacia = namebac )


_a=subwrd(args,indice)
_b=subwrd(args,indice+1)
_lim=subwrd(args,indice+2)
_limite1dia=subwrd(args,indice+3)
_limite10dias=subwrd(args,indice+4)
return 
endif 

n=n+1
endwhile
return 













