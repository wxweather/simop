#!/bin/bash

dia="$(date +'%d')"
mes="$(date +'%m')"
ano="$(date +'%Y')"
hora="$(date +'%X')"
arq="log-incremental_"$ano$mes$dia"_"$hora".txt"

function INC () {

echo " "
echo "INCREMENTAIS"
echo "Execucao iniciada em: "$dia"."$mes"."$ano as $hora
echo " "


# Executa os scripts individuais
./incremental_FURNAS.sh $1
./incremental_PCOLOMBIA.sh $1
./incremental_MARIMBONDO.sh $1
./incremental_ITAIPU.sh $1

# Inserir aqui outros postos flu

estecaminho=$(pwd)
echo "Arquivo de log salvo em: "
echo $estecaminho"/LOG/"$arq

}

INC $1 | tee 'LOG/'$arq



