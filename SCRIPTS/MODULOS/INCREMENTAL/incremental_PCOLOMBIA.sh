#!/bin/bash

# O posto "quase-" incremental USINA_INC.txt eh fornecido pela Julia e nao considera os postos flu
# O objetivo deste script eh construir um arquivo USINA_INC_BAT.txt, de vazoes bat-incrementais, a ser usado pelo BATSMAP
# As vazoes USINA_INC_BAT correspondem a USINA_INC subtraindo os postos flu considerando os tempo de viagem
# As vazoes USINA_INC_BAT sao copiadas do arquivo de ontem. Apenas o valor de hoje é calculado.
# O calculo da bat-incremental de hoje considera vazoes dos postos flu de dias anteriores, de acordo com tempos de viagem.

######## Variaveis especificas desta usina (mudar) ######## 
bacia="Grande"
usina="PCOLOMBIA" 
numlinhas=45 #  (50 no caso de Itaipu)
# Postos flu e tempos de viagem (tv) em horas
x1='CAPESCURO.txt'
# x1="PBUENOS.txt"
# x2="PARAGUACU.txt" 
tv_x1=8.0 
# tv_x2=10.0
########################################################### 

echo "------------------------------------------"
echo "Calculo da vazao incremental: $usina"


fileinc=$usina"_INC.txt"
fileincbat=$usina"_INC_BAT.txt"
fileincbatm1=$usina"_INC_BAT_M1.txt"
if test -z $1 ;then 
primeirodia=1
else 
let primeirodia=($1+1)
fi 
if test -z $ROD ;then 
ROD='00'
fi 
rod=$ROD
# Hoje
proxdia="$(date --date="$((primeirodia-1)) day ago" +'%d')"
proxmes="$(date --date="$((primeirodia-1)) day ago" +'%m')"
proxano="$(date --date="$((primeirodia-1)) day ago" +'%Y')"
proxpath="../../../OUTPUT/$proxano$proxmes$proxdia$rod/SMAPONS/$bacia/ARQ_ENTRADA/"

# Ontem
ultdia="$(date --date="$primeirodia day ago" +'%d')"
ultmes="$(date --date="$primeirodia day ago" +'%m')"
ultano="$(date --date="$primeirodia day ago" +'%Y')"
ultpath="../../../OUTPUT/$ultano$ultmes$ultdia$rod/SMAPONS/$bacia/ARQ_ENTRADA/"

# Verifica se diretorio de HOJE ja contem o arquivo USINA_INC_BAT.txt
# Se sim, interrompe o script, nao precisamos fazer outro.
if [ -f $proxpath$fileincbat ]; then
    echo "O diretorio $proxano$proxmes$proxdia$rod ja contem arquivo $fileincbat."
    echo "Novas vazoes incrementais para $usina nao serao calculadas."
    echo "Abandonando script 'incremental_$usina.sh'" 
    exit 
fi

# Verifica se diretorio de ONTEM contem o arquivo USINA_INC_BAT.txt
# 1) Se tivermos, vamos copiar o arquivo de ontem, e atualizar somente com a vazao de HOJE (caso ideal).
# 2) Se nao tivermos, vamos calcular a bat-incremental inteira (45 ou 50 dias). 
if [ -f $ultpath$fileincbat ]; then
    
    # Caso 1
    # Copia a vazao bat-incremental de ontem no diretorio de hoje
    cp $ultpath$fileincbat $proxpath$fileincbatm1
	echo " "
	echo "Calculando vazao incremental de $usina para o dia $proxano$proxmes$proxdia$rod ..."
	flag=1
else
	# Caso 2
	echo " "
    echo "Nao existe arquivo $fileincbat no diretorio $ultano$ultmes$ultdia$rod."
    echo "A vazao incremental sera calculada para $numlinhas dias."
    flag=0
fi

# dos2unix
cd $proxpath
dos2unix $x1; # dos2unix $x2; 
dos2unix $fileinc; 
if [ $flag -eq 1 ]; then 
	dos2unix $fileincbatm1 
fi

if [ $flag -eq 1 ]; then
	# Remove a primeira linha do arquivo USINA_INC_BAT_M1 e adiciona uma vazia no final (hoje)
	# adicionmos uma linha no final somente para este arquivo continuar com 45 linhas (50 no caso de Itaipu)
	# e nao dar problema na hora do loop abaixo
	sed -i '1d' $fileincbatm1 
	echo " " >> $fileincbatm1
fi

# Guarda as vazoes dos postos flu, inc de hoje e bat-inc de ontem em arrays
if [ $flag -eq 1 ]; then
	linha=0
	while read linex1<&3 && read lineqinc<&5 && read lineqincbatm1<&6; do
		qx1[`echo $linha`]=`cut -d '|' -f6- <<<$linex1`
    	#qx2[`echo $linha`]=`cut -d '|' -f6- <<<$linex2`
    	qinc[`echo $linha`]=`cut -d '|' -f6- <<<$lineqinc`
    	qincbatm1[`echo $linha`]=`cut -d '|' -f6- <<<$lineqincbatm1` # o ultimo valor deste array eh vazio
    	linha=$((linha+1))   
	done 3<$x1 5<$fileinc 6<$fileincbatm1
else
 	linha=0
	while read linex1<&3 && read lineqinc<&5; do
   		qx1[`echo $linha`]=`cut -d '|' -f6- <<<$linex1`
   		#qx2[`echo $linha`]=`cut -d '|' -f6- <<<$linex2`
   		qinc[`echo $linha`]=`cut -d '|' -f6- <<<$lineqinc`
   		linha=$((linha+1))   
	done 3<$x1 5<$fileinc
fi

#################### Calculo da Vazao BAT-INCREMENTAL ####################

# Calcula as vazoes dos postos flu propagadas de acordo com os tempos de viagem (tv)
linha=0
while [ $linha -lt $numlinhas ]; do
   	if [ $linha -eq 0 ]; then
       	# No caso do primeiro dia (45 dias atras), utilizamos a vazao do proprio posto
       	# Essa aproximacao sera usada somente no caso 2 (flag=0)
       	qx1_prop[`echo $linha`]=${qx1[$linha]}
       	#qx2_prop[`echo $linha`]=${qx2[$linha]}
   	else
       	# Calculo dos postos flu propagados
       	qx1_prop[`echo $linha`]=`echo "scale=2; ( ${qx1[$linha]} *(24.0 -$tv_x1) +  ${qx1[$linha -1 ]} *$tv_x1 ) /24.0" | bc`
       	# qx2_prop[`echo $linha`]=`echo "scale=2; ( ${qx2[$linha]} *(24.0 -$tv_x2) +  ${qx2[$linha -1 ]} *$tv_x2 ) /24.0" | bc`
   	fi
   	# Calculo da vazao bat-incremental!!
   	# Caso 1:
   	# Utilizamos o calculo da propagacao somente na ultima linha no caso 1 e 
   	# Copiamos  as linhas 0 a 43 do arquivo de vazao bat-incremental de ontem
   	if [ $flag -eq 1 ]; then
   		if [ $linha -lt $((numlinhas-1)) ]; then
       		q_bat_inc[`echo $linha`]=${qincbatm1[$linha]}
   		else
       		# q_bat_inc[`echo $linha`]=`echo "scale=2; ${qinc[$linha]} - ${qx1_prop[$linha]} - ${qx2_prop[$linha]}" | bc`
       		q_bat_inc[`echo $linha`]=`echo "scale=2; ${qinc[$linha]} - ${qx1_prop[$linha]}" | bc`
   		fi
   	# Caso 2:
   	# Calculamos a vazao incremental para todos os 45 ou 50 dias
   	# Os primeiros 1 ou 2 duas utilizam vazao dos postos nao propagadas
   	else
   	    # q_bat_inc[`echo $linha`]=`echo "scale=2; ${qinc[$linha]} - ${qx1_prop[$linha]} - ${qx2_prop[$linha]}" | bc`
   	    q_bat_inc[`echo $linha`]=`echo "scale=2; ${qinc[$linha]} - ${qx1_prop[$linha]}" | bc`
   	fi
   	linha=$((linha+1)) 
done

# Pega os campos de 1 a 5 do arquivo quase incremental
cut -d '|' -f 1,2,3,4,5 $fileinc > "TEMP.txt"

# Adiciona a vazao calculada no sexto campo
for f in ${q_bat_inc[*]}; do read x; echo "$x|${f%%??????????????}" >> $usina"_INC_BAT.txt" ; done < "TEMP.txt"
rm TEMP.txt

if [ $flag -eq 1 ]; then
	rm $fileincbatm1
fi	

echo "Vazao incremental de $usina calculada com sucesso."

cd ../../../../../SCRIPTS/MODULOS/INCREMENTAL/


    

