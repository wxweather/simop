dset ../../../OBS/OISST/OISST_MENSAL_%y4%m2.nc
title 
options template 
undef 9.999e+20 
dtype netcdf
xdef 360 linear -0.5 1
ydef 180 linear -89.5 1
zdef 1 linear 0 1
tdef 500 linear 00Z01NOV1981 1mo
vars 1
tmpsfc=>tmpsfc  0  t,y,x  ** surface Temp. [K]
endvars
