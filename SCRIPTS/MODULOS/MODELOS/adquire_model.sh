#!/bin/bash

bagen()
{

case $1 in
	"ANAL")
		wget -nc $2
		./g2ctl.pl $3 > cfs.ctl 
		gribmap -i cfs.ctl
		echo "'open cfs.ctl'" > script.gs
		echo "'set lon 150 360'"  >>script.gs
		echo "'set lat -60 20' " >>script.gs
		echo "'lats4d -o "$4" '"           >>script.gs
		echo "'quit'" >>script.gs
		grads -lbc "script.gs" >>./LOG.prn 2>&1
		;;
	"CFS") 
		wget -nc $2
		./g2ctl.pl $3 > cfs.ctl 
		gribmap -i cfs.ctl
		echo "'open cfs.ctl'" > script.gs
		echo "'set lon 260 360'"  >>script.gs
		echo "'set lat -60 20' " >>script.gs
		echo "'lats4d -o "$4" '"           >>script.gs
		echo "'quit'" >>script.gs
		grads -lbc "script.gs" >>./LOG.prn 2>&1
		;;
	"CFSENS")
	         #

			./g2ctl.pl  $2 >cfs.ctl 
			 gribmap -i cfs.ctl   >>./LOG.prn 2>&1
			echo "'open cfs.ctl'" > script.gs
			echo "'set lon 260 360'"  >>script.gs
			echo "'set lat -60 20' " >>script.gs
			
			echo "'lats4d -levs "$levs" -vars "$vars" -o "$filenc" '"           >>script.gs
			echo "'quit'" >>script.gs
			grads -lbc "script.gs" >>./LOG.prn 2>&1
		    ;;
	*)
			wget -nc $1
		./g2ctl.pl $2 > cfs.ctl 
		gribmap -i cfs.ctl
		echo "'open cfs.ctl'" > script.gs
		echo "'set lon 280 360'"  >>script.gs
		echo "'set lat -35 10' " >>script.gs
		echo "'lats4d -o "$3" '"           >>script.gs
		echo "'quit'" >>script.gs
		grads -lbc "script.gs" >>./LOG.prn 2>&1
		;;
esac		

}



get_cfs_vars()
{

#-------------------------------------------------------------------------------------
#  SIMOP 3.0 - SISTEMA DE APOIO A PREVISAO 
#
#  Sistema que adquire dados observados, rodadas de modelos, calcula medias e afins e 
#  gera figuras. 
#
#  By ReginaldoVentura de sa (regis@lamma.ufrj.br) 
#
#------------------------------------------------------------------------------------  
# 
#
#     MODULO ADQURE MODEL 
#
#-----------------------------------------------------------------------------------
#---------------------------------------------------------------------------------
# BAIXA DADOS DO CFS - VERSAO 5 MESES POR VARIAVEL  
# 
#
#
# VERSAO 2.0 DESENVOLVIDA POR REGINALDO VENTURA DE SA (regis@lamma.ufrj.br) 
# --------------------------------------------------------------------------------
# 20/11/2017 - VERSAO 2.0 
#
#---------------------------------------------------------------------------------
#
# Força ter saidas em lingua inglesa
#
#data="0"
#for data in `seq 0 0`
#do

	#
	# pega a data de hoje 
	#
	#cfsdata=`date +"%Y%m%d" -d "$data days ago" `
	cfsdata=`date +"%Y%m%d" `
	
	#
	# define as rodadas que seroa processadas 
	#
	#rodadas="00 06 12 18"
	rodadas="00"
	
	#
	# loop das rodadas	
	#
	for rodada in $rodadas 
	do
		#
		#  cria os diretorios 
		#
		CFSDIR="../../CICC/"$cfsdata$rodada"/CFS/4MESES/"
		#mkdir ../../CICC/$cfsdata$rodada
		#mkdir -p ../../CICC/$cfsdata$rodada"/CFS/"
		mkdir -p $CFSDIR
		# 
		# define as variaves que serao adquiridas
		# 	 
		#vars="prate tmp2m  wnd10m wnd925 wnd850 wnd500 wnd250 wnd200"
		vars="prate tmp2m  " ###wnd925 wnd700 wnd500 wnd200"

		#
		# cria um direorio para cada variavel 
		# 
		for names in $vars
		do
			mkdir $CFSDIR$names
		done 
		echo "["`date`"] ADQUIRINDO DADOS CFS" 
		for names in $vars
		do
			for ens in `seq --format=%02g 1 4`
			do
				 if [ -f  $CFSDIR$names$filenc ]
				 then
					 echo "ja  existe"
				 else
					 filegrib=$names"."$ens"."$cfsdata$rodada".daily.grb2"
					 filenc="CFS_4MESES_"$names"_"$ens"_"$cfsdata$rodada".nc" 
					 wget -nc "http://nomads.ncep.noaa.gov/pub/data/nccf/com/cfs/prod/cfs/cfs."$cfsdata"/"$rodada"/time_grib_"$ens"/"$filegrib >>./LOG.prn 2>&1
					 if [ -f $filegrib ]
					 then
						 ./g2ctl.pl  $filegrib >cfs.ctl 
						 gribmap -i cfs.ctl 
						 echo "'open cfs.ctl'" > script.gs
						 echo "'set lon 150 360'"  >>script.gs
						 echo "'set lat -35 20' " >>script.gs
						 echo "'lats4d  -mxtimes 9999 -o "$filenc" '"           >>script.gs
						 echo "'quit'" >>script.gs
						 grads -lbc "script.gs"
						 mv $filenc $CFSDIR$names
					fi
				fi	  
			done 
		done
	done
#done










}

cfs_mensal2()
{
#-------------------------------------------------------------------------------------
# LAMMOC/UFF  - LABORATÓRIO DE MONITORAMENTO E MODELAGEM DO SISTEMA CLIM
# 
# WXWEATHER   - CONSULTORIA EM METEOROLOGIA E COMPUTAÇÃO DE ALTO DESEMPENHO  
#     
#-------------------------------------------------------------------------------------
# ADQUIRE MODEL -  Sistema para baixar e pré-processar dados de modelos de previsão
# numerica do tempo e climáticos
#
#  funcao cfs_mensal() 
#  baixa os dados do CFS mensal e cria um ensemble com n membros num unico arquivo. 
#  
#  
#
#
# -----------------------------------------------------------------------------------  
# 
#  Desenvolvido por: Reginaldo Ventura de Sa (regis@lamma.ufrj.br) 
#  Release 1 em :  27/03/2018     
#
#-----------------------------------------------------------------------------------
# Sintaxe:
# adquire_model.sh cfs 
#
#-----------------------------------------------------------------------------------------------
# CHANGE LOG
# 
#-----------------------------------------------------------------------------------------------
#
# INICIO
# Altero CDLOG por que  estou dentro do diretorio CFS do WORKDISK
# isso foi feito por que tenho que ter os ultimos 2 dias de arquivos armazenados
# e para evitar que se baixe de novo caso se rode no mesmo dia. 
#
CDLOG="../../../LOG/"`date +"%Y%m%d"`   
mkdir CFS >/dev/null 2>&1
cd CFS
echo "======= INICIO CFS MENSAL =============" >$CDLOG"/cfsmensal.prn" 2>&1
echo "INICIO DO PROCESSO CFS: "`date +"%Y%m%d %H%M"` >>$CDLOG"/cfsmensal.prn" 2>&1
echo "INICIO DO PROCESSO CFS: "`date +"%Y%m%d %H%M"`
tempo1=`date +"%s"`
DIA=`date +"%d"`
#
# vetor rodada com os horarios da rodad.
#
 rodada=( 00 18 12 06 00 )
#
# quantidade de dias dos meses  para transformar kg/s/m2 em mm 
#
diasnomes=(99 31 28  31  30  31  30  31  31  30  31  30   31)
#
# vetores com os tempos
#  tempo = vetor com a data de hoje , ontem e antes de ontem 
# previsoes =  YYYYMM  =>  vetor com as n previsoes 
# datagrads = data do grads para lead00 existir ou não.
#

 tempo=( `date +"%Y%m%d"`   `date +"%Y%m%d" -d"1 days ago"`  `date +"%Y%m%d" -d"2 days ago"`  `date +"%Y%m%d" -d"3 days ago"`  ) 
 
 
 previsoes=( `date +"%Y%m"`                `date +"%Y%m" -d"1 months "` `date +"%Y%m" -d"2 months "` 
              `date +"%Y%m" -d"3 months "` `date +"%Y%m" -d"4 months "` `date +"%Y%m" -d"5 months "`
			  `date +"%Y%m" -d"6 months "`  `date +"%Y%m" -d"7 months "` `date +"%Y%m" -d"8 months "` `date +"%Y%m" -d"9 months "` )
			  
			  
 mesprev=(   `date +"%-m"`  `date +"%-m" -d"1 months "`  `date +"%-m" -d"2 months "`  
              `date +"%-m" -d"3 months "`  `date +"%-m" -d"4 months "`  `date +"%-m" -d"5 months "` 
			  `date +"%-m" -d"6 months "`    `date +"%-m" -d"7 months "`  `date +"%-m" -d"8 months "`  `date +"%-m" -d"9 months "` )
 
 
datagrads=( `date +"00Z01%b%Y"` `date +"00Z01%b%Y" -d"1 months"`) 
 

#
# cria o nome do arquivo final 
# 
filenc="CFS_MENSAL_"${tempo[0]}${rodada[0]}".nc"  

LOCAL="../../../CICC/"`date +"%Y%m%d00"`  
mkdir -p $LOCAL"/CFS/ENS"    >/dev/null 2>&1 


if test -e $LOCAL"/CFS/ENS/"$filenc ;then 
XONSCHECK[3]=1
export XONSCHECK
echo "CFS MENSAL JA FEITO" 
return 
fi 


echo "BALANCETE DOS ENSEMBLES" > ensembles_novo 
membro=1
echo "MEMBRO = "$membro >> ensembles_novo 

	if test "$DIA" -le "10" ;then 
	
	   REGLEAD=0 
	   echo " TEREMOS O LEAD0"
	   else
	   echo " NAO TEREMOS O LEAD 1"
	   REGLEAD=1
	fi

n=0
# for k in `seq  $REGLEAD 7`
# do
	# file[$n]="flxf.01."${tempo[0]}${rodada[0]}"."${previsoes[$k]}".avrg.grib.grb2"   
	
	# let lead[$n]=(${mesprev[$k]}-${mesprev[0]}) 
	
	
	# if test ${lead[$n]} -lt 0 ;then 
		# let lead[$n]=(${lead[$n]}+12) 
	# fi
	

	
	
	# if test ! -e ${file[$n]} ;then 
		# link="http://nomads.ncep.noaa.gov/pub/data/nccf/com/cfs/prod/cfs/cfs."${tempo[0]}"/"${rodada[0]}"/monthly_grib_01/flxf.01."${tempo[0]}${rodada[0]}"."${previsoes[$k]}".avrg.grib.grb2"
		# echo "PROCESSANDO: "${file[$n]}   >>$CDLOG"/cfsmensal.prn" 2>&1 
		# echo "PROCESSANDO: "${file[$n]}" FAZENDO DOWNLOAD"  
		
		# wget -nc $link  >>$CDLOG"/cfsmensal.prn" 2>&1
		# else
		# echo "PROCESSANDO: "${file[$n]}"  DOWNLOAD JA FEITO" 
    # fi 
		# if test -e  ${file[$n]} ;then 
	# check[$n]="OK"
	# else
	# check[$n]="NAO BAIXADO"
	# fi 
	
    
	# echo "["$n"]"${file[$n]}" LEAD "${lead[$n]}" CHECK "${check[$n]}  >> ensembles_novo 
	
	
	# echo ${lead[$n]} "----" 
    # let n=(n+1) 
# done 


for i in `seq 1 2`
do
for j in `seq 1 4`
do
let membro=($membro+1) 
echo "*"  >> ensembles_novo 
echo "MEMBRO = "$membro >> ensembles_novo 
echo "*" >> ensembles_novo 
for k in `seq  $REGLEAD 7`
do
	file[$n]="flxf.01."${tempo[$i]}${rodada[$j]}"."${previsoes[$k]}".avrg.grib.grb2"   
	if test ! -e ${file[$n]} ;then 
		link="http://nomads.ncep.noaa.gov/pub/data/nccf/com/cfs/prod/cfs/cfs."${tempo[$i]}"/"${rodada[$j]}"/monthly_grib_01/flxf.01."${tempo[$i]}${rodada[$j]}"."${previsoes[$k]}".avrg.grib.grb2"
		echo "PROCESSANDO: "${file[$n]}   >>$CDLOG"/cfsmensal.prn" 2>&1
		echo "PROCESSANDO: "${file[$n]}" FAZENDO DOWNLOAD"  
		wget -nc $link  >>$CDLOG"/cfsmensal.prn" 2>&1
				else
		echo "PROCESSANDO: "${file[$n]}"  DOWNLOAD JA FEITO" 
    fi 
	if test -e  ${file[$n]} ;then 
	check[$n]="OK"
	else
	check[$n]="NAO BAIXADO"
	fi 
	let mesens[$n]=(${mesprev[$k]}) 
    let lead[$n]=(${mesprev[$k]}-${mesprev[0]}) 
	if test ${lead[$n]} -lt 0 ;then 
		let lead[$n]=(${lead[$n]}+12) 
	fi
	echo "["$n"]"${file[$n]}" LEAD "${lead[$n]}" CHECK "${check[$n]}  >> ensembles_novo 
	
    let n=(n+1) 
done 
done
done 

let n=(n-1)

rm CFSALL.BIN    2>&1 
for k in `seq 0 $n`
do
	echo ${file[$k]}" "${lead[$k]}  
	perl ../g2ctl.pl  ${file[$k]}  >cfs.ctl 
	gribmap -i cfs.ctl   >>$CDLOG"/cfsmensal.prn" 2>&1 2>&1
	#echo "'lats4d -i cfs.ctl -lon 280 330 -lat -35 10 -format sequential -vars  pratesfc  -func @*"${diasnomes[${lead[$k]}]}"*24*3600 -o arqout'"           >script.gs
	echo "'lats4d -i cfs.ctl -lon 280 330 -lat -35 10 -format stream -vars  pratesfc  -func @*"${diasnomes[${mesens[$k]}]}"*24*3600  -o arqout'"           >script.gs
	#echo "-func @*"${diasnomes[$mesprev]}"*24*3600" 
	echo "'quit'" >>script.gs
	grads -lbc "script.gs" >>$CDLOG"/cfsmensal.prn" 2>&1
    cat arqout.bin >> CFSALL.BIN 
	rm arqout.bin 
done 	




#
# se temos lead00  cria o arquivo com 7 variaveis , se não  com 6 varoaveis. 
#




if test "$REGLEAD" -eq  "0" 
then 
echo " ======================= LEAD00 EXISTE ========================"  >>$CDLOG"/cfsmensal.prn" 2>&1
echo "dset ./CFSALL.BIN" > cfsmensal.ctl 
echo "title cfs mensal " >>cfsmensal.ctl
echo "undef 9.999e+20" >>cfsmensal.ctl
echo "xdef 55 linear 279.375 0.9375" >>cfsmensal.ctl
echo "ydef 50 levels -35.433 -34.488 -33.543 -32.598 -31.653 -30.709 -29.764 -28.819" >>cfsmensal.ctl
echo " -27.874 -26.929 -25.984 -25.039 -24.094 -23.15 -22.205 -21.26 -20.315 -19.37"  >>cfsmensal.ctl
echo " -18.425 -17.48 -16.535 -15.59 -14.646 -13.701 -12.756 -11.811 -10.866 -9.921"  >>cfsmensal.ctl
echo " -8.976 -8.031 -7.087 -6.142 -5.197 -4.252 -3.307 -2.362 -1.417 -0.472"         >>cfsmensal.ctl
echo " 0.472 1.417 2.362 3.307 4.252 5.197 6.142 7.087 8.031 8.976"                   >>cfsmensal.ctl
echo " 9.921 10.866"                                                                  >>cfsmensal.ctl
echo "zdef 1 linear 0 1"                                                              >>cfsmensal.ctl  
echo "tdef 6 linear  "${datagrads[0]}" 1mo"                                                >>cfsmensal.ctl
echo "vars 8"                                                                         >>cfsmensal.ctl
echo "lead00  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "lead01  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "lead02  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl  
echo "lead03  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl 
echo "lead04  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "lead05  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "lead06  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "lead07  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
#echo "lead08  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
#echo "lead09  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "endvars"                                                                        >>cfsmensal.ctl  

echo "'lats4d -i cfsmensal.ctl  -o "$filenc" -q'"           >script.gs
grads -lbc "script.gs"            >>$CDLOG"/cfsmensal.prn" 2>&1		
LOCAL="../../../CICC/"`date +"%Y%m%d00"`  
mkdir -p $LOCAL"/CFS/ENS"    >/dev/null 2>&1 
if test -e $filenc ;then 
mv $filenc $LOCAL"/CFS/ENS" >/dev/null 2>&1
XONSCHECK[3]=1
export XONSCHECK
else
XONSCHECK[3]=-1
export XONSCHECK
fi 

else
echo " ======================= LEAD00 NAO EXISTE   ========================"  >>$CDLOG"/cfsmensal.prn" 2>&1

echo "dset ./CFSALL.BIN" > cfsmensal.ctl 
echo "title cfs mensal porra" >>cfsmensal.ctl
echo "undef 9.999e+20" >>cfsmensal.ctl
echo "xdef 55 linear 279.375 0.9375" >>cfsmensal.ctl
echo "ydef 50 levels -35.433 -34.488 -33.543 -32.598 -31.653 -30.709 -29.764 -28.819" >>cfsmensal.ctl
echo " -27.874 -26.929 -25.984 -25.039 -24.094 -23.15 -22.205 -21.26 -20.315 -19.37"  >>cfsmensal.ctl
echo " -18.425 -17.48 -16.535 -15.59 -14.646 -13.701 -12.756 -11.811 -10.866 -9.921"  >>cfsmensal.ctl
echo " -8.976 -8.031 -7.087 -6.142 -5.197 -4.252 -3.307 -2.362 -1.417 -0.472"         >>cfsmensal.ctl
echo " 0.472 1.417 2.362 3.307 4.252 5.197 6.142 7.087 8.031 8.976"                   >>cfsmensal.ctl
echo " 9.921 10.866"                                                                  >>cfsmensal.ctl
echo "zdef 1 linear 0 1"                                                              >>cfsmensal.ctl  
echo "tdef 6 linear "${datagrads[1]}" 1mo"                                                >>cfsmensal.ctl
echo "vars 7"                                                                         >>cfsmensal.ctl
echo "lead01  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "lead02  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl  
echo "lead03  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl 
echo "lead04  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "lead05  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "lead06  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "lead07  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
#echo "lead08  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
#echo "lead09  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]"                     >>cfsmensal.ctl
echo "endvars"                                                                        >>cfsmensal.ctl  
echo "'lats4d -i cfsmensal.ctl  -o "$filenc" -q'"           >script.gs
grads -lbc "script.gs"            >>$CDLOG"/cfsmensal.prn" 2>&1		
LOCAL="../../../CICC/"`date +"%Y%m%d00"`  
mkdir -p $LOCAL"/CFS/ENS"    >/dev/null 2>&1 

if test -e $filenc ;then 
mv $filenc $LOCAL"/CFS/ENS" >/dev/null 2>&1
XONSCHECK[3]=1
export XONSCHECK
else
XONSCHECK[3]=-1
export XONSCHECK
fi 
 

fi

echo "FINAL DO PROCESSO CFS: "`date +"%Y%m%d %T"`  >>$CDLOG"/cfsmensal.prn" 2>&1
echo "FINAL DO PROCESSO CFS: "`date +"%Y%m%d %T"`
tempo2=`date +"%s"`
let "tempo=tempo2-tempo1"
echo "DURACAO DO PROCESSO:"$tempo" segundos" 
echo "DURACAO DO PROCESSO:"$tempo" segundos"  >>$CDLOG"/cfsmensal.prn" 2>&1









}




get_analises_gfs()
{

#-------------------------------------------------------------------------------------
#  SIMOP 3.0 - SISTEMA DE APOIO A PREVISAO 
#
#  Sistema que adquire dados observados, rodadas de modelos, calcula medias e afins e 
#  gera figuras. 
#
#  By ReginaldoVentura de sa (regis@lamma.ufrj.br) 
#
#------------------------------------------------------------------------------------  
# 
#
#     MODULO ADQURE MODEL 
#
#-----------------------------------------------------------------------------------
data=`date +"%Y%m%d"`
if [ "$1"="" ] 
then 
rodada="00"
else
rodada=$1
fi 
DIRW="../../CICC/"$data$rodada"/ANALISES/"
mkdir ../../CICC/$data$rodada
mkdir ../../CICC/$data$rodada/ANALISES
mkdir $DIRW 
ens="01"
#
# Analise CFS
#
for ens in `seq --format=%02g 1 4`
do

	site="http://nomads.ncep.noaa.gov/pub/data/nccf/com/cfs/prod/cfs/cfs."$data"/"$rodada"/6hrly_grib_"$ens"/"
	filegrib="pgbanl."$ens"."$data$rodada".grb2"
	filenc="ANALISE_CFS_"$data$rodada"_"$ens".nc" 
	bagen ANAL $site$filegrib $filegrib $filenc 
	mv $filenc $DIRW
done
#
# Analise GFS
#

site="http://nomads.ncep.noaa.gov/pub/data/nccf/com/gfs/prod/gfs."$data$rodada"/"
filegrib0p25="gfs.t"$rodada"z.pgrb2.0p25.anl"
filegrib0p50="gfs.t"$rodada"z.pgrb2.0p50.anl"
filegrib1p00="gfs.t"$rodada"z.pgrb2.1p00.anl"
filegrib0p25b="gfs.t"$rodada"z.pgrb2b.0p25.anl"
filegrib0p50b="gfs.t"$rodada"z.pgrb2b.0p50.anl"
filegrib1p00b="gfs.t"$rodada"z.pgrb2b.1p00.anl"
filenc0p25="ANALISE_GFS_"$data$rodada"_0p25.nc" 
filenc0p50="ANALISE_GFS_"$data$rodada"_0p50.nc" 
filenc1p00="ANALISE_GFS_"$data$rodada"_1p00.nc" 
filenc0p25b="ANALISE_GFS_"$data$rodada"_0p25b.nc" 
filenc0p50b="ANALISE_GFS_"$data$rodada"_0p50b.nc" 
filenc1p00b="ANALISE_GFS_"$data$rodada"_1p00b.nc" 

bagen ANAL  $site$filegrib0p25 $filegrib0p25 $filenc0p25
bagen ANAL $site$filegrib0p50 $filegrib0p50 $filenc0p50
bagen ANAL $site$filegrib1p00 $filegrib0p00 $filenc1p00
bagen ANAL $site$filegrib0p25 $filegrib0p25 $filenc0p25
bagen ANAL $site$filegrib0p50 $filegrib0p50 $filenc0p50
bagen ANAL $site$filegrib1p00 $filegrib1p00 $filenc1p00

mv ANALISE_GFS* $DIRW 








}



get_gfs_ens()
{
#-------------------------------------------------------------------------------------
# LAMMOC/UFF  - LABORATÓRIO DE MONITORAMENTO E MODELAGEM DO SISTEMA CLIM
# AMBMET      - CONSULTORIA EM METEOROLOGIA , OCEANOGRAFIA E HIDROLOGIA 
# WXWEATHER   - CONSULTORIA EM METEOROLOGIA E COMPUTAÇÃO DE ALTO DESEMPENHO  
#     
#-------------------------------------------------------------------------------------
# ADQUIRE MODEL -  Sistema para baixar e pré-processar dados de modelos de previsão
# numerica do tempo e climáticos
#
# Baixa dados de modelos de tempo e clima para serem utilizados em diversos processos
# Modelos implementados:
# ETA40 CPTEC 
# GEFS versao ONS  (NCEP) 
# CFS 
#  
# -----------------------------------------------------------------------------------  
# 
#  Desenvolvido por: Reginaldo Ventura de Sa (regis@lamma.ufrj.br) 
#  Release 1 em :  21/11/2017     
#
#-----------------------------------------------------------------------------------
# Sintaxe:
# adquire_model.sh  <modelo> <opcoes> 
#
# modelos disponiveis Reease 1 :  eta40 ,  gfs , gefs e cfs(mensal)   
#
# eta        Pega os dados da data atual 
# eta - n    Pega os dados n dias para atrás (máximo 7 anteriores ou disponibildiade no cptec)
# eta YYYYMMDD RR HHZDDmmmYYYY    Pega na data definida (minimo 7 dias anteriores)
#                                 YYYY ano em numero  MM mes em numero DD dia em numero
#                                 mmm mes abreviado em ingles  RR Rodada (00 ou 12) 
#
#               
#
#GFS
#
# get_gfs_ens AAAAMMDD HH 
#
#  Baixa os gfs  de 1 grau, meio gru  e 0.25 
#
# LOCAL ONDE TUDO ACONTECE
#
export LANG=en_us_8859_1
#
# pega data
#
if [ $1 ="" ];then
   data=`date +"%Y%m%d"`
else
   data=$1
fi 

echo "["`date`"] INICIO DO LOG GEFS 1P00 " > $CDLOG/LOGGFSENS.prn 2>&1 
echo "INICIO DO PROCESSO GEFS: "`date +"%Y%m%d %H%M"` >>$CDLOG/LOGGFSENS.prn 2>&1
echo "INICIO DO PROCESSO GEFS: "`date +"%Y%m%d %H%M"`
tempo1=`date +"%s"`

#
# pega rodada (00z, 06Z, 12Z ou 18Z)
#
if  [ $2 ="" ];then
rodada=$ROD	
else
rodada=$2 
fi 
# echo $data
# echo $rodada


LOCAL="../../CICC/" 
LOCALFILE=$LOCAL/$data$rodada"/GEFS1P00/"
mkdir $LOCAL                                     >/dev/null 2>&1
mkdir $LOCAL/$data$rodada                        >/dev/null 2>&1
mkdir $LOCALFILE                                 >/dev/null 2>&1 


mkdir $LOCAL                   >/dev/null 2>&1
mkdir $LOCALFILE               >/dev/null 2>&1
# nome do arquivo de saida 
#
#filegrib=$tipoens"_"$rodada"_"$data"_BR.grb2"
filegrib="GEFS_"$rodada"_"$data"_BR_1P00.grb2"

file=$LOCALFILE"/"$filegrib
if test -e  $file ;then 
echo $file" Ja foi baixado." >> $CDLOG/LOGGFSENS.prn 2>&1 

XONSCHECK[2]=1

echo "---->"${XONSCHECK[2]} 
return 

else


#
# baixa os 20 membros e membro controle 
#
for ens in `seq --format=%02g 0 20`
do
#
# define o label tipoens :gec00 controle demais ensembles gep
#
if [ $ens = 00 ];then 
tipoens="gec00" 
else
tipoens="gep"$ens
fi 
#
# baixar
#
echo "Baixando :"$tipoens
#
# Baixa todos os arquivos 16 dias de previsão 
#
for n in `seq --format=%02g 0 6 384`
do
#
# manter compatibilidade
#
hora=$n
#
# nome do arquivo temporario 
#
file="tmpfilegrib.grb2"
#

#
# link do gribfilter (altere com cuidado!!!!!!!!!!!!!!!!!)
# veja no site do nomads como gerar um outro de sua preferencia
#
#
link01="http://nomads.ncep.noaa.gov/cgi-bin/filter_gens.pl?file="$tipoens".t"$rodada"z.pgrb2f"$hora"&lev_surface=on&var_APCP=on&var_PRATE=on&var_TMAX=on&var_TMIN=on&var_TMP=on&subregion=&leftlon=280&rightlon=330&toplat=10&bottomlat=-35&dir=%2Fgefs."$data"%2F00%2Fpgrb2"
#
#  baixo arquivo 
#
wget  -O $file $link01  >>$CDLOG/LOGGFSENS.prn 2>&1 
#
#  junta tudo num so arquivo 
#
wgrib2 -append -grib $filegrib $file  >>$CDLOG/LOGGFSENS.prn 2>&1  
done
done 




if test -e $filegrib ;then 
mv $filegrib $LOCALFILE   >/dev/null 2>&1 
XONSCHECK[2]=1

else
XONSCHECK[2]=-1

fi 


fi

echo "FINAL DO PROCESSO GEFS: "`date +"%Y%m%d %T"`  >>$CDLOG/LOGGFSENS.prn 2>&1  
echo "FINAL DO PROCESSO GEFS: "`date +"%Y%m%d %T"`
tempo2=`date +"%s"`
let "tempo=tempo2-tempo1"
echo "DURACAO DO PROCESSO:"$tempo" segundos" 
echo "DURACAO DO PROCESSO:"$tempo" segundos"  >>$CDLOG/LOGGFSENS.prn 2>&1 
echo ${XONSCHECK[2]} 
}






get_gfs()
{
#-------------------------------------------------------------------------------------
#  SIMOP 3.0 - SISTEMA DE APOIO A PREVISAO 
#
#  Sistema que adquire dados observados, rodadas de modelos, calcula medias e afins e 
#  gera figuras. 
#
#  By ReginaldoVentura de sa (regis@lamma.ufrj.br) 
#
#------------------------------------------------------------------------------------  
# 
#
#     MODULO ADQURE MODEL 
#
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------
#
# MODELO: GFS  (1.0 0.50 e 0.25 graus)
# TIPO DE DADO: GRIB2
# pOR rEGIS. regis@lamma.ufrj.br 
#
#---- featuretes
# ele baixa via nomads com grib filter
# só baixa chuva, temp 
# Regiao de corte: 280 230 longitude -35 10 latitude
# 
# em 31/08/2017  
# Updates
#
#--------------------------------------------------------
#
# inicio do processo
# 
#
# força o ingles
#
export LANG=en_us_8859_1
#
# inicio
#
echo "["`date`"] INICIO DO LOG GFS1P00 "  >./LOGGFS1P00.prn 2>&1 
echo "["`date`"] INICIO DO LOG GFS1P00 "  >./LOGGFS0P50.prn 2>&1 
echo "["`date`"] INICIO DO LOG GFS1P00 "  >./LOGGFS0P25.prn 2>&1 
echo "["`date`"] INICIO DO PROCESSO BAIXAR GFS - CHUVA E TEMP " 
#
# pega data
#
if [ $1 ="" ];then
   data=`date +"%Y%m%d"`
else
   data=$1
fi 
#
# pega rodada (00z, 06Z, 12Z ou 18Z)
#
if  [ $2 ="" ];then
rodada="00"
else
rodada=$2 
fi 
echo $data
echo $rodada
#
# cria os diretorios onde ficarao os dados
#
# 1P00
#
LOCAL="../../CICC/" 
LOCALFILE1=$LOCAL/$data$rodada"/GFS1P00/"
mkdir $LOCAL                                     >>./LOGGFS1P00.prn 2>&1 
mkdir $LOCAL/$data$rodada                                     >>./LOGGFS1P00.prn 2>&1 
mkdir $LOCALFILE1                                  >>./LOGGFS1P00.prn 2>&1 
#
# 0P50
#
LOCAL="../../CICC/" 
LOCALFILE2=$LOCAL/$data$rodada"/GFS0P50/"
mkdir $LOCAL                                     >>./LOGGFS0P50.prn 2>&1 
mkdir $LOCAL/$data$rodada                                     >>./LOGGFS0P50.prn 2>&1 
mkdir $LOCALFILE2                                    >>./LOGGFS0P50.prn 2>&1 
#
# 0P25
#
LOCAL="../../CICC/" 
LOCALFILE3=$LOCAL/$data$rodada"/GFS0P25/"
mkdir $LOCAL                                                  >>./LOGGFS0P25.prn 2>&1 
mkdir $LOCAL/$data$rodada                                     >>./LOGGFS0P25.prn 2>&1 
mkdir $LOCALFILE3                                             >>./LOGGFS0P25.prn 2>&1 
# nome do arquivo de saida 
#
#filegrib=$tipoens"_"$rodada"_"$data"_BR.grb2"
filegrib1="GFS_"$rodada"_"$data"_BR_1P00.grb2"
filegrib2="GFS_"$rodada"_"$data"_BR_0P50.grb2"
filegrib3="GFS_"$rodada"_"$data"_BR_0P25.grb2"
#
# Baixa todos os arquivos 16 dias de previsão 
#
for n in `seq --format=%03g 0 6 384`
do
#
# manter compatibilidade
#
hora=$n
#
# nome do arquivo temporario 
#
file1="tmpfilegrib1.grb2"
file2="tmpfilegrib2.grb2"
file3="tmpfilegrib3.grb2"
#
#
# link do gribfilter (altere com cuidado!!!!!!!!!!!!!!!!!)
# veja no site do nomads como gerar um outro de sua preferencia
#
#
####link01="http://nomads.ncep.noaa.gov/cgi-bin/filter_gens.pl?file="$tipoens".t"$rodada"z.pgrb2f"$hora"&lev_surface=on&var_APCP=on&var_PRATE=on&var_TMAX=on&var_TMIN=on&var_TMP=on&subregion=&leftlon=280&rightlon=330&toplat=10&bottomlat=-35&dir=%2Fgefs."$data"%2F00%2Fpgrb2"
link01="http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_1p00.pl?file=gfs.t"$rodada"z.pgrb2.1p00.f"$hora"&lev_surface=on&var_APCP=on&var_PRATE=on&var_RH=on&var_TMAX=on&var_TMIN=on&var_TMP=on&subregion=&leftlon=280&rightlon=330&toplat=10&bottomlat=-35&dir=%2Fgfs."$data$rodada
link02="http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p50.pl?file=gfs.t"$rodada"z.pgrb2full.0p50.f"$hora"&lev_surface=on&var_APCP=on&var_PRATE=on&var_RH=on&var_TMAX=on&var_TMIN=on&var_TMP=on&subregion=&leftlon=280&rightlon=330&toplat=10&bottomlat=-35&dir=%2Fgfs."$data$rodada
link03="http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t"$rodada"z.pgrb2.0p25.f"$hora"&lev_surface=on&var_APCP=on&var_PRATE=on&var_RH=on&var_TMAX=on&var_TMIN=on&var_TMP=on&subregion=&leftlon=280&rightlon=330&toplat=10&bottomlat=-35&dir=%2Fgfs."$data$rodada
#
#  baixo arquivo 1P00
#
file=$LOCALFILE1/$filegrib1
if [ -e  $file ];then 
echo "GFS 1P00 JA BAIXADO:"$n
else
wget  -O $file1 $link01  >>./LOGGFS1P00.prn 2>&1 
#
#  junta tudo num so arquivo 
#
wgrib2 -append -grib $filegrib1 $file1  >>./LOGGFS1P00.prn 2>&1 
fi
#  baixo arquivo 0P50
file=$LOCALFILE2/$filegrib2
if [ -e  $file ];then 
echo "GFS 0P50 JA BAIXADO"
else
wget  -O $file2 $link02  >>./LOGGFS0P50.prn 2>&1 
#
#  junta tudo num so arquivo 
#
wgrib2 -append -grib $filegrib2 $file2  >>./LOGGFS0P50.prn 2>&1 
fi
#  baixo arquivo 0P25
file=$LOCALFILE3/$filegrib3
if [ -e  $file ];then 
echo "GFS 0P25 JA BAIXADO"
else
wget  -O $file3 $link03  >>./LOGGFS0P25.prn 2>&1 
#  junta tudo num so arquivo 
wgrib2 -append -grib $filegrib3 $file3  >>./LOGGFS0P25.prn 2>&1 
fi
done
#
# copia arquivos para diretorios finais
#
mv $filegrib1 $LOCALFILE1 >>./LOGGFS1P00.prn 2>&1
mv $filegrib2 $LOCALFILE2 >>./LOGGFS0P50.prn 2>&1
mv $filegrib3 $LOCALFILE3 >>./LOGGFS0P25.prn 2>&1
}


get_eta_40 ()
{
#-------------------------------------------------------------------------------------
# LAMMOC/UFF  - LABORATÓRIO DE MONITORAMENTO E MODELAGEM DO SISTEMA CLIM
# AMBMET      - CONSULTORIA EM METEOROLOGIA , OCEANOGRAFIA E HIDROLOGIA 
# WXWEATHER   - CONSULTORIA EM METEOROLOGIA E COMPUTAÇÃO DE ALTO DESEMPENHO  
#     
#-------------------------------------------------------------------------------------
# ADQUIRE MODEL -  Sistema para baixar e pré-processar dados de modelos de previsão
# numerica do tempo e climáticos
#
# Baixa dados de modelos de tempo e clima para serem utilizados em diversos processos
# Modelos implementados:
# ETA40 CPTEC 
# GEFS versao ONS  (NCEP) 
# CFS 
#  
# -----------------------------------------------------------------------------------  
# 
#  Desenvolvido por: Reginaldo Ventura de Sa (regis@lamma.ufrj.br) 
#  Release 1 em :  21/11/2017     
#
#-----------------------------------------------------------------------------------
# Sintaxe:
# adquire_model.sh  <modelo> <opcoes> 
#
# modelos disponiveis Reease 1 :  eta40 ,  gfs , gefs e cfs(mensal)   
#
# eta        Pega os dados da data atual 
# eta - n    Pega os dados n dias para atrás (máximo 7 anteriores ou disponibildiade no cptec)
# eta YYYYMMDD RR HHZDDmmmYYYY    Pega na data definida (minimo 7 dias anteriores)
#                                 YYYY ano em numero  MM mes em numero DD dia em numero
#                                 mmm mes abreviado em ingles  RR Rodada (00 ou 12) 
#-----------------------------------------------------------------------------------------------
# CHANGE LOG
# 
#-----------------------------------------------------------------------------------------------
#
# força a saida em ingles
#
export LANG=en_us_8859_1
#
#  INICIO
#
echo "["`date`"] INICIO DO LOG ETA40KM "  >$CDLOG/LOGETA40.prn 2>&1
echo "INICIO DO PROCESSO ETA40: "`date +"%Y%m%d %H%M"` >>$CDLOG/LOGETA40.prn 2>&1
echo "INICIO DO PROCESSO ETA40: "`date +"%Y%m%d %H%M"`
tempo1=`date +"%s"`
#
# Se opcao for vazia pega a data do momento
# se não tiver vazia opçoes $1 e $2 sao argumentos
# se $1 = - então $2 dias para trás que sera pego os dados 
if [ -z $1  ] 
then
data=`date +"%Y%m%d"`
####datagrads=`date +"%d%b%Y" -d "1 days"`
datagrads=`date +"%d%b%Y"`
rodada=$ROD
else
let b="$1"
data=`date +"%Y%m%d" -d "$1 days ago"`
datagrads=`date +"%d%b%Y" -d "$b  days ago"` 
rodada=$ROD
fi
#
# para rodar n dias atrás  
#   caso $1="-"
# #
# if [ "$1" == "-" ]
# then
# let b="$2"
# data=`date +"%Y%m%d" -d "$1 days ago"`
# datagrads=`date +"%d%b%Y" -d "$b  days ago"` 
# rodada=$ROD
# fi
#
# define o local onde sera feito as coisas
#
LOCAL="../../CICC/" 
LOCALFILE=$LOCAL"/"$data$rodada"/ETA40/"
file=$LOCALFILE"/"$data$rodada".bin"
mkdir $LOCAL                           >/dev/null 2>&1
mkdir $LOCAL$data$rodada               >/dev/null 2>&1
mkdir $LOCALFILE                       >/dev/null 2>&1
echo $data      
echo $datagrads 

fileeta=( "pp"$data"_0036.bin" "pp"$data"_0060.bin" "pp"$data"_0084.bin" "pp"$data"_0108.bin" "pp"$data"_0132.bin" 
          "pp"$data"_0156.bin" "pp"$data"_0180.bin" "pp"$data"_0204.bin" "pp"$data"_0228.bin" "pp"$data"_0252.bin"  )

#
#  arquivo de saida
#
filebin=$data$rodada".bin" 
if [ ! -e $file ];then 
#
# Adquire os dados no site do CPTEC. 
# Atençao:  
# Verifique pois o CPTEC altera os caminhos sem avisar!!!
#
echo "["`date`"] BAIXANDO DADOS ETA 40KM " 
echo "["`date`"] BAIXANDO DADOS ETA 40KM "  >>$CDLOG/LOGETA40.prn 2>&1

for k in `seq 0 9`
do
wget -nc --random-wait  ftp://ftp1.cptec.inpe.br:21/modelos/io/tempo/regional/Eta40km_ENS/prec24/$data$rodada/${fileeta[$k]}   >>$CDLOG/LOGETA40.prn 2>&1
done 
#
# existem 10 arquivos .bin
# separados fica dificil de trabalhar com os arquivos
# por isso vou juntar todos os .bin num único do arquivo
#
echo "["`date`"] CRIANDO ARQUIVOS PARA O GRADS" 
rm $data$rodada".bin" >>$CDLOG/LOGETA40.prn 2>&1
#rm *.ctl            >>$CDLOG/LOGETA40.prn 2>&1
#
#
#
for file in `ls -1 *.bin`
do
cat $file >>  $filebin     
rm $file                            
done


if test -e $filebin ;then 
mv $filebin $LOCALFILE
echo "ETA40KM BAIXADO COM SUCESSO"

else
echo "ETA40KM NAO FOI BAIXADO. VERIFIQUE FONTE DE DADOS OU CONEXAO INTERNET" 
fi 

 
else
echo "ETA 40 KM JA BAIXADO"
echo "ETA 40 KM JA BAIXADO"  >>$CDLOG/LOGETA40.prn 2>&1
fi



echo "FINAL DO PROCESSO ETA40: "`date +"%Y%m%d %T"`  >>$CDLOG/LOGETA40.prn 2>&1
echo "FINAL DO PROCESSO ETA40: "`date +"%Y%m%d %T"`
tempo2=`date +"%s"`
let "tempo=tempo2-tempo1"
echo "DURACAO DO PROCESSO:"$tempo" segundos" 
echo "DURACAO DO PROCESSO:"$tempo" segundos"  >>$CDLOG/LOGETA40.prn 2>&1
echo ${XONSCHECK[1]} 
}











#-------------------------------------------------------------------------------------
# LAMMOC/UFF  - LABORATÓRIO DE MONITORAMENTO E MODELAGEM DO SISTEMA CLIM
# AMBMET      - CONSULTORIA EM METEOROLOGIA , OCEANOGRAFIA E HIDROLOGIA 
# WXWEATHER   - CONSULTORIA EM METEOROLOGIA E COMPUTAÇÃO DE ALTO DESEMPENHO  
#     
#-------------------------------------------------------------------------------------
# ADQUIRE MODEL -  Sistema para baixar e pré-processar dados de modelos de previsão
# numerica do tempo e climáticos
#
# Baixa dados de modelos de tempo e clima para serem utilizados em diversos processos
# Modelos implementados:
# ETA40 CPTEC 
# GEFS versao ONS  (NCEP) 
# CFS 
#  
# -----------------------------------------------------------------------------------  
# 
#  Desenvolvido por: Reginaldo Ventura de Sa (regis@lamma.ufrj.br) 
#  Release 1 em :  21/11/2017     
#
#-----------------------------------------------------------------------------------
# Sintaxe:
# adquire_model.sh  <modelo> <opcoes> 
#
# modelos disponiveis Reease 1 :  eta40 ,  gfs , gefs e cfs(mensal)   
#
# eta        Pega os dados da data atual 
# eta - n    Pega os dados n dias para atrás (máximo 7 anteriores ou disponibildiade no cptec)
# eta YYYYMMDD RR HHZDDmmmYYYY    Pega na data definida (minimo 7 dias anteriores)
#                                 YYYY ano em numero  MM mes em numero DD dia em numero
#                                 mmm mes abreviado em ingles  RR Rodada (00 ou 12) 
#
#               
#
#GFS
#
# get_gfs_ens AAAAMMDD HH 
#
#  Baixa os gfs  de 1 grau, meio gru  e 0.25 
#
# LOCAL ONDE TUDO ACONTECE
#
export LANG=en_us_8859_1
mkdir ../../WORKDISK        >/dev/null  2>&1      
cd ../../WORKDISK           >/dev/null  2>&1
export CDLOG="../../LOG/"`date +"%Y%m%d"`    
mkdir $CDLOG                >/dev/null  2>&1
#
# copia os arquivos de manipulação do grib
#
cp ../MODULOS/COMMON_STUFF/g2ctl.pl . 

#
#  INICIO
#
echo "MODELO SENDO UTILIZADO:"$1


#
# exeuta o modelo escolhido. caso nenhuma opção 
#  seja informada, executa lista de execução
#
case "$1" in 
           eta40)
           get_eta_40 $2 $3 $4 		  
           ;; 
           gefs)
           get_gfs_ens $2 $3 $4 		  
           ;; 
		   
           gfs)
           get_gfs $2 $3 $4 		  
           ;;
		   
           cfs)
		   cfs_mensal2 $2
		   ;;
		   
		   cfsvars)
		   get_cfs_vars $2 $3 $4 
		   ;;
		   gfsanal)
		   get_analises_gfs $2 $3 $4 
		   ;;
		   gfs)
		   get_gfs $2 $3 $4 
		   ;;
		   *)
		  
	   get_eta_40   $2 $3 $4 	  
       get_gfs_ens $2 $3 $4    
       cfs_mensal2 $2
       gfs
       cfsars	   
		   
esac 

#
# FIM 
#
 
 echo " F I M    D  A      O P E R A Ç A  O"