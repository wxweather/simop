#!/bin/bash

dia="$(date +'%d')"
mes="$(date +'%m')"
ano="$(date +'%Y')"
hora="$(date +'%X')"
arq="log-geraflu_"$ano$mes$dia"_"$hora".txt"

function GERAFLU () {

echo " "
echo "GERAFLU"
echo "Execucao iniciada em: "$dia"."$mes"."$ano as $hora
echo " "


# Executa os scripts individuais
./geraflu_PBUENOS.sh $1
./geraflu_PARAGUACU.sh $1 
./geraflu_CAPESCURO.sh $1
./geraflu_PASSAGEM.sh $1
./geraflu_BALSA.sh $1
./geraflu_FLOR_ESTRA.sh $1
./geraflu_IVINHEMA.sh $1
./geraflu_PTAQUARA.sh $1

# Inserir aqui outros postos flu

estecaminho=$(pwd)
echo "Arquivo de log salvo em: "
echo $estecaminho"/LOG/"$arq

}

GERAFLU $1 | tee 'LOG/'$arq



