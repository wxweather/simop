#!/bin/bash

bacia="Itaipu" # mudar
postoflu="IVINHEMA" # mudar
file=$postoflu".txt"

echo "------------------------------------"
echo "Posto flu: "$postoflu
echo "Verificando data da ultima rodada..."

if test -z $ROD ;then 
ROD='00'
fi 


### 1) Identifica o ultimo dia com dados de entrada
### (ou seja, procura diretorio mais recente com arquivo do posto flu)
if test -z $1 ;then 
i=0
else
i=$1
fi 

primeirodia=-1
while [  $primeirodia = -1 ]; do

    # Pega data Hoje -i dias
    dia="$(date --date="$i day ago" +'%d')"
    mes="$(date --date="$i day ago" +'%m')"
    ano="$(date --date="$i day ago" +'%Y')"

    # Verifica se diretorio hoje-i existe
    if [ -d "../../../OUTPUT/$ano$mes$dia$ROD/" ]; then
        primeirodia=$i
            
        # Verifica se diretorio hoje-i contem posto flu
        if [ -f "../../../OUTPUT/$ano$mes$dia$ROD/SMAPONS/$bacia/ARQ_ENTRADA/$file" ]; then
            echo "Ultima rodada realizada ha $i dia(s) em $dia.$mes.$ano."
            echo " "
            primeirodia=$i
        else
            primeirodia=-1
            i=$((i+1))
        fi
    
    else
        i=$((i+1))
    fi  
    
done

## 2) Copia arquivo de vazao obs do posto flu

# Fazer para todos os dias até chegar em ontem
# Definir aqui a data do arquivo a ser gerado: quantos dias atras?
# Ex1: daysago=0 para gerar arquivo na pasta do dia de HOJE
# Ex2: daysago=1 para gerar arquivo na pasta do dia de HOJE-1 (ONTEM)
if test -z $1 ;then 
daysago=0
else
daysago=$1
fi 
while [  $primeirodia -gt $daysago ]; do

echo "Calculo para "$((primeirodia-1))" dia(s) atras:"

# Ultimo dia com arquivo de entrada do posto flu
ultdia="$(date --date="$primeirodia day ago" +'%d')"
ultmes="$(date --date="$primeirodia day ago" +'%m')"
ultano="$(date --date="$primeirodia day ago" +'%Y')"

# Dia seguinte 
proxdia="$(date --date="$((primeirodia-1)) day ago" +'%d')"
proxmes="$(date --date="$((primeirodia-1)) day ago" +'%m')"
proxano="$(date --date="$((primeirodia-1)) day ago" +'%Y')"

echo "Verificando diretorio /$proxano$proxmes$proxdia$ROD/..."

# Verifica se existe diretorio do dia seguinte (porque vamos precisar dos dados das usinas deste dia para gerar o posto flu)
if [ -d "../../../OUTPUT/$proxano$proxmes$proxdia$ROD/" ]; then
    # Copia ultimo arquivo do posto flu para pasta do dia seguinte
    cp "../../../OUTPUT/$ultano$ultmes$ultdia$ROD/SMAPONS/$bacia/ARQ_ENTRADA/$file" "../../../OUTPUT/$proxano$proxmes$proxdia$ROD/SMAPONS/$bacia/ARQ_ENTRADA/"
else
    # Caso a pasta do dia seguinte nao exista:
    echo "ERRO: Diretorio /$proxano$proxmes$proxdia$ROD/ nao existe."
    echo "Favor organizar dados observados em: XONS/OUTPUT/$proxano$proxmes$proxdia$ROD/ antes de rodar este script."
    echo " "
    exit
fi

#################### Calculo da regressao ####################

cd "../../../OUTPUT/$proxano$proxmes$proxdia$ROD/SMAPONS/$bacia/ARQ_ENTRADA/"

# Prepara arquivos
mv $file "IVINHEMAM1.txt" # mudar
x1="IVINHEMAM1.txt" # mudar
x2="ITAIPU.txt" # mudar
#x3="FUNILMG.txt" # mudar
#x4="CAMARGOS.txt" # mudar

# O espaco no nome do arquivo atrapalha (depois vamos devolve-lo)
#mv "FUNIL MG.txt" "FUNILMG.txt" # mudar

# dos2unix (ajeitar o formato para tirar o \r do final -- Ver com o Regis)
dos2unix $x1; dos2unix $x2;

# Cria arquivo auxiliar com as primeiras $primeirodia linhas removidas e novas linhas no final
cp $x1 "IVINHEMAM1_MantemPeriodo.txt" # mudar
sed -i '1d' "IVINHEMAM1_MantemPeriodo.txt" # mudar
x5="IVINHEMAM1_MantemPeriodo.txt" # mudar
#count=0
#while [ $count -lt $((primeirodia-daysago)) ]; do
    echo " " >> "IVINHEMAM1_MantemPeriodo.txt" # mudar
#    count=$((count+1))
#done

# Coeficientes deste posto flu 
coefsfile="../../../../../SCRIPTS/MODULOS/POSTOSFLU/"$postoflu".coefs"

# Le arquivos liha por linha
# Mudar aqui de acordo com o numero de variaveis explicativas
linha=0
while read linex1<&3 && read linex2<&4 && read linex5<&7; do
   
   	mescoef="$(date --date="$primeirodia day ago" +'%m')"
	b=`sed -n "1 p" $coefsfile | cut -d ',' -f $((mescoef+0))`
	a1=`sed -n "2 p" $coefsfile | cut -d ',' -f $((mescoef+0))`
	a2=`sed -n "3 p" $coefsfile | cut -d ',' -f $((mescoef+0))`
#	a3=`sed -n "4 p" $coefsfile | cut -d ',' -f $((mescoef+0))`
#	a4=`sed -n "5 p" $coefsfile | cut -d ',' -f $((mescoef+0))`
    
    # Variaveis explicativas (vazoes observadas das usinas)
    qx1=`cut -d '|' -f6- <<<$linex1`
    qx2=`cut -d '|' -f6- <<<$linex2`
#   qx3=`cut -d '|' -f6- <<<$linex3`
#   qx4=`cut -d '|' -f6- <<<$linex4`
    
    # Vazao de ontem na mesma linha para copiar
    qx5=`cut -d '|' -f6- <<<$linex5`
        
    # Vazao calculada do posto flu
    if [ $linha -gt 48 ]; then	
        qy[`echo $linha`]=`echo "scale=2; ($a1*$qx1)+($a2*$qx2)+$b" | bc`
        #echo '('$a1'*'$qx1')+('$a2'*'$qx2')+('$a3'*'$qx3')+('$a4'*'$qx4')+'$b
        # echo "LINHA CALCULADA "$linha
    else
        qy[`echo $linha`]=$qx5
        #  echo "LINHA COPIADA "$linha
    fi
                
    linha=$((linha+1))   
         
done 3<$x1 4<$x2 7<$x5

# Pega os campos de 1 a 5 de arquivo recente
cut -d '|' -f 1,2,3,4,5 ITAIPU.txt > NOVO.txt # mudar

# Adiciona a vazao calculada 
for f in ${qy[*]}; do read x; echo "$x|${f%%??????????????}" >> $file ; done < NOVO.txt
rm NOVO.txt
#rm PBUENOSM1.txt

# Substitui o VNS (de ITAIPU) por VMD (do posto flu) e codigo
# Atualizar os codigos na mao
sed -i 's/VNS/VMD/g' $file # mudar
sed -i 's/64918980/64617000/g' $file # mudar

echo " "
echo "Arquivo $file gerado em /$proxano$proxmes$proxdia$ROD/ com sucesso."
echo " "

# Devolvendo o espaco
#mv "FUNILMG.txt" "FUNIL MG.txt" # mudar
rm "IVINHEMAM1_MantemPeriodo.txt" # mudar
rm "IVINHEMAM1.txt" # mudar

primeirodia=$((primeirodia-1))

cd ../../../../../SCRIPTS/MODULOS/POSTOSFLU/

done

    







