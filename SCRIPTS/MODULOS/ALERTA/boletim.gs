*'sdfopen http://nomads.ncep.noaa.gov:9090/dods/gfs_0p25_1hr/gfs20170330/gfs_0p25_1hr_18z'


_status=0
while (_status =0)
pi=getfile(1)
xlon=subwrd(_args,1) 
xlat=subwrd(_args,2)
codigo=subwrd(_args,3)
cidade=subwrd(_args,4)
'sdfopen http://nomads.ncep.noaa.gov:9090/dods/gfs_0p25_1hr/gfs20170328/gfs_0p25_1hr_18z'
li=boletim("saida.prn",49,xlon,xlat,codigo,cidade) 
'close 1'
endwhile

'quit'






function boletim( arqsaida, tempof, xlon, xlat ,codigo , cidade  )


lon360=xlon+360
'set lon 'lon360
'set lat 'xlat

t=7
while (t<=tempof)
'set t ' t
'q time'
var=subwrd(result,3)
horax=substr(var,1,2)
hora=substr(var,1,2)':00'
dia=substr(var,4,2)
mes=substr(var,6,3)
ano=substr(var,9,4)

*
*  temp max
*
*'d ave(tmpprs-273.16,t='t',t='t+23')'
*var=sublin(result,2)
*tmed=subwrd(var,4)
*
*  temp max
*
'd max(tmpprs-273.16,t='t',t='t+23')'
var=sublin(result,2)
tmax=subwrd(var,4)
*
* temp min
*
'd min(tmpprs-273.16,t='t',t='t+23')'
var=sublin(result,2)
tmin=subwrd(var,4)
*
*  temp max 2m
*
*'d ave(tmp2m-273.16,t='t',t='t+23')'
*var=sublin(result,2)
*tmed2m=subwrd(var,4)
*
*  temp max 2m
*
*'d max(tmp2m-273.16,t='t',t='t+23')'
*var=sublin(result,2)
*tmax2m=subwrd(var,4)
*
* temp min 2m
*
*'d min(tmp2m-273.16,t='t',t='t+23')'
*var=sublin(result,2)
*tmin2m=subwrd(var,4)
*
* ur max
*
'd max(rhprs,t='t',t='t+23')'
var=sublin(result,2)
urmax=subwrd(var,4)
*
* ur min 
*
'd min(rhprs,t='t',t='t+23')'
var=sublin(result,2)
urmin=subwrd(var,4)
*
* tbi
*
tbi=5*(urmin/100)-0.1*(tmax-27)

if (tbi >= 4.26) ; RI="BAIXO_RISCO" ; endif 
if (tbi <  3.26) ; RI="ALTO_RISCO" ; endif 
if (tbi < 4.26 & tbi >= 3.26 ) ; RI="MEDIO_RISCO" ; endif 

*
* vento 
*
'define umedio=ave(ugrdprs,t='t',t='t+23')'
'define vmedio=ave(vgrdprs,t='t',t='t+23')'
'define wdir = 57.3*atan2(umedio,vmedio) + 180'
'define vel=mag(umedio,vmedio)' 
'd wdir'
dir=subwrd(result,4)
'd vel'
vel=subwrd(result,4) 
if (dir <11.25 | dir >=348.75) ; xdir="N"  ; endif 
if (dir >=11.25 & dir <33.75) ; xdir="NNE" ; endif 
if (dir >=33.75 & dir <56.25) ; xdir="NE" ; endif 
if (dir >=56.25 & dir <78.75) ; xdir="ENE" ; endif 
if (dir >=78.75 & dir <101.25) ; xdir="E" ; endif 
if (dir >=101.25 & dir <123.75) ; xdir="ESE" ; endif 
if (dir >=123.75 & dir <146.25) ; xdir="SE" ; endif 
if (dir >=146.25 & dir <168.75) ; xdir="S" ; endif 
if (dir >=168.75 & dir <191.25) ; xdir="SSW" ; endif 
if (dir >=191.25 & dir <213.75) ; xdir="SW" ; endif 
if (dir >=213.75 & dir <236.25) ; xdir="WSW" ; endif 
if (dir >=236.25 & dir <258.75) ; xdir="WSW" ; endif 
if (dir >=258.75 & dir <281.25) ; xdir="W" ; endif 
if (dir >=281.25 & dir <303.75) ; xdir="WNW" ; endif 
if (dir >=303.75 & dir <326.25) ; xdir="NW" ; endif 
if (dir >=326.25 & dir <348.75) ; xdir="NNW" ; endif 

*
* nebulosidade
*
*'d min(tcdcclm,t='t',t='t+23')'
*var=sublin(result,2)
*nuvemin=subwrd(var,4)
*'d max(tcdcclm,t='t',t='t+23')'
*var=sublin(result,2)
*nuvemax=subwrd(var,4)
'd ave(tcdcclm,t='t',t='t+23')'
var=sublin(result,2)
nuvemed=subwrd(var,4)
nuvem=nuvemed/100

q=write(arqsaida,cidade' 'codigo' 'ano' 'mes' 'dia' 'hora' 'tmin' 'tmax' 'urmin' 'urmax' 'RI' 'vel' 'xdir' 'nuvem,append)

t=t+24

endwhile
say "terminando cidade:"cidade


return

function getfile()
_id=read("CIDADES.DAT")
_status=sublin(_id,1)   
_args=sublin(_id,2)

if (_status < 0 )
'quit'
endif 
return 
