#!/bin/bash


export LANG=en_us_8859_1

cd ../../WORKDISK            >>./LOG.prn 2>&1 


#
# criar ctl 
#
if [ "$1" = "" ];then 
datadehoje=`date +"%Y%m%d"`
grads_data=`date +"00Z%d%b%Y"`
else
datadehoje=$1
grads_data=$2
fi



LOCAL="../../DADOS_MODELOS/GFS/OPENDAP/"
LOCAL1P00=$LOCAL"/"$datadehoje"/1P00"
LOCAL0P50=$LOCAL"/"$datadehoje"/0P50"
LOCAL0P25=$LOCAL"/"$datadehoje"/0P25"
LOCAL0P25H=$LOCAL"/"$datadehoje"/0P25H"
LOCALGENS=$LOCAL"/"$datadehoje"/GENS"

#
# baixa os dados de hoje
#
file1p00=$datadehoje"_1P00.bin"
file0p50=$datadehoje"_0P50.bin"
file0p25=$datadehoje"_0P25.bin"
file0p25h=$datadehoje"_0P25H.bin"
filegens=$datadehoje"_GENS.bin"


rfile1p00=$LOCAL1P00"/"$datadehoje"_1P00.bin"
rfile0p50=$LOCAL0P50"/"$datadehoje"_0P50.bin"
rfile0p25=$LOCAL0P25"/"$datadehoje"_0P25.bin"
rfile0p25h=$LOCAL0P25H"/"$datadehoje"_0P25H.bin"
rfilegens=$LOCALGENS"/"$datadehoje"_GENS.bin"



















#
# data de hoje
#


# echo "dset "$rfile0p25 > gfs_0p25.ctl
# echo "title GFS 0.25 deg starting from 00Z08jul2015, downloaded Jul 08 04:44 UTC" >>gfs_0p25.ctl
# echo "undef 9.999e+20" >>gfs_0p25.ctl
# echo "xdef 201 linear -80 0.25" >>gfs_0p25.ctl
# echo "ydef 201 linear -40 0.25" >>gfs_0p25.ctl
# echo "zdef 1 levels 1000">>gfs_0p25.ctl
# echo "tdef 10 linear "$grads_data" 1dy" >>gfs_0p25.ctl 
# echo "vars 1">>gfs_0p25.ctl
# echo "chuva  0  t,y,x  ** chuva mm">>gfs_0p25.ctl
# echo "endvars">>gfs_0p25.ctl

# echo "dset "$rfile0p25h > gfs_0p25h.ctl
# echo "title GFS 0.25 deg starting from 00Z08jul2015, downloaded Jul 08 04:44 UTC" >>gfs_0p25h.ctl
# echo "undef 9.999e+20" >>gfs_0p25h.ctl
# echo "xdef 201 linear -80 0.25" >>gfs_0p25h.ctl
# echo "ydef 201 linear -40 0.25" >>gfs_0p25h.ctl
# echo "zdef 1 levels 1000">>gfs_0p25h.ctl
# echo "tdef 6 linear "$grads_data" 1dy" >>gfs_0p25h.ctl 
# echo "vars 1">>gfs_0p25h.ctl
# echo "chuva  0  t,y,x  ** chuva mm">>gfs_0p25h.ctl
# echo "endvars">>gfs_0p25h.ctl


# echo "dset "$rfile0p50 > gfs_0p50.ctl
# echo "title GFS 0.25 deg starting from 00Z08jul2015, downloaded Jul 08 04:44 UTC" >>gfs_0p50.ctl
# echo "undef 9.999e+20" >>gfs_0p50.ctl
# echo "xdef 101 linear -80 0.50" >>gfs_0p50.ctl
# echo "ydef 101 linear -40 0.50" >>gfs_0p50.ctl
# echo "zdef 1 levels 1000">>gfs_0p50.ctl
# echo "tdef 10 linear "$grads_data" 1dy" >>gfs_0p50.ctl 
# echo "vars 1">>gfs_0p50.ctl
# echo "chuva  0  t,y,x  ** chuva mm">>gfs_0p50.ctl
# echo "endvars">>gfs_0p50.ctl

# echo "dset "$rfile1p00 > gfs_1P0.ctl
# echo "title GFS 1.0 deg starting from 00Z08jul2015, downloaded Jul 08 04:44 UTC" >>gfs_1P0.ctl
# echo "undef 9.999e+20" >>gfs_1P0.ctl
# echo "xdef 51 linear -80 1.00" >>gfs_1P0.ctl
# echo "ydef 51 linear -40 1.00" >>gfs_1P0.ctl
# echo "zdef 1 levels 1000">>gfs_1P0.ctl
# echo "tdef 16 linear "$grads_data" 1dy" >>gfs_1P0.ctl 
# echo "vars 1">>gfs_1P0.ctl
# echo "chuva  0  t,y,x  ** chuva mm">>gfs_1P0.ctl
# echo "endvars">>gfs_1P0.ctl


# echo "dset "$rfilegens > gfsens.ctl
# echo "title GFS 0.25 deg starting from 00Z08jul2015, downloaded Jul 08 04:44 UTC" >>gfsens.ctl
# echo "undef 9.999e+20" >>gfsens.ctl
# echo "xdef 51 linear -80 1" >>gfsens.ctl
# echo "ydef 51 linear -40 1" >>gfsens.ctl
# echo "zdef 1 levels 1000">>gfsens.ctl
# echo "tdef 65 linear "$grads_data" 6hr" >>gfsens.ctl 
# echo "vars 21">>gfsens.ctl
# echo "gec00 0  t,y,x  ** chuva mm"$grads_data >>gfsens.ctl
# echo "gep01 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep02 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep03 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep04 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep05 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep06 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep07 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep08 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep09 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep10 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep11 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep12 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep13 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep14 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep15 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep16 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep17 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep18 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep19 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "gep20 0  t,y,x  ** chuva mm "$grads_data >>gfsens.ctl
# echo "endvars">>gfsens.ctl




# echo "'open gfs_1P0.ctl'" > script1p0.gs 
# echo "pi=calcbacia(chuva,1,16,SAE_GFS_1P0_)" >>script1p0.gs
# cat ../MODULOS/CALCBACIA/calcbacia.gs >>script1p0.gs     
# grads -lbc "script1p0.gs"                               >>./LOG.prn 2>&1   

# echo "'open gfs_0P50.ctl'" > script0p50.gs 
# echo "pi=calcbacia(chuva,1,10,SAE_GFS_0P50_)" >>script0p50.gs   >>./LOG.prn 2>&1 
# cat ../MODULOS/CALCBACIA/calcbacia.gs >>script0p50.gs    
# grads -lbc "script0p50.gs"   >>./LOG.prn 2>&1 

# echo "'open gfs_0P25.ctl'" > script0p25.gs 
# echo "pi=calcbacia(chuva,1,10,SAE_GFS_0P25_)" >>script0p25.gs
# cat ../MODULOS/CALCBACIA/calcbacia.gs >>script0p25.gs    
# grads -lbc "script0p25.gs"   >>./LOG.prn 2>&1 

# echo "'open gfs_0P25h.ctl'" > script0p25h.gs 
# echo "pi=calcbacia(chuva,1,6,SAE_GFS_0P25h_)" >>script0p25h.gs
# cat ../MODULOS/CALCBACIA/calcbacia.gs >>script0p25h.gs 
# grads -lbc "script0p25h.gs"  >>./LOG.prn 2>&1 

# # echo "'open gfsens.ctl'" > scriptgens.gs 
# # echo "pi=calcbacia(chuva,1,6,SAE_GFS_GENS_)" >>scriptgens.gs
# # cat ../MODULOS/CALCBACIA/calcbacia.gs >>scriptgens.gs 
# # grads -lbc "scriptgens.gs"  


# #
# # copia dados para diretorio final 
# #
# mkdir  ../../SAIDAS/HICON/$datadehoje/                         >>./LOG.prn 2>&1 
# mkdir  ../../SAIDAS/HICON/$datadehoje/CHUVA_MEDIA              >>./LOG.prn 2>&1  
# mv SAE_GFS*  ../../SAIDAS/HICON/$datadehoje/CHUVA_MEDIA        >>./LOG.prn 2>&1 


# # mkdir ../SAIDAS
# # cd ../SAIDAS
# # mkdir $datadehoje
# # cd $datadehoje
# # mkdir chuva_media
# # cd chuva_media
# # cp ../../../WORKDISK/*.prn .
# # cd ../../
# # cd ATUAL
# # mv ../../WORKDISK/*.prn .








