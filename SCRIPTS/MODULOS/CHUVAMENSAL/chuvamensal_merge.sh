#/bin/sh 

cd ../../WORKDISK 


export LANG=en_us_8859_1
mes=(xxx jan feb mar apr may jun jul aug sep oct nov dec)
dias=(99 31 28  31  30  31  30  31  31  30  31  30   31)
nummes=(99 01 02 03 04 05 06 07 08 09 10 11 12) 






for nmes in `seq 19  -1  0`
do


m=`date +"%-m" -d "$nmes months ago"`
mesx=`date +"%b" -d "$nmes months ago"` 	
grads=`date +"%b%Y" -d  "$nmes months ago"`	
grads2=`date +"%b%Y"`	

t1="01"$grads
t2=${dias[$m]}$grads

arqnc="../../OBS/CHUVA_MENSAL/CHUVA_MERGE_22km_MENSAL_"`date +"%m%Y" -d "$nmes months ago"`".nc" 
atual="../../OBS/CHUVA_MENSAL/CHUVA_MERGE_22km_MENSAL_"`date +"%m%Y"`".nc" 
rm $atual 


echo $arqnc
if test ! -e  $arqnc ;then 
echo $t1" " $t2
echo "dset ../../OBS/MERGE_0P25/CHUVAMERGE_0P22_%y4%m2%d2.nc" > chuvamerge.ctl
echo "options  little_endian template"                          >>chuvamerge.ctl
echo "title global daily analysis "                             >>chuvamerge.ctl
echo "undef -999.0 "                                            >>chuvamerge.ctl 
echo "dtype netcdf "                                            >>chuvamerge.ctl 
echo "xdef 251 linear    -80 0.2000"                           >>chuvamerge.ctl
echo "ydef 226 linear -35  0.2000 "                          >>chuvamerge.ctl
echo "zdef 1    linear 1 1 "                                  >>chuvamerge.ctl
echo "tdef "${dias[$m]}"  linear 01"$grads" 1dy"              >>chuvamerge.ctl  
echo "vars 1"                                                 >>chuvamerge.ctl
echo "rain=>rain  0 t,y,x  chuva acumulada 24 horas"                >>chuvamerge.ctl
echo "ENDVARS"   >>chuvamerge.ctl




echo "'open chuvamerge.ctl'" >script.gs
echo "'lats4d -o "$arqnc" -vars rain   -func (@)*"${dias[$m]}" -mean -time "$t1" "$t2"'"  >>script.gs
echo "'quit'" >>script.gs

 
grads -lbc "script.gs"

else
       echo $arqnc" Ja existe"
fi        


done








