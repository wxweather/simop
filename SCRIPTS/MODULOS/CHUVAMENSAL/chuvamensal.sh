#/bin/sh 


chuvamensalcpc()
{
LOCAL="../../OBS/CHUVA_MENSAL/"

mes=(xxx jan feb mar apr may jun jul aug sep oct nov dec)
dias=(99 31 28  31  30  31  30  31  31  30  31  30   31)
nummes=(99 01 02 03 04 05 06 07 08 09 10 11 12) 


rm $LOCAL"/CHUVA_CPC_54km_MENSAL_"`date +"%m%Y"`".nc" 



for nmes in `seq 15  -1  0`
do


m=`date +"%-m" -d "$nmes months ago"`
mesx=`date +"%b" -d "$nmes months ago"` 	
grads=`date +"%b%Y" -d  "$nmes months ago"`	
t1="01"$grads
t2=${dias[$m]}$grads

# echo $m
# echo $mesx
# echo $grads
# echo $t1
# echo $t2



arqnc=$LOCAL"CHUVA_CPC_54km_MENSAL_"`date +"%m%Y" -d "$nmes months ago"`".nc" 
#atual=$LOCAL"/CHUVA_CPC_54km_MENSAL_"`date +"%m%Y"`".nc" 
#rm $atual 


echo $arqnc 
ls $arqnc 
if test -e  $arqnc
then 

       echo $arqnc" Ja existe"
else

echo $t1" " $t2
echo $arqnc 

echo "dset ../../OBS/CPC_GAUGE_0P50/CHUVACPC_0P50_%y4%m2%d2.nc" > chuva.ctl 
echo "title d"  >> chuva.ctl 
echo "options template ">> chuva.ctl
echo "undef -999">> chuva.ctl
echo "dtype netcdf">> chuva.ctl
echo "xdef 102 linear 279.75 0.5">> chuva.ctl
echo "ydef 092 linear -35.25 0.5">> chuva.ctl
echo "zdef 1 linear 0 1" >>chuva.ctl
echo "tdef "${dias[$m]}"  linear 01"$grads" 1dy"  >>chuva.ctl
echo "vars 1" >>chuva.ctl
echo "rain=>rain  0  t,y,x  the grid analysis (0.1mm/day)" >>chuva.ctl
echo "endvars" >>chuva.ctl



echo "'open chuva.ctl'" >script.gs
echo "'lats4d -o "$arqnc" -vars rain   -func (@/10)*"${dias[$m]}" -mean -time "$t1" "$t2"'"  >>script.gs
echo "'quit'" >>script.gs

 
grads -lbc "script.gs"

fi        


done
}


chuvamensalmerge()
{

mes=(xxx jan feb mar apr may jun jul aug sep oct nov dec)
dias=(99 31 28  31  30  31  30  31  31  30  31  30   31)
nummes=(99 01 02 03 04 05 06 07 08 09 10 11 12) 

for nmes in `seq 19  -1  0`
do


m=`date +"%-m" -d "$nmes months ago"`
mesx=`date +"%b" -d "$nmes months ago"` 	
grads=`date +"%b%Y" -d  "$nmes months ago"`	
grads2=`date +"%b%Y"`	

t1="01"$grads
t2=${dias[$m]}$grads

arqnc="../../OBS/CHUVA_MENSAL/CHUVA_MERGE_22km_MENSAL_"`date +"%m%Y" -d "$nmes months ago"`".nc" 
atual="../../OBS/CHUVA_MENSAL/CHUVA_MERGE_22km_MENSAL_"`date +"%m%Y"`".nc" 
rm $atual 


echo $arqnc
if test ! -e  $arqnc ;then 
echo $t1" " $t2
echo "dset ../../OBS/MERGE_0P25/CHUVAMERGE_0P22_%y4%m2%d2.nc" > chuvamerge.ctl
echo "options  little_endian template"                          >>chuvamerge.ctl
echo "title global daily analysis "                             >>chuvamerge.ctl
echo "undef -999.0 "                                            >>chuvamerge.ctl 
echo "dtype netcdf "                                            >>chuvamerge.ctl 
echo "xdef 251 linear    -80 0.2000"                           >>chuvamerge.ctl
echo "ydef 226 linear -35  0.2000 "                          >>chuvamerge.ctl
echo "zdef 1    linear 1 1 "                                  >>chuvamerge.ctl
echo "tdef "${dias[$m]}"  linear 01"$grads" 1dy"              >>chuvamerge.ctl  
echo "vars 1"                                                 >>chuvamerge.ctl
echo "rain=>rain  0 t,y,x  chuva acumulada 24 horas"                >>chuvamerge.ctl
echo "ENDVARS"   >>chuvamerge.ctl




echo "'open chuvamerge.ctl'" >script.gs
echo "'lats4d -o "$arqnc" -vars rain   -func (@)*"${dias[$m]}" -mean -time "$t1" "$t2"'"  >>script.gs
echo "'quit'" >>script.gs

 
grads -lbc "script.gs"

else
       echo $arqnc" Ja existe"
fi        


done
}





cd ../../WORKDISK 
export LANG=en_us_8859_1
chuvamensalcpc
chuvamensalmerge
