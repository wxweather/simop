#!/bin/sh 

export DIRXONS="../../../OUTPUT/"     ## diretorio OUTPUT

export LASTRUN=""             ##data da ultima rodada do XONS
export LASTRUNSMAP=""         ##data da  ultima rodada do SMAPONS  

export QOBSDIR="../../../OBS/QOBS/"  ### diretorio onde ficam as vazoes
export QOBSFILE="vobs_d.csv"         ### arquivo de vazoes diaria 
export QOBSCHECK=0          ### valor gerado pelo XONS. define se execta o SMAPONS ou não. 
export VOBSFILECHECK=0      ### 1 se arquivo de vazao estiver faltando ele repete ate completar o que for necessario 
                            ### 0  nao completa 

export ROD="00"							

 XONSCHECK[1]=0
 XONSCHECK[2]=0
 XONSCHECK[3]=0
 export XONSCHECK
 