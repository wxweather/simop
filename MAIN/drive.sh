#!/bin/bash 


calcbacia_eta40_smapons() 
{
#-------------------------------------------------------------------------------------
# LAMMOC/UFF  - LABORATÓRIO DE MONITORAMENTO E MODELAGEM DO SISTEMA CLIM
# AMBMET      - CONSULTORIA EM METEOROLOGIA , OCEANOGRAFIA E HIDROLOGIA 
# WXWEATHER   - CONSULTORIA EM METEOROLOGIA E COMPUTAÇÃO DE ALTO DESEMPENHO  
#     
#-------------------------------------------------------------------------------------
#
#  
#  
#
#
# -----------------------------------------------------------------------------------  
# 
#  Desenvolvido por: Reginaldo Ventura de Sa (regis@lamma.ufrj.br) 
#  Release 1 em :  27/03/2018     
#
#-----------------------------------------------------------------------------------
# Sintaxe:
#
#-----------------------------------------------------------------------------------------------
# CHANGE LOG
# 
#-----------------------------------------------------------------------------------------------
#
# INICIO

#
# script calcbacia
#
CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs" 
#
# arquivos para gerar previsao de conjuntos
#
cp ../MODULOS/CALCBACIA/CONFIG/RELACAO_GEFS_CONJUNTO.dat  .     >/dev/null 2>&1 
cp ../MODULOS/CALCBACIA/CONFIG/RELACAO_ETA_CONJUNTO.dat .        >/dev/null 2>&1 
#
# pega data de trabalho
#
if [ "$1" = "" ];then
   data=`date +"%Y%m%d"`
   datagrads=`date +"12Z%d%b%Y" -d"1 day"`
   rodada=$ROD 
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 


echo "["`date +"%Y%m%d %T"`"] INICIO DO LOG CALCBACIA ETA40KM " >> $CDLOG/"calcbacia_eta40_smapons.prn"  2>&1 
echo "["`date +"%Y%m%d %T"`"] INICIO DO LOG CALCBACIA ETA40KM "
if test -e  "../../CICC/"$data$rodada"/ETA40/"$data$rodada".bin"  ;then 
echo " Arquivo de dados ETA40km achado"
SMAPONSCHECK[1]=1
export SMAPONSCHECK
else
echo "[ERRO 001]  ARQUIVO DE DADOS ETA40KM EM ../../CICC/"$data$rodada"/ETA40/  NÃO EXISTE" 
 SMAPONSCHECK[1]=-1
 export SMAPONSCHECK
return
fi 
#
# cria o ctl do eta40km 
#



echo "DSET ^../../CICC/"$data$rodada"/ETA40/"$data$rodada".bin" >eta40.ctl 
echo "UNDEF -9999."                               >>eta40.ctl 
echo "TITLE eta 10 dias"                          >>eta40.ctl 
echo "XDEF  144 LINEAR  -83.00   0.40"            >>eta40.ctl 
echo "YDEF  157 LINEAR  -50.20   0.40"            >>eta40.ctl 
echo "ZDEF   1 LEVELS 1000"                       >>eta40.ctl  
echo "TDEF   10 LINEAR "$datagrads" 24hr"         >>eta40.ctl 
echo "VARS  1"                                    >>eta40.ctl 
echo "PREC  0  99  Total  24h Precip.        (m)" >>eta40.ctl  
echo "ENDVARS"                                    >>eta40.ctl 
cat eta40.ctl >> $CDLOG/"calcbacia_eta40_smapons.prn" 2>&1 
#
# configuração do calcbacia
#
echo "'open eta40.ctl' "                            >scripteta40.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_ons_eta40_V2.dat"'    >>scripteta40.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfigNT0156R4.dat"'      >>scripteta40.gs
echo "_modelo="ETA40""                            >>scripteta40.gs
echo "_prefixo="ETA40_LIMITES""                   >>scripteta40.gs
echo "freq=24"                                    >>scripteta40.gs
echo "rmv=1"                                      >>scripteta40.gs
echo "var="PREC""                                 >>scripteta40.gs
echo "tinicio=1"                                  >>scripteta40.gs
echo "numtempos=10 "                              >>scripteta40.gs
echo "modlon=0 "                                  >>scripteta40.gs
echo "_conjunto=1 "                               >>scripteta40.gs
echo '_arq_debug="'$CDLOG'/debug_eta40_smapons.prn"'        >>scripteta40.gs
echo "_formato="ALL""                             >>scripteta40.gs
echo "modocalc=PG"                                >>scripteta40.gs   
echo "_numens_cfs=6"                               >>scripteta40.gs     
echo "_postos=0"                               >>scripteta40.gs     
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scripteta40.gs
echo "'quit'"                                                  >>scripteta40.gs
#
# executa calcbacia V3
#
cat $CALC >>scripteta40.gs 
grads -lbc "scripteta40.gs"  >>$CDLOG/"calcbacia_eta40_smapons.prn" 2>&1 
#
# cria diretorio de SAIDA
#
SAIDA="../../OUTPUT/"$data$rodada
mkdir $SAIDA  >/dev/null 2>&1
mkdir $SAIDA"/SMAPONS"       >/dev/null 2>&1
mkdir $SAIDA"/SMAPONS/"      >/dev/null 2>&1
#
# redistribui para cada bacia/diretorio os arquivos 
#
bacias=(Grande Itaipu Paranapanema Paranaiba )
baciao=(GRANDE ITAIPU PARANAPANEMA PARANAIBA ) 
numbacias=3
for b in `seq 0 3`
do
mkdir $SAIDA"/SMAPONS/"${bacias[$b]}  >/dev/null 2>&1
mkdir $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"  >/dev/null 2>&1
done 
mv conjunto_ETA40_GRANDE.dat $SAIDA"/SMAPONS/Grande/ARQ_ENTRADA/"           >/dev/null 2>&1
mv conjunto_ETA40_ITAIPU.dat $SAIDA"/SMAPONS/Itaipu/ARQ_ENTRADA/"           >/dev/null 2>&1
mv conjunto_ETA40_PARANAIBA.dat $SAIDA"/SMAPONS/Paranaiba/ARQ_ENTRADA/"        >/dev/null 2>&1
mv conjunto_ETA40_PARANAPANEMA.dat $SAIDA"/SMAPONS/Paranapanema/ARQ_ENTRADA/"    >/dev/null 2>&1
for b in `seq 0 3`
do
for file in `ls -1 ${baciao[$b]}*`
do
file2=`echo $file | cut -d"_" -f2-6`
mv "./"$file $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"$file2  #>/dev/null 2>&1
done
done


echo "["`date`"] FINAL DO LOG CALCBACIA ETA40KM SMAPONS" $CDLOG/"calcbacia_eta40_smapons.prn" 2>&1 
echo "["`date`"] FINAL DO LOG CALCBACIA ETA40KM SMAPONS"



}


calcbacia_gefsrmv_smapons()
{
#-------------------------------------------------------------------------------------
# LAMMOC/UFF  - LABORATÓRIO DE MONITORAMENTO E MODELAGEM DO SISTEMA CLIM
# AMBMET      - CONSULTORIA EM METEOROLOGIA , OCEANOGRAFIA E HIDROLOGIA 
# WXWEATHER   - CONSULTORIA EM METEOROLOGIA E COMPUTAÇÃO DE ALTO DESEMPENHO  
#     
#-------------------------------------------------------------------------------------
#
#  
#  
#
#
# -----------------------------------------------------------------------------------  
# 
#  Desenvolvido por: Reginaldo Ventura de Sa (regis@lamma.ufrj.br) 
#  Release 1 em :  27/03/2018     
#
#-----------------------------------------------------------------------------------
# Sintaxe:
#
#-----------------------------------------------------------------------------------------------
# CHANGE LOG
# 
#-----------------------------------------------------------------------------------------------
#
# INICIO


CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs" 
cp ../MODULOS/CALCBACIA/CONFIG/RELACAO_GEFS_CONJUNTO.dat  .     >/dev/null 2>&1 
cp ../MODULOS/CALCBACIA/CONFIG/RELACAO_ETA_CONJUNTO.dat .        >/dev/null 2>&1 

if [ "$1" = "" ];then
   data=`date +"%Y%m%d"`
   datagrads=`date +"00Z%d%b%Y"`
   rodada=$ROD
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 



echo "["`date`"] INICIO DO LOG CALCBACIA GEFS COM REMOCAO VIES " >> $CDLOG/"calcbacia.gefsrmv_smapons.prn" 2>&1
echo "["`date`"] INICIO DO LOG CALCBACIA GEFS COM REMOCAO VIES "
if test -e  "../../CICC/"$data$rodada"/GEFS1P00/GEFS_"$rodada"_"$data"_BR_1P00.grb2"  ;then 
echo " Arquivo de dados ETA40km achado"
SMAPONSCHECK[2]=1
export SMAPONSCHECK
else
echo "[ERRO 001]  ARQUIVO DE DADOS ETA40KM EM ../../CICC/"$data$rodada"/ETA40/  NÃO EXISTE" 
SMAPONSCHECK[2]=-1
export SMAPONSCHECK
return
fi 


perl ./g2ctl.pl "../../CICC/"$data$rodada"/GEFS1P00/GEFS_"$rodada"_"$data"_BR_1P00.grb2" > gefs.ctl
gribmap -i gefs.ctl  
cat gefs.ctl >> $CDLOG/"calcbacia.gefsrmv_smapons.prn" 2>&1


echo "'open gefs.ctl' "                                      >scriptgefs.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/DOUGLAS_grande_gfs_1p00.dat"'    >>scriptgefs.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/GEFSconfigNT0156R4.dat"'      >>scriptgefs.gs
echo '_modelo="GEFSRMV"'                            >>scriptgefs.gs
echo '_prefixo="GEFSRMV_LIMITES"'                   >>scriptgefs.gs
echo "freq=6"                                    >>scriptgefs.gs
echo "rmv=1"                                      >>scriptgefs.gs
echo 'var="APCPsfc"'                                 >>scriptgefs.gs
echo "tinicio=1"                                  >>scriptgefs.gs
echo "numtempos=10"                              >>scriptgefs.gs
echo "modlon=1 "                                  >>scriptgefs.gs
echo "_conjunto=1 "                               >>scriptgefs.gs
echo '_arq_debug="'$CDLOG'/debug_gefsrmv_smapons.prn"'        >>scriptgefs.gs
echo "_formato="ALL""                             >>scriptgefs.gs
echo "modocalc=PG"                                >>scriptgefs.gs   
echo "_numens_cfs=6"                               >>scriptgefs.gs     
echo "_postos=0"                               >>scriptgefs.gs     
                       
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scriptgefs.gs
echo "'quit'"                                                  >>scriptgefs.gs



cat $CALC >>scriptgefs.gs 



grads -lbc "scriptgefs.gs"  >> $CDLOG/"calcbacia.gefsrmv_smapons.prn" 2>&1

SAIDA="../../OUTPUT/"$data$rodada
mkdir $SAIDA  >/dev/null 2>&1
mkdir $SAIDA"/SMAPONS"       >/dev/null 2>&1
bacias=(Grande Itaipu Paranapanema Paranaiba )
baciao=(GRANDE ITAIPU PARANAPANEMA PARANAIBA ) 
numbacias=3
for b in `seq 0 3`
do
mkdir $SAIDA"/SMAPONS/"${bacias[$b]}  >/dev/null 2>&1
mkdir $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"  >/dev/null 2>&1
done 



mv conjunto_GEFSRMV_GRANDE.dat $SAIDA"/SMAPONS/Grande/ARQ_ENTRADA"           >/dev/null 2>&1
mv conjunto_GEFSRMV_ITAIPU.dat $SAIDA"/SMAPONS/Itaipu/ARQ_ENTRADA"           >/dev/null 2>&1
mv conjunto_GEFSRMV_PARANAIBA.dat $SAIDA"/SMAPONS/Paranaiba/ARQ_ENTRADA"        >/dev/null 2>&1
mv conjunto_GEFSRMV_PARANAPANEMA.dat $SAIDA"/SMAPONS/Paranapanema/ARQ_ENTRADA"    >/dev/null 2>&1

bacias=(Grande Itaipu Paranapanema Paranaiba )
baciao=(GRANDE ITAIPU PARANAPANEMA PARANAIBA ) 
numbacias=3

for b in `seq 0 3`
do
for file in `ls -1 ${baciao[$b]}*`
do
file2=`echo $file | cut -d"_" -f2-6`
mv "./"$file $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"$file2  #>/dev/null 2>&1
done
done 





echo "["`date`"] FINAL DO LOG GEFS COM REMOCAO VIES  " >>  $CDLOG/"calcbacia.gefsrmv_smapons.prn" 2>&1 
echo "["`date`"] FINAL DO LOG GEFS COM REMOCAO VIES  "


}


calcbacia_conjuntos()
{
#-------------------------------------------------------------------------------------
# LAMMOC/UFF  - LABORATÓRIO DE MONITORAMENTO E MODELAGEM DO SISTEMA CLIM
# AMBMET      - CONSULTORIA EM METEOROLOGIA , OCEANOGRAFIA E HIDROLOGIA 
# WXWEATHER   - CONSULTORIA EM METEOROLOGIA E COMPUTAÇÃO DE ALTO DESEMPENHO  
#     
#-------------------------------------------------------------------------------------
#
#  
#  
#
#
# -----------------------------------------------------------------------------------  
# 
#  Desenvolvido por: Reginaldo Ventura de Sa (regis@lamma.ufrj.br) 
#  Release 1 em :  27/03/2018     
#
#-----------------------------------------------------------------------------------
# Sintaxe:
#
#-----------------------------------------------------------------------------------------------
# CHANGE LOG
# 
#-----------------------------------------------------------------------------------------------
#
# INICIO

CALC="../MODULOS/CALCBACIA/calc_conjunto.gs" 


if [ $1 ="" ];then
   data=`date +"%Y%m%d"`
   datagrads=`date +"00Z%d%b%Y" -d"1 day"`
   rodada=$ROD
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 








echo "["`date`"] INICIO DO LOG CALCBACIA CONJUNTO ETA40+GEFS " >> $CDLOG/"calcbacia.conjunto.prn" 2>&1 
echo "["`date`"] INICIO DO LOG CALCBACIA CONJUNTO ETA40+GEFS "

echo "DSET ^../../CICC/"$data$rodada"/ETA40/"$data$rodada".bin" >eta40.ctl 
echo "UNDEF -9999."                               >>eta40.ctl 
echo "TITLE eta 10 dias"                          >>eta40.ctl 
echo "XDEF  144 LINEAR  -83.00   0.40"            >>eta40.ctl 
echo "YDEF  157 LINEAR  -50.20   0.40"            >>eta40.ctl 
echo "ZDEF   1 LEVELS 1000"                       >>eta40.ctl  
echo "TDEF   10 LINEAR "$datagrads" 24hr"         >>eta40.ctl 
echo "VARS  1"                                    >>eta40.ctl 
echo "PREC  0  99  Total  24h Precip.        (m)" >>eta40.ctl  
echo "ENDVARS"                                    >>eta40.ctl 

cat eta40.ctl >> $CDLOG/"calcbacia.conjunto.prn" 2>&1 



bacias=(Grande Itaipu Paranapanema Paranaiba )
baciao=(GRANDE ITAIPU PARANAPANEMA PARANAIBA ) 
numbacias=3

for b in `seq 0 $numbacias`
do 
echo "'open eta40.ctl' "                            >scriptconjunto.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_ons_eta40.dat"'    >>scriptconjunto.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfigNT0156R4.dat"'      >>scriptconjunto.gs
echo "_modelo="ETA40""                            >>scriptconjunto.gs
echo '_prefixo="PRECMEDIA_ETA40.GEFS"'                   >>scriptconjunto.gs
echo '_baciap="'${baciao[b]}'"'                                    >>scriptconjunto.gs
echo 'arq_eta= "../../OUTPUT/'$data$rodada'/SMAPONS/'${bacias[b]}'/ARQ_ENTRADA/conjunto_ETA40_'${baciao[b]}'.dat"'  >>scriptconjunto.gs
echo 'arq_gefs="../../OUTPUT/'$data$rodada'/SMAPONS/'${bacias[b]}'/ARQ_ENTRADA/conjunto_GEFSRMV_'${baciao[b]}'.dat"'  >>scriptconjunto.gs
echo "pi=prevconj(arq_eta, arq_gefs, 10) "  >>scriptconjunto.gs
echo "'quit'"                                                  >>scriptconjunto.gs
cat $CALC >>scriptconjunto.gs 

echo `"pwd"` >>$CDLOG"/calcbacia.conjunto.prn" 2>&1
grads -lbc "scriptconjunto.gs" >>$CDLOG"/calcbacia.conjunto.prn" 2>&1 
#mv ${baciao[b]}* $SAIDA"/SMAPONS/"${bacia[b]}"/ARQ_ENTRADA"                   >/dev/null 2>&1
done


SAIDA="../../OUTPUT/"$data$rodada
for b in `seq 0 3`
do
for file in `ls -1 ${baciao[$b]}*`
do
file2=`echo $file | cut -d"_" -f2-6`
mv "./"$file $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"$file2  #>/dev/null 2>&1
done
done 




} 


calcbacia_chuvamerge_smapons()
{
export LANG=en_us_8859_1
cd ../SCRIPTS/WORKDISK/
cp ../MODULOS/CALCBACIA/CONFIG/config_postos_ons.dat .
CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs"  


if [ "$1" = "" ];then
   data=`date +"%Y%m%d" `
   datagrads=`date +"00Z%d%b%Y" -d"90 days ago"`
   rodada=$ROD
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 

echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA 22 km SMAPONS" >> $CDLOG/"/calcbacia.chuvamerge_SMAPONS.prn" 2>&1 
echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA 22 km SMAPONS"



echo "dset ../../OBS/MERGE_0P25/CHUVAMERGE_0P22_%y4%m2%d2.nc" > chuvamerge.ctl 
echo "title d"                                                  >> chuvamerge.ctl 
echo "options template"                                         >> chuvamerge.ctl 
echo "undef -999"                                               >> chuvamerge.ctl 
echo "dtype netcdf"                                              >> chuvamerge.ctl  
echo "xdef 251 linear -80 0.2"                               >> chuvamerge.ctl 
echo "ydef 226 linear -35 0.2"                               >> chuvamerge.ctl 
echo "zdef 1 linear 0 1"                                        >> chuvamerge.ctl  
echo "tdef 91  linear "$datagrads" 1dy"                         >> chuvamerge.ctl 
echo "vars 1"                                                   >> chuvamerge.ctl 
echo "rain=>rain  0  t,y,x  chuva acumulada 24 horas"      >> chuvamerge.ctl    
echo "endvars"                                                  >> chuvamerge.ctl 
echo "'open chuvamerge.ctl' "                            >scriptchuvamerge.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_ons_observado.dat"'    >>scriptchuvamerge.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfig.dat"'      >>scriptchuvamerge.gs
echo "_modelo="OBSMERGE""                            >>scriptchuvamerge.gs
echo "_prefixo="CHUVAMERGE""                   >>scriptchuvamerge.gs
echo "freq=24"                                    >>scriptchuvamerge.gs
echo "rmv=0"                                      >>scriptchuvamerge.gs
echo "var="rain""                                 >>scriptchuvamerge.gs
echo "tinicio=1"                                  >>scriptchuvamerge.gs
echo "numtempos=91 "                              >>scriptchuvamerge.gs
echo "modlon=0 "                                  >>scriptchuvamerge.gs
echo "_conjunto=0 "                               >>scriptchuvamerge.gs
echo '_arq_debug="'$CDLOG'/debug_chuvamerge.prn"'        >>scriptchuvamerge.gs
echo "_formato="ALL""                             >>scriptchuvamerge.gs
echo "modocalc=CMB"                                >>scriptchuvamerge.gs   
echo "_numens_cfs=6"                               >>scriptchuvamerge.gs     
echo "_postos=1"                                   >>scriptchuvamerge.gs     
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scriptchuvamerge.gs
echo "'quit'"                                                  >>scriptchuvamerge.gs


cat $CALC >>scriptchuvamerge.gs 


grads -lbc "scriptchuvamerge.gs"  >>$CDLOG"/calcbacia.chuvamerge_SMAPONS.prn" 2>&1 

SAIDA="../../OUTPUT/"$data$rodada



bacias=(Grande Itaipu Paranapanema Paranaiba )
baciao=(GRANDE ITAIPU PARANAPANEMA PARANAIBA ) 
numbacias=3

for b in `seq 0 3`
do
for file in `ls -1 ${baciao[$b]}*`
do
file2=`echo $file | cut -d"_" -f2-6`
echo uniq $file   $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"$file2
uniq $file >  $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"$file2
done
done




 
echo "["`date`"] FINAL DO LOG CALCBACIA CHUVA MERGE " >>$CDLOG"/calcbacia.chuvamerge_SMAPONS.prn" 2>&1 
echo "["`date`"] FINAL DO LOG CALCBACIA CHUVA MERGE "








}


calcbacia_eta40_cmb() 
{

 

CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs" 
cp ../MODULOS/CALCBACIA/CONFIG/RELACAO_GEFS_CONJUNTO.dat  .     >/dev/null 2>&1 
cp ../MODULOS/CALCBACIA/CONFIG/RELACAO_ETA_CONJUNTO.dat .        >/dev/null 2>&1 

if [ $1 ="" ];then
   data=`date +"%Y%m%d"`
   datagrads=`date +"12Z%d%b%Y" -d"1 day"`
   rodada="00"
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 
CDLOG="../../LOG/"$data
mkdir $CDLOG   >/dev/null 2>&1 

if test -e "../../CICC/"$data$rodada"/ETA40/"$data$rodada".bin" ;then 


echo "["`date +"%Y%m%d %T"`"] INICIO DO LOG CALCBACIA ETA40KM " > $CDLOG/"calcbacia.eta40.prn" 2>&1 
echo "["`date +"%Y%m%d %T"`"] INICIO DO LOG CALCBACIA ETA40KM "
echo "DSET ^../../CICC/"$data$rodada"/ETA40/"$data$rodada".bin" >eta40.ctl 
echo "UNDEF -9999."                               >>eta40.ctl 
echo "TITLE eta 10 dias"                          >>eta40.ctl 
echo "XDEF  144 LINEAR  -83.00   0.40"            >>eta40.ctl 
echo "YDEF  157 LINEAR  -50.20   0.40"            >>eta40.ctl 
echo "ZDEF   1 LEVELS 1000"                       >>eta40.ctl  
echo "TDEF   10 LINEAR "$datagrads" 24hr"         >>eta40.ctl 
echo "VARS  1"                                    >>eta40.ctl 
echo "PREC  0  99  Total  24h Precip.        (m)" >>eta40.ctl  
echo "ENDVARS"                                    >>eta40.ctl 
cat eta40.ctl >> $CDLOG/"calcbacia.eta40.prn" 2>&1 





SAIDA="../../OUTPUT/"$data$rodada
mkdir $SAIDA  >/dev/null 2>&1
mkdir $SAIDA"/DIARIO"       >/dev/null 2>&1




echo "'open eta40.ctl' "                            >scripteta40naoons.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_lammoc_eta40.dat"'    >>scripteta40naoons.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfigNT0156R4.dat"'      >>scripteta40naoons.gs
echo "_modelo="ETA40""                            >>scripteta40naoons.gs
echo "_prefixo="ETA40""                   >>scripteta40naoons.gs
echo "freq=24"                                    >>scripteta40naoons.gs
echo "rmv=0"                                      >>scripteta40naoons.gs
echo "var="PREC""                                 >>scripteta40naoons.gs
echo "tinicio=1"                                  >>scripteta40naoons.gs
echo "numtempos=10 "                              >>scripteta40naoons.gs
echo "modlon=0 "                                  >>scripteta40naoons.gs
echo "_conjunto=1 "                               >>scripteta40naoons.gs
echo '_arq_debug="'$CDLOG'/debug_eta40.prn"'        >>scripteta40naoons.gs
echo "_formato="ALL""                             >>scripteta40naoons.gs
echo "modocalc=CMB"                                >>scripteta40naoons.gs   
echo "_numens_cfs=6"                               >>scripteta40naoons.gs     
echo "_postos=0"                               >>scripteta40naoons.gs     
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scripteta40naoons.gs
echo "'quit'"                                                  >>scripteta40naoons.gs


cat $CALC >>scripteta40naoons.gs 


grads -lbc "scripteta40naoons.gs"  >>$CDLOG"/calcbacia.eta40.prn" 2>&1 
mv ETA40* $SAIDA"/DIARIO"  >/dev/null 2>&1

fi 
 
echo "["`date`"] FINAL DO LOG CALCBACIA ETA40KM CMB " >> $CDLOG/"calcbacia.eta40.prn" 2>&1 
echo "["`date`"] FINAL DO LOG CALCBACIA ETA40KM CMB"



}


calcbacia_gefsrmv_cmb()
{



CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs" 
cp ../MODULOS/CALCBACIA/CONFIG/RELACAO_GEFS_CONJUNTO.dat  .     >/dev/null 2>&1 
cp ../MODULOS/CALCBACIA/CONFIG/RELACAO_ETA_CONJUNTO.dat .        >/dev/null 2>&1 

if [ $1 ="" ];then
   data=`date +"%Y%m%d"`
   datagrads=`date +"00Z%d%b%Y"`
   rodada="00"
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 
CDLOG="../../LOG/"$data
mkdir $CDLOG   >/dev/null 2>&1 


echo "["`date`"] INICIO DO LOG CALCBACIA GEFS COM REMOCAO VIES " > $CDLOG/"calcbacia.gefsrmv.prn" 2>/dev/null 
echo "["`date`"] INICIO DO LOG CALCBACIA GEFS COM REMOCAO VIES "



perl ./g2ctl.pl "../../CICC/"$data$rodada"/GEFS1P00/GEFS_"$rodada"_"$data"_BR_1P00.grb2" > gefs.ctl
gribmap -i gefs.ctl  
cat gefs.ctl >> $CDLOG/"calcbacia.gefsrmv.prn" 2>&1



SAIDA="../../OUTPUT/"$data$rodada
mkdir $SAIDA  >/dev/null 2>&1
mkdir $SAIDA"/DIARIO"       >/dev/null 2>&1

echo "'open gefs.ctl' "                                      >scriptgefsnaoons.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_lammoc_eta40.dat"'    >>scriptgefsnaoons.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/GEFSconfigNT0156R4.dat"'      >>scriptgefsnaoons.gs
echo '_modelo="GEFS"'                            >>scriptgefsnaoons.gs
echo '_prefixo="GEFS"'                   >>scriptgefsnaoons.gs
echo "freq=6"                                    >>scriptgefsnaoons.gs
echo "rmv=0"                                      >>scriptgefsnaoons.gs
echo 'var="APCPsfc"'                                 >>scriptgefsnaoons.gs
echo "tinicio=1"                                  >>scriptgefsnaoons.gs
echo "numtempos=10 "                              >>scriptgefsnaoons.gs
echo "modlon=1 "                                  >>scriptgefsnaoons.gs
echo "_conjunto=0 "                               >>scriptgefsnaoons.gs
echo '_arq_debug="'$CDLOG'/debug_gfs.prn"'        >>scriptgefsnaoons.gs
echo "_formato="ALL""                             >>scriptgefsnaoons.gs
echo "modocalc=CMB"                                >>scriptgefsnaoons.gs   
echo "_numens_cfs=6"                               >>scriptgefsnaoons.gs     
echo "_postos=0"                               >>scriptgefsnaoons.gs     
                       
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scriptgefsnaoons.gs
echo "'quit'"                                                  >>scriptgefsnaoons.gs



cat $CALC >>scriptgefsnaoons.gs 



grads -lbc "scriptgefsnaoons.gs"  > $CDLOG/"calcbacia.gefs.prn" 2>&1


mv GEFS* $SAIDA"/DIARIO"  >/dev/null 2>&1

echo "["`date`"] FINAL DO LOG GEFS COM REMOCAO VIES  " >>  $CDLOG/"calcbacia.gefs.prn" 2>&1 
echo "["`date`"] FINAL DO LOG GEFS COM REMOCAO VIES  "


}


calcbacia_cfsmensal()
{


cd ../SCRIPTS/WORKDISK/
CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs" 
#cp ../MODULOS/CALCBACIA/RELACAO_GEFS_CONJUNTO.dat  .     >/dev/null 2>&1 
#cp ../MODULOS/CALCBACIA/RELACAO_ETA_CONJUNTO.dat .        >/dev/null 2>&1 

if [ $1 ="" ];then
   data=`date +"%Y%m%d"`
   datagrads=`date +"00Z%d%b%Y"`
   rodada="00"
   dia=`date +"%d"`
else
   data=$1
   datagrads=$2
   rodada=$3   
   dia=$4
fi 
CDLOG="../../LOG/"$data
mkdir $CDLOG   >/dev/null 2>&1 


echo "["`date`"] INICIO DO LOG CALCBACIA CFS " > $CDLOG/"calcbacia.cfsmensal.prn" 2>&1 
echo "["`date`"] INICIO DO LOG CALCBACIA CFS "



#./g2ctl.pl "../../CICC/"$data$rodada"/GEFS1P00/GEFS_"$rodada"_"$data"_BR_1P00.grb2" > gefs.ctl 
#gribmap -i gefs.ctl 
#cat gefs.ctl >> $CDLOG/"calcbacia.gefsrmv.prn" 2>&1



# echo "dset ../../CICC/"$data$rodada"/CFS/ENS/CFS_MENSAL_"$data$rodada".nc" > cfs.ctl 
# echo "title cfs" >> cfs.ctl
# echo "undef 9.999e+20" >> cfs.ctl
# echo "xdef 55 linear 279.375 0.9375" >> cfs.ctl
# echo "ydef 50 levels -35.433 -34.488 -33.543 -32.598 -31.653 -30.709 -29.764 -28.819" >> cfs.ctl
# echo " -27.874 -26.929 -25.984 -25.039 -24.094 -23.15 -22.205 -21.26 -20.315 -19.37" >> cfs.ctl
# echo " -18.425 -17.48 -16.535 -15.59 -14.646 -13.701 -12.756 -11.811 -10.866 -9.921" >> cfs.ctl
# echo " -8.976 -8.031 -7.087 -6.142 -5.197 -4.252 -3.307 -2.362 -1.417 -0.472" >> cfs.ctl
# echo " 0.472 1.417 2.362 3.307 4.252 5.197 6.142 7.087 8.031 8.976" >> cfs.ctl
# echo " 9.921 10.866" >> cfs.ctl
# echo "zdef 1 linear 0 1" >> cfs.ctl
# echo "tdef 6 linear 00Z01APR2018 1mo" >> cfs.ctl
# echo "vars 6 " >> cfs.ctl
# echo "lead01  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]" >> cfs.ctl
# echo "lead02  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]" >> cfs.ctl
# echo "lead03  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]" >> cfs.ctl
# echo "lead04  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]" >> cfs.ctl
# echo "lead05  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]" >> cfs.ctl
# echo "lead06  0  t,y,x  ** surface Precipitation Rate [kg/m^2/s]" >> cfs.ctl
# echo "endvars" >> cfs.ctl


# 'open cfsbin2.ctl'
# say "INICIO"
# _arqbacia="bacias_eta40.dat"
# _arqconfig="ETAconfig.dat"
# _modelo="CFS"
# _prefixo="ETA40_LIMITES"
# freq=24
# rmv=0
# var="lead01 lead02 lead03 lead04 lead05 lead06"
# tinicio=1
# numtempos=15
# modlon=1
# conjunto=0
# _arq_debug="debug_chuvamensal.prn"
# _formato="ALL"
# modocalc="CFSENS"
# _numens_cfs=6
# pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)

#echo "'open cfs.ctl' "                                      >scriptcfsmensal.gs
echo "'sdfopen ../../CICC/"$data$rodada"/CFS/ENS/CFS_MENSAL_"$data$rodada".nc'" > scriptcfsmensal.gs  
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_lammoc_cfs.dat"'         >>scriptcfsmensal.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfigNT0156R4.dat"'                  >>scriptcfsmensal.gs
echo '_modelo="CFSMENSAL"'                            >>scriptcfsmensal.gs
echo '_prefixo="CFSMENSAL"'                   >>scriptcfsmensal.gs
echo "freq=24"                                    >>scriptcfsmensal.gs
echo "rmv=0"                                      >>scriptcfsmensal.gs

echo "DIA="$dia 
if [ "$dia" -le "10" ] ;then 
echo 'var="lead00 lead01 lead02 lead03 lead04 lead05 lead06"'                                 >>scriptcfsmensal.gs
else
echo 'var="lead01 lead02 lead03 lead04 lead05 lead06 lead07"'                                 >>scriptcfsmensal.gs
fi 


echo "tinicio=1"                                  >>scriptcfsmensal.gs
echo "numtempos=6 "                              >>scriptcfsmensal.gs
echo "modlon=1 "                                  >>scriptcfsmensal.gs
echo "_conjunto=0 "                               >>scriptcfsmensal.gs
echo '_arq_debug="'$CDLOG'/debug_cfsmensal.prn"'        >>scriptcfsmensal.gs
echo "_formato="ALL""                             >>scriptcfsmensal.gs
echo "modocalc=CFSENS"                                >>scriptcfsmensal.gs   
echo "_numens_cfs=7"                               >>scriptcfsmensal.gs     
echo "_postos=0"                               >>scriptcfsmensal.gs     
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scriptcfsmensal.gs
echo "'quit'"                                                  >>scriptcfsmensal.gs



cat $CALC >>scriptcfsmensal.gs 



grads -lbc "scriptcfsmensal.gs"  >$CDLOG"/calcbacia.cfsmensal.prn" 2>&1 

SAIDA="../../OUTPUT/"$data$rodada"/MENSAL/"
mkdir -p $SAIDA
mv CFSMENSAL* $SAIDA


}


calcbacia_chuvacpc()
{
export LANG=en_us_8859_1
cd ../SCRIPTS/WORKDISK/
cp ../MODULOS/CALCBACIA/CONFIG/config_postos_ons.dat .
CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs"  


if [ $1 ="" ];then
   data=`date +"%Y%m%d" `
   datagrads=`date +"00Z%d%b%Y" -d"90 days ago"`
   rodada="00"
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 
CDLOG="../../LOG/"$data
mkdir $CDLOG   >/dev/null 2>&1 


echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA 54 km" > $CDLOG/"calcbacia.chuvacpc.prn" 2>&1 
echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA 54 km "

echo "dset ../../OBS/CPC_GAUGE_0P50/CHUVACPC_0P50_%y4%m2%d2.nc" > chuvacpc.ctl 
echo "title d"                                                  >> chuvacpc.ctl 
echo "options template"                                         >> chuvacpc.ctl 
echo "undef -999"                                               >> chuvacpc.ctl 
echo "dtype netcdf"                                              >> chuvacpc.ctl  
echo "xdef 102 linear 279.75 0.5"                               >> chuvacpc.ctl 
echo "ydef 092 linear -35.25 0.5"                               >> chuvacpc.ctl 
echo "zdef 1 linear 0 1"                                        >> chuvacpc.ctl  
echo "tdef 90  linear "$datagrads" 1dy"                         >> chuvacpc.ctl 
echo "vars 1"                                                   >> chuvacpc.ctl 
echo "rain=>rain  0  t,y,x  the grid analysis (0.1mm/day)"      >> chuvacpc.ctl    
echo "endvars"                                                  >> chuvacpc.ctl 


echo "'open chuvacpc.ctl' "                            >scriptchuvacpc.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_ons_observado.dat"'    >>scriptchuvacpc.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfig.dat"'      >>scriptchuvacpc.gs
echo "_modelo="OBSCPC""                            >>scriptchuvacpc.gs
echo "_prefixo="CHUVACPC""                   >>scriptchuvacpc.gs
echo "freq=24"                                    >>scriptchuvacpc.gs
echo "rmv=0"                                      >>scriptchuvacpc.gs
echo "var="rain""                                 >>scriptchuvacpc.gs
echo "tinicio=1"                                  >>scriptchuvacpc.gs
echo "numtempos=90 "                              >>scriptchuvacpc.gs
echo "modlon=1 "                                  >>scriptchuvacpc.gs
echo "_conjunto=0 "                               >>scriptchuvacpc.gs
echo '_arq_debug="'$CDLOG'/debug_chuvacpc.prn"'        >>scriptchuvacpc.gs
echo "_formato="ALL""                             >>scriptchuvacpc.gs
echo "modocalc=CMB"                                >>scriptchuvacpc.gs   
echo "_numens_cfs=6"                               >>scriptchuvacpc.gs     
echo "_postos=1"                                   >>scriptchuvacpc.gs     
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scriptchuvacpc.gs
echo "'quit'"                                                  >>scriptchuvacpc.gs


cat $CALC >>scriptchuvacpc.gs 


#grads -lbc "scriptchuvacpc.gs"  >>$CDLOG"/calcbacia.chuvacpc.prn" 2>&1 

SAIDA="../../OUTPUT/"$data$rodada




# bacias=(Grande Itaipu Paranapanema Paranaiba )
# baciao=(GRANDE ITAIPU PARANAPANEMA PARANAIBA ) 
# numbacias=3

# for b in `seq 0 3`
# do
# for file in `ls -1 ${baciao[$b]}*`
# do
# file2=`echo $file | cut -d"_" -f2-6`
# mv "./"$file $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"$file2  #>/dev/null 2>&1
# done
# done 


echo "'open chuvacpc.ctl' "                            >scriptchuvacpcnaoons.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_lammoc_merge.dat"'    >>scriptchuvacpcnaoons.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfig.dat"'      >>scriptchuvacpcnaoons.gs
echo "_modelo="OBSCPC""                            >>scriptchuvacpcnaoons.gs
echo "_prefixo="CHUVACPC""                   >>scriptchuvacpcnaoons.gs
echo "freq=24"                                    >>scriptchuvacpcnaoons.gs
echo "rmv=0"                                      >>scriptchuvacpcnaoons.gs
echo "var="rain""                                 >>scriptchuvacpcnaoons.gs
echo "tinicio=1"                                  >>scriptchuvacpcnaoons.gs
echo "numtempos=90 "                              >>scriptchuvacpcnaoons.gs
echo "modlon=1 "                                  >>scriptchuvacpcnaoons.gs
echo "_conjunto=0 "                               >>scriptchuvacpcnaoons.gs
echo '_arq_debug="'$CDLOG'/debug_chuvacpc.prn"'        >>scriptchuvacpcnaoons.gs
echo "_formato="ALL""                             >>scriptchuvacpcnaoons.gs
echo "modocalc=CMB"                                >>scriptchuvacpcnaoons.gs   
echo "_numens_cfs=6"                               >>scriptchuvacpcnaoons.gs     
echo "_postos=0"                                   >>scriptchuvacpcnaoons.gs     
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scriptchuvacpcnaoons.gs
echo "'quit'"                                                  >>scriptchuvacpcnaoons.gs


cat $CALC >>scriptchuvacpcnaoons.gs 


grads -lbc "scriptchuvacpcnaoons.gs"  >>$CDLOG"/calcbacia.chuvacpc.prn" 2>&1 


mkdir $SAIDA"/DIARIO" >/dev/null 2>&1
mv CHUVACPC*     $SAIDA"/DIARIO" >/dev/null 2>&1

echo "["`date`"] FINAL DO LOG CALCBACIA CHUVA CPC " >>$CDLOG"/calcbacia.chuvacpc.prn" 2>&1 
echo "["`date`"] FINAL DO LOG CALCBACIA CHUVA CPC "


}


calcbacia_chuvamerge_cmb()
{
export LANG=en_us_8859_1
cd ../SCRIPTS/WORKDISK/
cp ../MODULOS/CALCBACIA/CONFIG/config_postos_ons.dat .
CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs"  


if [ $1 ="" ];then
   data=`date +"%Y%m%d" `
   datagrads=`date +"00Z%d%b%Y" -d"90 days ago"`
   rodada="00"
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 
CDLOG="../../LOG/"$data
mkdir $CDLOG   >/dev/null 2>&1 
echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA 22 km" > $CDLOG/"calcbacia.chuvamerge.prn" 2>&1 
echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA 22 km "



echo "dset ../../OBS/MERGE_0P25/CHUVAMERGE_0P22_%y4%m2%d2.nc" > chuvamerge.ctl 
echo "title d"                                                  >> chuvamerge.ctl 
echo "options template"                                         >> chuvamerge.ctl 
echo "undef -999"                                               >> chuvamerge.ctl 
echo "dtype netcdf"                                              >> chuvamerge.ctl  
echo "xdef 251 linear -80 0.2"                               >> chuvamerge.ctl 
echo "ydef 226 linear -35 0.2"                               >> chuvamerge.ctl 
echo "zdef 1 linear 0 1"                                        >> chuvamerge.ctl  
echo "tdef 91  linear "$datagrads" 1dy"                         >> chuvamerge.ctl 
echo "vars 1"                                                   >> chuvamerge.ctl 
echo "rain=>rain  0  t,y,x  chuva acumulada 24 horas"      >> chuvamerge.ctl    
echo "endvars"                                                  >> chuvamerge.ctl 




SAIDA="../../OUTPUT/"$data$rodada






echo "'open chuvamerge.ctl' "                                                 >scriptchuvamergenaoons.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_lammoc_merge.dat"'    >>scriptchuvamergenaoons.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfig.dat"'                >>scriptchuvamergenaoons.gs
echo "_modelo="OBSMERGE""                                                    >>scriptchuvamergenaoons.gs
echo "_prefixo="CHUVAMERGE""                                                 >>scriptchuvamergenaoons.gs
echo "freq=24"                                    >>scriptchuvamergenaoons.gs
echo "rmv=0"                                      >>scriptchuvamergenaoons.gs
echo "var="rain""                                 >>scriptchuvamergenaoons.gs
echo "tinicio=1"                                  >>scriptchuvamergenaoons.gs
echo "numtempos=91 "                              >>scriptchuvamergenaoons.gs
echo "modlon=0 "                                  >>scriptchuvamergenaoons.gs
echo "_conjunto=0 "                               >>scriptchuvamergenaoons.gs
echo '_arq_debug="'$CDLOG'/debug_chuvamerge.prn"'        >>scriptchuvamergenaoons.gs
echo "_formato="ALL""                             >>scriptchuvamergenaoons.gs
echo "modocalc=CMB"                                >>scriptchuvamergenaoons.gs   
echo "_numens_cfs=6"                               >>scriptchuvamergenaoons.gs     
echo "_postos=0"                                   >>scriptchuvamergenaoons.gs     
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scriptchuvamergenaoons.gs
echo "'quit'"                                                  >>scriptchuvamergenaoons.gs


cat $CALC >>scriptchuvamergenaoons.gs 


grads -lbc "scriptchuvamergenaoons.gs"  >>$CDLOG"/calcbacia.chuvamerge.prn" 2>&1 

mkdir $SAIDA"/DIARIO" >/dev/null 2>&1
mv CHUVAMERGE* $SAIDA"/DIARIO"   >/dev/null 2>&1








 
echo "["`date`"] FINAL DO LOG CALCBACIA CHUVA MERGE CMB " >>$CDLOG"/calcbacia.chuvamerge.prn" 2>&1 
echo "["`date`"] FINAL DO LOG CALCBACIA CHUVA MERGE CMB"








}


calcbacia_chuvacpc_mensal()
{

cd ../SCRIPTS/WORKDISK/

#../MODULOS/CHUVAMENSAL/chuvamensal_cpc.sh

export LANG=en_us_8859_1


#cp ../MODULOS/CALCBACIA/config_postos_ons.dat .
CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs"  


if [ $1 ="" ];then
   data=`date +"%Y%m%d" `
   datagrads=`date +"00Z01%b%Y" -d"18 months ago"`
   rodada="00"
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 
CDLOG="../../LOG/"$data
mkdir $CDLOG   >/dev/null 2>&1 


echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA 54 km MENSAL" > $CDLOG/"calcbacia.chuvacpcmensal.prn" 2>&1 
echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA 54 km MENSAL "
echo "dset ../../OBS/CHUVA_MENSAL/CHUVA_CPC_54km_MENSAL_%m2%y4.nc" > chuvacpcmensal.ctl 
echo "title d"                                                  >> chuvacpcmensal.ctl 
echo "options template"                                         >> chuvacpcmensal.ctl 
echo "undef -999"                                               >> chuvacpcmensal.ctl 
echo "dtype netcdf"                                              >> chuvacpcmensal.ctl  
echo "xdef 102 linear 279.75 0.5"                               >> chuvacpcmensal.ctl 
echo "ydef 092 linear -35.25 0.5"                               >> chuvacpcmensal.ctl 
echo "zdef 1 linear 0 1"                                        >> chuvacpcmensal.ctl  
echo "tdef 19  linear "$datagrads" 1mo"                         >> chuvacpcmensal.ctl 
echo "vars 1"                                                   >> chuvacpcmensal.ctl 
echo "rain=>rain  0  t,y,x  the grid analysis (0.1mm/day)"      >> chuvacpcmensal.ctl    
echo "endvars"                                                  >> chuvacpcmensal.ctl 




echo "'open chuvacpcmensal.ctl' "                            >scriptchuvacpcmensal.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_simop.dat"'    >>scriptchuvacpcmensal.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfig.dat"'      >>scriptchuvacpcmensal.gs
echo "_modelo="OBSCPCMENSAL""                            >>scriptchuvacpcmensal.gs
echo "_prefixo="CHUVACPCMENSAL""                   >>scriptchuvacpcmensal.gs
echo "freq=24"                                    >>scriptchuvacpcmensal.gs
echo "rmv=0"                                      >>scriptchuvacpcmensal.gs
echo "var="rain""                                 >>scriptchuvacpcmensal.gs
echo "tinicio=1"                                  >>scriptchuvacpcmensal.gs
echo "numtempos=19 "                              >>scriptchuvacpcmensal.gs
echo "modlon=1 "                                  >>scriptchuvacpcmensal.gs
echo "_conjunto=0 "                               >>scriptchuvacpcmensal.gs
echo '_arq_debug="'$CDLOG'/debug_chuvacpcmensal.prn"'        >>scriptchuvacpcmensal.gs
echo "_formato="ALL""                             >>scriptchuvacpcmensal.gs
echo "modocalc=CMB"                                >>scriptchuvacpcmensal.gs   
echo "_numens_cfs=6"                               >>scriptchuvacpcmensal.gs       
echo "_postos=0"                               >>scriptchuvacpcmensal.gs       
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scriptchuvacpcmensal.gs
echo "'quit'"                                                  >>scriptchuvacpcmensal.gs




cat $CALC >>scriptchuvacpcmensal.gs 


grads -lbc "scriptchuvacpcmensal.gs"  >>$CDLOG"/calcbacia.chuvacpcmensal.prn" 2>&1 




SAIDA="../../OUTPUT/"$data$rodada
mkdir $SAIDA"/MENSAL/" >/dev/null 2>&1
mv CHUVACPCMENSAL*   $SAIDA"/MENSAL" >/dev/null 2>&1



# bacias=(Grande Itaipu Paranapanema Paranaiba )
# baciao=(GRANDE ITAIPU PARANAPANEMA PARANAIBA ) 
# numbacias=3

# for b in `seq 0 3`
# do
# for file in `ls -1 ${baciao[$b]}*`
# do
# file2=`echo $file | cut -d"_" -f2-6`
# mv "./"$file $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"$file2  #>/dev/null 2>&1
# done
# done 
echo "["`date`"] FINAL DO LOG CALCBACIA ETA40KM " >>$CDLOG"/calcbacia.chuvacpcmensal.prn" 2>&1 
echo "["`date`"] FINAL DO LOG CALCBACIA ETA40KM "


}


calcbacia_chuvamerge_mensal()
{

cd ../SCRIPTS/WORKDISK/

#../MODULOS/CHUVAMENSAL/chuvamensal_merge.sh

export LANG=en_us_8859_1


#cp ../MODULOS/CALCBACIA/config_postos_ons.dat .
CALC="../MODULOS/CALCBACIA/calcbaciaV3_all.gs"  


if [ $1 ="" ];then
   data=`date +"%Y%m%d" `
   datagrads=`date +"00Z01%b%Y" -d"18 months ago"`
   rodada="00"
else
   data=$1
   datagrads=$2
   rodada=$3   
fi 
CDLOG="../../LOG/"$data
mkdir $CDLOG   >/dev/null 2>&1 


echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA MERGE 22 km MENSAL" >$CDLOG"/calcbacia.chuvamergemensal.prn" 2>&1 
echo "["`date`"] INICIO DO LOG CALCBACIA CHUVA MERGE 22 km MENSAL "
echo "dset ../../OBS/CHUVA_MENSAL/CHUVA_MERGE_22km_MENSAL_%m2%y4.nc" > chuvamergemensal.ctl 
echo "title merge"                                                  >> chuvamergemensal.ctl 
echo "options template"                                         >> chuvamergemensal.ctl 
echo "undef -999"                                               >> chuvamergemensal.ctl 
echo "dtype netcdf"                                              >> chuvamergemensal.ctl  
echo "xdef 251 linear -80 0.2"                               >> chuvamergemensal.ctl 
echo "ydef 313 linear -35 0.2"                               >> chuvamergemensal.ctl 
echo "zdef 1 linear 0 1"                                        >> chuvamergemensal.ctl  
echo "tdef 19  linear "$datagrads" 1mo"                         >> chuvamergemensal.ctl 
echo "vars 1"                                                   >> chuvamergemensal.ctl 
echo "rain=>rain  0  t,y,x  the grid analysis (0.1mm/day)"      >> chuvamergemensal.ctl    
echo "endvars"                                                  >> chuvamergemensal.ctl 


echo "'open chuvamergemensal.ctl' "                            >scriptchuvamergemensal.gs
echo '_arqbacia="../MODULOS/CALCBACIA/CONTORNOS/bacias_simop.dat"'    >>scriptchuvamergemensal.gs
echo '_arqconfig="../MODULOS/CALCBACIA/CONFIG/ETAconfig.dat"'      >>scriptchuvamergemensal.gs
echo "_modelo="OBSMERGEMENSAL""                            >>scriptchuvamergemensal.gs
echo "_prefixo="CHUVAMERGEMENSAL""                   >>scriptchuvamergemensal.gs
echo "freq=24"                                    >>scriptchuvamergemensal.gs
echo "rmv=0"                                      >>scriptchuvamergemensal.gs
echo "var="rain""                                 >>scriptchuvamergemensal.gs
echo "tinicio=1"                                  >>scriptchuvamergemensal.gs
echo "numtempos=19 "                              >>scriptchuvamergemensal.gs
echo "modlon=0 "                                  >>scriptchuvamergemensal.gs
echo "_conjunto=0 "                               >>scriptchuvamergemensal.gs
echo '_arq_debug="'$CDLOG'/debug_chuvamergemensal.prn"'        >>scriptchuvamergemensal.gs
echo "_formato="ALL""                             >>scriptchuvamergemensal.gs
echo "modocalc=CMB"                                >>scriptchuvamergemensal.gs   
echo "_numens_cfs=6"                               >>scriptchuvamergemensal.gs       
echo "_postos=0"                               >>scriptchuvamergemensal.gs       
echo "pi=calcbacia(var,tinicio,numtempos,modlon,rmv,freq,modocalc)"   >>scriptchuvamergemensal.gs
echo "'quit'"                                                  >>scriptchuvamergemensal.gs




cat $CALC >>scriptchuvamergemensal.gs 


grads -lbc "scriptchuvamergemensal.gs"  >>$CDLOG"/calcbacia.chuvamergemensal.prn" 2>&1 




SAIDA="../../OUTPUT/"$data$rodada
mkdir $SAIDA"/MENSAL/" >/dev/null 2>&1
mv CHUVAMERGEMENSAL*   $SAIDA"/MENSAL" >/dev/null 2>&1



# bacias=(Grande Itaipu Paranapanema Paranaiba )
# baciao=(GRANDE ITAIPU PARANAPANEMA PARANAIBA ) 
# numbacias=3

# for b in `seq 0 3`
# do
# for file in `ls -1 ${baciao[$b]}*`
# do
# file2=`echo $file | cut -d"_" -f2-6`
# mv "./"$file $SAIDA"/SMAPONS/"${bacias[$b]}"/ARQ_ENTRADA/"$file2  #>/dev/null 2>&1
# done
# done 
echo "["`date`"] FINAL DO LOG CALCBACIA ETA40KM " >>$CDLOG"/calcbacia.chuvamergemensal.prn" 2>&1  
echo "["`date`"] FINAL DO LOG CALCBACIA ETA40KM "

}



chuvamensalcpc()
{
echo "INICIO CGHUVA MENSAL MERGE" >$CDLOG"/chuvamensal.cpc.prn" 2>&1 
LOCAL="../../OBS/CHUVA_MENSAL/"

mes=(xxx jan feb mar apr may jun jul aug sep oct nov dec)
dias=(99 31 28  31  30  31  30  31  31  30  31  30   31)
nummes=(99 01 02 03 04 05 06 07 08 09 10 11 12) 


rm $LOCAL"/CHUVA_CPC_54km_MENSAL_"`date +"%m%Y"`".nc"  >/dev/null 2>&1 



for nmes in `seq 15  -1  0`
do


m=`date +"%-m" -d "$nmes months ago"`
mesx=`date +"%b" -d "$nmes months ago"` 	
grads=`date +"%b%Y" -d  "$nmes months ago"`	
t1="01"$grads
t2=${dias[$m]}$grads

echo $m    >>$CDLOG"/chuvamensal.cpc.prn" 2>&1 
echo $mesx    >>$CDLOG"/chuvamensal.cpc.prn" 2>&1 
echo $grads   >>$CDLOG"/chuvamensal.cpc.prn" 2>&1 
echo $t1  >>$CDLOG"/chuvamensal.cpc.prn" 2>&1 
echo $t2   >>$CDLOG"/chuvamensal.cpc.prn" 2>&1 



arqnc=$LOCAL"CHUVA_CPC_54km_MENSAL_"`date +"%m%Y" -d "$nmes months ago"`".nc" 
#atual=$LOCAL"/CHUVA_CPC_54km_MENSAL_"`date +"%m%Y"`".nc" 
#rm $atual 


echo $arqnc   >>$CDLOG"/chuvamensal.cpc.prn" 2>&1 

if test -e  $arqnc
then 

       echo $arqnc" Ja existe"  >>$CDLOG"/chuvamensal.merge.prn" 2>&1 
else

echo $t1" " $t2   >>$CDLOG"/chuvamensal.cpc.prn" 2>&1 
echo $arqnc       >>$CDLOG"/chuvamensal.cpc.prn" 2>&1 

echo "dset ../../OBS/CPC_GAUGE_0P50/CHUVACPC_0P50_%y4%m2%d2.nc" > chuva.ctl 
echo "title d"  >> chuva.ctl 
echo "options template ">> chuva.ctl
echo "undef -999">> chuva.ctl
echo "dtype netcdf">> chuva.ctl
echo "xdef 102 linear 279.75 0.5">> chuva.ctl
echo "ydef 092 linear -35.25 0.5">> chuva.ctl
echo "zdef 1 linear 0 1" >>chuva.ctl
echo "tdef "${dias[$m]}"  linear 01"$grads" 1dy"  >>chuva.ctl
echo "vars 1" >>chuva.ctl
echo "rain=>rain  0  t,y,x  the grid analysis (0.1mm/day)" >>chuva.ctl
echo "endvars" >>chuva.ctl



echo "'open chuva.ctl'" >script.gs
echo "'lats4d -o "$arqnc" -vars rain   -func (@/10)*"${dias[$m]}" -mean -time "$t1" "$t2"'"  >>script.gs
echo "'quit'" >>script.gs

 
grads -lbc "script.gs"   >>$CDLOG"/chuvamensal.merge.prn" 2>&1

fi        


done
}


chuvamensalmerge()
{
echo "INICIO CGHUVA MENSAL MERGE" >$CDLOG"/chuvamensal.merge.prn" 2>&1  
mes=(xxx jan feb mar apr may jun jul aug sep oct nov dec)
dias=(99 31 28  31  30  31  30  31  31  30  31  30   31)
nummes=(99 01 02 03 04 05 06 07 08 09 10 11 12) 

for nmes in `seq 19  -1  0`
do


m=`date +"%-m" -d "$nmes months ago"`
mesx=`date +"%b" -d "$nmes months ago"` 	
grads=`date +"%b%Y" -d  "$nmes months ago"`	
grads2=`date +"%b%Y"`	

t1="01"$grads
t2=${dias[$m]}$grads

arqnc="../../OBS/CHUVA_MENSAL/CHUVA_MERGE_22km_MENSAL_"`date +"%m%Y" -d "$nmes months ago"`".nc" 
atual="../../OBS/CHUVA_MENSAL/CHUVA_MERGE_22km_MENSAL_"`date +"%m%Y"`".nc" 
rm $atual >/dev/null 2>&1 


echo $arqnc  >>$CDLOG"/chuvamensal.merge.prn" 2>&1  
if test ! -e  $arqnc ;then 
echo $t1" " $t2     >>$CDLOG"/chuvamensal.merge.prn" 2>&1  
echo "dset ../../OBS/MERGE_0P25/CHUVAMERGE_0P22_%y4%m2%d2.nc" > chuvamerge.ctl
echo "options  little_endian template"                          >>chuvamerge.ctl
echo "title global daily analysis "                             >>chuvamerge.ctl
echo "undef -999.0 "                                            >>chuvamerge.ctl 
echo "dtype netcdf "                                            >>chuvamerge.ctl 
echo "xdef 251 linear    -80 0.2000"                           >>chuvamerge.ctl
echo "ydef 226 linear -35  0.2000 "                          >>chuvamerge.ctl
echo "zdef 1    linear 1 1 "                                  >>chuvamerge.ctl
echo "tdef "${dias[$m]}"  linear 01"$grads" 1dy"              >>chuvamerge.ctl  
echo "vars 1"                                                 >>chuvamerge.ctl
echo "rain=>rain  0 t,y,x  chuva acumulada 24 horas"                >>chuvamerge.ctl
echo "ENDVARS"   >>chuvamerge.ctl




echo "'open chuvamerge.ctl'" >script.gs
echo "'lats4d -o "$arqnc" -vars rain   -func (@)*"${dias[$m]}" -mean -time "$t1" "$t2"'"  >>script.gs
echo "'quit'" >>script.gs

 
grads -lbc "script.gs"   >>$CDLOG"/chuvamensal.merge.prn" 2>&1  

else
       echo $arqnc" Ja existe"   >>$CDLOG"/chuvamensal.merge.prn" 2>&1  
fi        


done
}



cicc()
{

 
cd ../MODULOS/MODELOS
./adquire_model.sh eta40  $1
./adquire_model.sh gefs  $1
./adquire_model.sh cfs  $1
./adquire_model.sh cfsvars  $1
./adquire_model.sh gfsanal  $1
./adquire_model.sh gfs  $1



 
 
 cd ../../WORKDISK
}


obs()
{
cd ../MODULOS/OBSMOD
./adquire_obs.sh $1 $2 $3 
cd ../../WORKDISK

}




#---------------------------------------------------------------



chmod 777 ../SCRIPTS/MODULOS/OBSMOD/adquire_obs.sh 
chmod 777 ../SCRIPTS/MODULOS/MODELOS/adquire_model.sh 
chmod 777 ../SCRIPTS/MODULOS/CHUVAMENSAL/chuvamensal.sh 

 


source ./config_xons.sh 
export LANG=en_us_8859_1



          
# copia os arquivos de manipulação do grib
#



mkdir -p ../SCRIPTS/WORKDISK  
cd ../SCRIPTS/WORKDISK  
rm *  >/dev/null  2>&1
cp ../MODULOS/COMMON_STUFF/g2ctl.pl .  >/dev/null  2>&1
mkdir -p CFS   >/dev/null  2>&1
export CDLOG="../../LOG/"`date +"%Y%m%d"`  
export CDLOG2="../LOG/"`date +"%Y%m%d"`  
  
mkdir $CDLOG                >/dev/null  2>&1
mkdir $CDLOG2                >/dev/null  2>&1

###echo `date `>$CDLOG2/"xons_cicc.log" 




case $1 in 

teste)
obs 
;;

sst)
cd 


cicc)
cicc     
obs
chuvamensalcpc
chuvamensalmerge
;;


smapons) 
smapons   $2   $3  $4   ####   dias para trás  RODADA   VOBS 
;;


*)
cicc
obs
# chuvamensalcpc
# chuvamensalmerge
# calcbacia_eta40_cmb
# calcbacia_gefsrmv_cmb
# calcbacia_chuvacpc
# calcbacia_chuvamerge_cmb
#calcbacia_cfsmensal
calcbacia_chuvacpc_mensal
calcbacia_chuvamerge_mensal
;;
esac